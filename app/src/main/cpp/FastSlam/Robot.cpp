//
// Created by Tarak on 15-10-2018.
//

#include "Robot.h"
#include "HelperClass.h"
#include "DataAssociation.h"


#include<cmath>
#include <random>

using namespace std;
using namespace Eigen;
using Eigen::MatrixXd;;
#include <chrono>
#include <string>
#include <sstream>
#include <iostream>

#define ROUNDD(f, c) (((double)((int)((f) * (c))) / (c)))


Robot::Robot(double x, double y, double z, double heading, vector<vector<double> > geofence) {
    this->mX = x;
    this->mY = y;
    this->mZ = z;



    this->orientation = heading;
    this->geofence = geofence;
    double data[] = {0.1, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, 0.1, 0.0, 0.0, 0.0, 0.0, pow(3.0*M_PI/180,2)};
    this->control_noise = Map<Matrix4d>(data);
    double observationdata[] = {0.1, 0.0, 0.0, pow(3.0*M_PI/180,2)};
    this->obs_noise = Map<Matrix2d >(observationdata);
}

void Robot::setValues(double x, double y, double z, double orientation) {

    if(x < this->geofence.at(0).at(1) || x > this->geofence.at(0).at(0)){
        this->mX = x;
    } else{
        throw "Index out of bounds: X";
    }

    if(y < this->geofence.at(1).at(1) || y > this->geofence.at(1).at(0)){
        this->mY = y;
    } else{
        throw "Index out of bounds: Y";
    }

    if(z < this->geofence.at(2).at(1) || z > this->geofence.at(2).at(0)){
        this->mZ = z;
    } else{
        throw "Index out of bounds: Z";
    }

    if (!(orientation < 0 ||  orientation > 2*M_PI)){
        this->orientation = orientation;
    } else{
        if(orientation < 0){
            orientation += 2 * M_PI;
            this->orientation = orientation;
        } else{
            orientation -= 2 * M_PI;
            this->orientation = orientation;
        }
    }
}

void Robot::setNoise(double forward_noise, double bearing_noise, double turnNoise,
                     double distance_noise) {
    this->forward_noise = forward_noise;
    this->turn_noise = turnNoise;
    this->bearing_noise = bearing_noise;
    this->distance_noise = distance_noise;
}

void Robot::setMappingNoise(int bluetooth_mapping_noise, double bluetooth_distance_noise,
                            int wifi_mapping_noise, float map_mapping_noise, double particle_noise) {
    this->bluetooth_mapping_noise = bluetooth_mapping_noise;
    this->bluetooth_distance_noise = bluetooth_distance_noise;
    this->wifi_mapping_noise = wifi_mapping_noise;
    this->mag_mapping_noise = map_mapping_noise;
    this->particle_noise    = particle_noise;
}

double Robot::adjustCoordinates(double value, int index) {
    double adjustedvalue = value;

    if (value < this->geofence.at(index).at(0)){
        adjustedvalue = this->geofence.at(index).at(1) - (this->geofence.at(index).at(0) - value);
    } else if(value > this->geofence.at(index).at(1)){
        adjustedvalue = this->geofence.at(index).at(0) + (value - this->geofence.at(index).at(1));
    }

    return adjustedvalue;

}

Robot* Robot::robotMove(double stepLength, double new_heading) {

    double cal_heading= 0.0;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    std::default_random_engine generator (seed);
    std::normal_distribution<double> turn_distribution(0, this->turn_noise * M_PI/180);
    double noise   = turn_distribution(generator);
    cal_heading = new_heading + noise;




    if(stepLength >0) {
        double distanceTravel = 0.0;
        if(this->isIsRobot()){
            std::normal_distribution<double> forward_distribution(0, this->forward_noise);
            double noise2 = forward_distribution(generator);
            distanceTravel = stepLength + noise2;
        }else{
            distanceTravel = stepLength;
        }

        //double distanceTravel = stepLength + noise2;

//    double[] newPosition = nextPosition(distanceTravel, Math.toDegrees(heading));
        this->previousX = mX;
        this->previousY = mY;
        this->previousZ = mZ;

        double newPosition[3];
        nextPosition(distanceTravel, radToDeg(cal_heading), newPosition);
        mX = adjustCoordinates(newPosition[0], 0);
        mY = adjustCoordinates(newPosition[1], 1);
        mZ = adjustCoordinates(newPosition[2], 2);
    }

    orientation += new_heading;
    if (orientation < 0) {
        orientation += 2 * M_PI;
    }
    orientation = fmod(orientation,2 * M_PI);



    Robot* newRobot = new Robot(*this);
    return newRobot;
}

double* Robot::nextPosition(double meters, double heading, double returnValue[]) {
    double position[2];
    double coords[] = {this->mX, this->mY, this->mZ};
    convertCartesianToSpherical(coords,position);
    double latLng[2];
    distance(position, meters, heading,latLng);

    convertSphericalToCartesian(latLng[0], latLng[1],returnValue);


    return returnValue;
}

double* Robot::distance(double *from, double distance, double heading, double latlng[]) {
    distance    /= 6371000;
    heading      = degToRad(heading);
    double fromLat = degToRad(from[0]);
    double fromLng = degToRad(from[1]);
    double cosdistance = cos(distance);
    double sindistance = sin(distance);


    double sinFromLat = sin(fromLat);
    double cosFromLat = cos(fromLat);
    double sinLat = cosdistance * sinFromLat + sindistance * cosFromLat * cos(heading);
    double dLng = atan2(
            sindistance * cosFromLat * sin(heading),
            cosdistance - sinFromLat * sinLat);
    latlng[0] = radToDeg(asin(sinLat));
    latlng[1] = radToDeg(fromLng + dLng);
    return latlng;

}

double* Robot::convertSphericalToCartesian(double latitude, double longitude, double returnValue[]) {

    double earthRadius = 6371000; //radius in m
    double lat = degToRad(latitude);
    double lon = degToRad(longitude);
    double x = earthRadius * cos(lat) * cos(lon);
    double y = earthRadius * cos(lat) * sin(lon);
    double z = earthRadius * sin(lat);
    returnValue[0] =  x;
    returnValue[1] =  y;
    returnValue[2] =  z;

    return returnValue;
}

double Robot::degToRad(double deg) {
    return (deg * M_PI)/180;
}

double* Robot::convertCartesianToSpherical(double *cartesian, double latlng[]) {
    double radius = sqrt((cartesian[0] * cartesian[0])+(cartesian[1] * cartesian[1])+(cartesian[2] * cartesian[2]));
    double lat = radToDeg(asin(cartesian[2] / radius));
    double lng = radToDeg(atan2(cartesian[1], cartesian[0]));
    latlng[0] = lat;
    latlng[1] = lng;
    return latlng;

}


double Robot::radToDeg(double deg) {
    if(deg < 0){
        deg += 2 * M_PI;
    }else if(deg > 2*M_PI){
        deg -= 2*M_PI;
    }
    double radians =  deg * 180 / M_PI;
    return radians;


}

vector<vector<double> > Robot::sense(vector<Landmark> landmarks, int min, int max) {
    if(landmarks.size() < min){
        std::ostringstream ss;
        ss<<min;
        throw "Atleast "+ss.str()+" observations needed for sense";
    }
//    sort(landmarks.begin(),landmarks.end(),[ ](  Landmark lhs,  Landmark rhs )
//    {
//        return lhs.getDistance() > rhs.getDistance();
//    });
    int obsNum = landmarks.size() > max ? max : landmarks.size();
    vector<vector<double>> obs;
    HelperClass util;
    double pos[4];
    getPos(pos);
    for(int i=0;i<obsNum;i++){
        Landmark lm = landmarks.at(i);
        double* lm_pos  = lm.getPos();
        double distance = util.getEuclideanDistance(lm_pos,pos,this->distance_noise,3);

        //double distance = util.getLandMarkdistance(lm.getDistance(),this->distance_noise);
        double angle    = util.correctedGetDirection(lm_pos,pos,(this->bearing_noise));
//        if(angle < 0){
//            angle += 2*M_PI;
//        }else if(angle >2*M_PI){
//            angle -= 2*M_PI;
//        };
        //double obsangle[] = {distance,angle};
        vector<double> obsangle;
        obsangle.push_back(distance);
        obsangle.push_back(angle);

        obs.push_back(obsangle);
        delete lm_pos;
    }



    return obs;
}

double* Robot::getPos(double pos[]) {
    pos[0]  = this->mX;
    pos[1]  = this->mY;
    pos[2]  = this->mZ;
    pos[3]  = this->orientation;
    return pos;
}

Robot* Robot::robotUpdate(vector<vector<double>> obs) {

    vector<DataAssociation> dataAssociationList;
    for(int i=0;i<obs.size();i++){

        vector<double> obsValues = (vector<double> &&) obs.at(i);
        double prob        = exp(-70);
        int landmark_index = -1;
        if(this->landMarks.size() > 0){
            double dataass[2];
            this->pre_compute_data_association(obsValues, dataass);
            prob = dataass[0];
            landmark_index = (int)dataass[1];
            if (prob < this->TOL)
                landmark_index = -1;
        }
        DataAssociation dAss(obsValues, landmark_index, prob);
        dataAssociationList.push_back(dAss);

    }

    sort(dataAssociationList.begin(), dataAssociationList.end(),
         [] (DataAssociation & a, DataAssociation & b) { return a.getLandmark_index() < b.getLandmark_index(); });
    //sort such that new landmarks are placed last
    reverse(dataAssociationList.begin(), dataAssociationList.end());

    MatrixXd initial_pose(4,1);
    double temp_array[]   = {this->mX, this->mY, this->mZ, this->orientation};
    initial_pose          = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(temp_array, 4, 1);

    MatrixXd pose_mean        = initial_pose;
    MatrixXd pose_cov         = this->control_noise;

    //update pose of particles
    for (DataAssociation da:dataAssociationList) {
        int land_indx       = da.getLandmark_index();

        if(land_indx > -1){
            vector<MatrixXd> jacobians = computeJacobians(this->landMarks.at(land_indx));
            pose_cov             = (jacobians.at(2).transpose() * ((jacobians.at(3).inverse()) * (jacobians.at(2))) + pose_cov.inverse()).inverse();
            double val[da.getObservation().size()];
            for (int i=0;i<da.getObservation().size();i++){
                val[i] = da.getObservation().at(i);
            }
            pose_mean            = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(temp_array, 4, 1) + pose_cov * (jacobians.at(2).transpose()) * ((jacobians.at(3)).inverse())*(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(val, 1, 2).transpose() - (jacobians.at(0)));

            EigenMultivariateNormal<double> mnvSolver(pose_mean.col(0), pose_cov);
//            MultivariateNormalDistribution mnd = new MultivariateNormalDistribution(pose_mean.getColumn(0), pose_cov.getData());
            Matrix<double ,4, 1> new_pose  = mnvSolver.samples(1);

            double x    = new_pose(0, 0);
            double y    = new_pose(1, 0);
            double z    = new_pose(2, 0);
            double orie = new_pose(3, 0);

            this->setValues(new_pose(0, 0), new_pose(1, 0), new_pose(2, 0), new_pose(3, 0));


        }else{
            //add this landmark to the list
            this->createLandMark(da.getObservation());
        }
    }
    HelperClass util;

    //update landmarks kalman matrix
    for (DataAssociation da:dataAssociationList) {
        int land_indx       = da.getLandmark_index();

        if(land_indx > -1){
            vector<MatrixXd>jacobians = computeJacobians(this->landMarks.at(land_indx));
            double val[da.getObservation().size()];
            for (int i=0;i<da.getObservation().size();i++){
                val[i] = da.getObservation().at(i);
            }

            double x    = jacobians.at(3)(0, 0);
            double y    = jacobians.at(3)(0, 1);
            double z    = jacobians.at(3)(1, 0);
            double orie = jacobians.at(3)(1, 1);
            double normal = util.multi_normal(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(val, 1, 2).transpose(), jacobians.at(0), jacobians.at(3));
            this->weight = pow(normal,0.5);
            this->updateLandMark(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(val, 1, 2).transpose(), land_indx, jacobians.at(0),  jacobians.at(1), jacobians.at(3));
        }else{
            this->weight *= da.getProbability();
        }
    }
    double temp[]   = {this->mX, this->mY, this->mZ, this->orientation};
    double prior = util.multi_normal(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(temp, 4, 1), initial_pose, this->control_noise);
    double prop = util.multi_normal(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(temp, 4, 1), pose_mean, pose_cov)+0.000000001;
    double likelihood = pow(prior/prop, 0.25);
    this->weight *=  likelihood;
    if(std::isnan(this->weight)){
        cout<<"see here";
    }
    if(this->weight == 0){
        //System.out.print("check");
        cout<<"see here";
    }
    if(std::isnan(this->weight)){
        //System.out.print("check");
        cout<<"xxx";
    }



    Robot* newRobot = new Robot(*this);
    return newRobot;

}

double* Robot::pre_compute_data_association(vector<double>obsValues, double computed[]) {
    double prob = 0.0;
    int landmark_idx = -1;
    HelperClass util;
    double temp[]   = {this->mX, this->mY, this->mZ, this->orientation};
    for(int i=0;i<this->landMarks.size();i++){
        Landmark landMark = landMarks.at(i);
        vector<MatrixXd> jacobians      = computeJacobians(landMark);

        MatrixXd pose_cov             = (jacobians.at(2).transpose() * ((jacobians.at(3).inverse()) * (jacobians.at(2))) + (this->control_noise).inverse()).inverse();
        double val[obsValues.size()];
        for (int in=0;in<obsValues.size();in++){
            val[in] = obsValues.at(in);
        }
        MatrixXd pose_mean             = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(temp, 4, 1) + pose_cov * (jacobians.at(2).transpose()) * ((jacobians.at(3)).inverse())*(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(val, 1, 2).transpose() - (jacobians.at(0)));
        EigenMultivariateNormal<double> mnvSolver(pose_mean.col(0), pose_cov);
        Matrix<double ,4, 1> new_pose_mat   = mnvSolver.samples(1);
        double new_pose[]               = {new_pose_mat(0, 0), new_pose_mat(1, 0), new_pose_mat(2, 0), new_pose_mat(3, 0)};
        double distance                 = util.getEuclideanDistance(landMark.getPos(),new_pose,0,3);
        double angle                    = util.correctedGetDirection(landMark.getPos(),new_pose,0);
        double observed[]               = {distance, angle};
        double p                        = util.multi_normal(Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(val, 1, 2).transpose(),Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(observed, 1, 2).transpose() , jacobians.at(3));
        if(p > prob){
            prob = p;
            landmark_idx = i;
        }




    }

//    delete util;
    computed[0]     = prob;
    computed[1]     = landmark_idx;

    return computed;
}

//vector<MatrixXd> Robot::computeJacobians(Landmark landmark) {
//    vector<MatrixXd>jacobians;
//    double* landmarkCoords = landmark.getPos();
//    double particlePos[4];
//    getPos(particlePos);
//
//    double dx              = landmarkCoords[0] - particlePos[0];
//    double dy              = landmarkCoords[1] - particlePos[1];
//    double dz              = landmarkCoords[2] - particlePos[2];
//
//    double distance        = sqrt(pow(dx,2)+pow(dy,2)+pow(dz,2));
//    //double distance          = pow(dx,2)+pow(dy,2)+pow(dz,2);
//    double earthRadius      = 6371000;
//
//    double  a               = earthRadius * ((particlePos[0]*landmarkCoords[1]) - (landmarkCoords[0] * particlePos[1]));
//    double  b               = landmarkCoords[2]* (pow(particlePos[0], 2)+pow(particlePos[1],2)) - particlePos[2]*(landmarkCoords[0]*particlePos[0]+landmarkCoords[1]*particlePos[1]);
//    double  orientationdiff = atan2(a,b);
//
//    double obs[]            = {distance, orientationdiff};
//    MatrixXd predicted_obs    = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(obs, 2, 1);
//    double  denominator         = (pow(a,2)+pow(b,2));
//
//    double feature_alpha        = (((a* particlePos[2]* particlePos[0]) - (earthRadius * particlePos[1] * b)) / denominator);
//    double feature_beta         = (((a* particlePos[2]* particlePos[1]) + (earthRadius * particlePos[0] * b)) / denominator);
//    double feature_gamma        = a*((pow(particlePos[0],2)+pow(particlePos[1],2))) / denominator;
//
//
//    double distance_differential[]       = { (dx/distance), (dy/distance), (dz/distance), feature_alpha, feature_beta, feature_gamma};
//    MatrixXd feature_jacobian = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(distance_differential, 2, 3);
//
//
//    double  pose_alpha          = ((earthRadius * landmarkCoords[1] * b) + (a * (landmarkCoords[0] * particlePos[2] - 2*landmarkCoords[2]*particlePos[0]))) / denominator;
//    double  pose_beta           = ((a * (particlePos[2] * landmarkCoords[1] - 2*particlePos[1]*landmarkCoords[2])) - (earthRadius * b * landmarkCoords[0])) / denominator;
//    double  pose_gamma          = (a*( (particlePos[0] * landmarkCoords[0]) + (particlePos[1] * landmarkCoords[1]))) / denominator;
//
//    double angle_differential[]       = { (-dx/distance), (-dy/distance), (-dz/distance), 0 ,pose_alpha, pose_beta, pose_gamma, -1};
//
//    MatrixXd pose_jacobian    = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(angle_differential, 2, 4);
//    MatrixXd adj_cov          = ((feature_jacobian) * (landmark.getSigma()) * (feature_jacobian.transpose())) + (this->obs_noise);
//
//    double first = adj_cov(0,0);
//    double second = adj_cov(0,1);
//    double third = adj_cov(1,0);
//    double fourth = adj_cov(1,1);
//
//    jacobians.push_back(predicted_obs);
//    jacobians.push_back(feature_jacobian);
//    jacobians.push_back(pose_jacobian);
//    jacobians.push_back(adj_cov);
//
//    return jacobians;
//}


vector<MatrixXd> Robot::computeJacobians(Landmark landmark) {
    vector<MatrixXd>jacobians;
    double* landmarkCoords = landmark.getPos();
    double particlePos[4];
    getPos(particlePos);

    double dx              = landmarkCoords[0] - particlePos[0];
    double dy              = landmarkCoords[1] - particlePos[1];
    double dz              = landmarkCoords[2] - particlePos[2];

    double distance        = sqrt(pow(dx,2)+pow(dy,2)+pow(dz,2));
    //double distance          = pow(dx,2)+pow(dy,2)+pow(dz,2);
    double earthRadius      = 6371000;


    double costhetadenom = pow(earthRadius,2);
    //double costhetanum   = (((1/costhetadenom)*landmarks[0] * robot[0])) + (((1/costhetadenom) * landmarks[1] * robot[1])) + (((1/costhetadenom) * landmarks[2] * robot[2]));

    double x_factor      = landmarkCoords[0]/costhetadenom*particlePos[0];
    double y_factor      = landmarkCoords[1]/costhetadenom*particlePos[1];
    double z_factor      = landmarkCoords[2]/costhetadenom*particlePos[2];


    double costheta      = ROUNDD(x_factor + y_factor + z_factor,100);


    double orientationdiff = acos(costheta);



    double obs[]            = {distance, orientationdiff};
    MatrixXd predicted_obs    = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(obs, 2, 1);

    double commonmultiple   = (-1 / (sqrt(1-pow(costheta,2) + 0.00000001)));
    double feature_alpha    = particlePos[0] * commonmultiple / (costhetadenom);
    double feature_beta     = (particlePos[1] * commonmultiple) / (costhetadenom);
    double feature_gamma    = (particlePos[2] * commonmultiple) / (costhetadenom) ;

    double pose_alpha       = (landmarkCoords[0] * commonmultiple) / (costhetadenom);
    double pose_beta        = (landmarkCoords[1] * commonmultiple) / (costhetadenom);
    double pose_gamma       = (landmarkCoords[2] * commonmultiple) / (costhetadenom);



    double distance_differential[]       = { (dx/distance), (dy/distance), (dz/distance), feature_alpha, feature_beta, feature_gamma};
    MatrixXd feature_jacobian            = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(distance_differential, 2, 3);



    double angle_differential[]       = { (-dx/distance), (-dy/distance), (-dz/distance), 0 ,pose_alpha, pose_beta, pose_gamma, -1};

    MatrixXd pose_jacobian    = Map<Matrix<double,Dynamic,Dynamic,RowMajor>>(angle_differential, 2, 4);

    MatrixXd adj_cov          = feature_jacobian * (landmark.getSigma()) * (feature_jacobian.transpose()) + (this->obs_noise);

    double first = adj_cov(0,0);
    double second = adj_cov(0,1);
    double third = adj_cov(1,0);
    double fourth = adj_cov(1,1);


    jacobians.push_back(predicted_obs);
    jacobians.push_back(feature_jacobian);
    jacobians.push_back(pose_jacobian);
    jacobians.push_back(adj_cov);

    return jacobians;
}




void Robot::createLandMark(vector<double> obs) {
    Landmark lm = guessLandMark(obs);
    this->landMarks.push_back(lm);
}

Landmark Robot::guessLandMark(vector<double> obs) {
    double distance = obs.at(0);
    double turn     = obs.at(1);
    if (turn < 0) {
        turn += 2 * M_PI;
    }
    turn = fmod(turn,2 * M_PI);
    double nextpositionarray[3];
    nextPosition(distance,radToDeg(turn),nextpositionarray);
    Landmark lmark(nextpositionarray[0], nextpositionarray[1], nextpositionarray[2]);
    return  lmark;
}

void Robot::updateLandMark(MatrixXd obs, int landmark_idx, MatrixXd ass_obs, MatrixXd ass_jacobian,
                           MatrixXd ass_adjcov) {

    Landmark lm             = this->landMarks.at(landmark_idx);
    MatrixXd K_gain         = lm.getSigma() * (ass_jacobian.transpose())* ((ass_adjcov).inverse());
    MatrixXd new_mu         = lm.getMu() + (K_gain * (obs - ass_obs));
    MatrixXd new_sig        = (MatrixXd::Identity(3,3) - (K_gain * (ass_jacobian))) * (lm.getSigma());
    lm.update(new_mu, new_sig);
    this->landMarks.at(landmark_idx) = lm;

}

double Robot::getWeight() {
    return this->weight;
}

double Robot::getX() {
    return this->mX;
}

double Robot::getY() {
    return this->mY;
}

double Robot::getZ() {
    return this->mZ;
}

void Robot::setWeight(double weight) {
    this->weight = weight;
}

Robot::Robot(const Robot &robot) {

    this->mX          = robot.mX;
    this->mY          = robot.mY;
    this->mZ          = robot.mZ;
    this->orientation = robot.orientation;
    this->geofence    = robot.geofence;
    this->control_noise = robot.control_noise;
    this->obs_noise     = robot.obs_noise;
    this->landMarks     = robot.landMarks;
    this->forward_noise = robot.forward_noise;
    this->turn_noise    = robot.turn_noise;
    this->bearing_noise = robot.bearing_noise;
    this->distance_noise = robot.distance_noise;
    this->bluetooth_mapping_noise = robot.bluetooth_mapping_noise;
    this->bluetooth_distance_noise = robot.bluetooth_distance_noise;
    this->wifi_mapping_noise = robot.wifi_mapping_noise;
    this->mag_mapping_noise = robot.mag_mapping_noise;
    this->weight            = robot.weight;
    this->particle_noise  = robot.particle_noise;

    this->previousX      = robot.previousX;
    this->previousY      = robot.previousY;
    this->previousZ      = robot.previousZ;

    this->readings   = robot.readings;
    this->id         = robot.id;
    this->isRobot    = robot.isRobot;
}

const double Robot::getConstantWeight() {
    return this->weight;
}

void Robot::addMeasurement(vector<Beaconsdata> bdata, vector<Wifidetails> wData, float mag_x,
                           float mag_y, float mag_z, vector<Particlepos> particlepos, float device_mag_x, float device_mag_y, float device_mag_z, vector<Particlepos> waypointpos) {
    vector<Beaconsdata> modifiedBeaconsData;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    for(int i=0;i<bdata.size();i++){
        Beaconsdata beacon = bdata.at(i);
        double distance = beacon.getBeacon_distance();
        int rssi        = beacon.getRssi();


        default_random_engine generator_distance(seed);
        std::normal_distribution<double> distance_distribution(0.0, this->bluetooth_distance_noise);
        double noise   = distance_distribution(generator_distance);
        //distance += noise;
        beacon.setBeacon_distance(distance);

        default_random_engine generator_rssi(seed);
        std::normal_distribution<double > rssi_distribution(0.0, this->bluetooth_mapping_noise);
        int rssi_noise   = (int) rssi_distribution(generator_rssi);
        //rssi += rssi_noise;
        beacon.setRssi(rssi);
        modifiedBeaconsData.push_back(beacon);
    }

    vector<Wifidetails> modifiedWifiDetails;
    for (int i=0;i<wData.size();i++){
        Wifidetails wifi = wData.at(i);
        int rssi         = wifi.getRssi();

        default_random_engine generator_rssi(seed);
        std::normal_distribution<double > rssi_distribution(0.0, this->wifi_mapping_noise);
        int rssi_noise   = (int) rssi_distribution(generator_rssi);
        //rssi += rssi_noise;
        wifi.setRssi(rssi);
        modifiedWifiDetails.push_back(wifi);
    }

    vector<Particlepos> modifiedParticlePos;
    for (int i=0; i<particlepos.size();i++){
        Particlepos particle = particlepos.at(i);

        default_random_engine generator_distance(seed);
        std::normal_distribution<double> distance_distribution(0.0, this->particle_noise);

        double x        = particle.getX();
//        double x_noise  =  distance_distribution(generator_distance);
//        x               += x_noise;
        particle.setX(x);

        double y        = particle.getY();
//        double y_noise  =  distance_distribution(generator_distance);
//        y               += y_noise;
        particle.setY(y);


        double z        = particle.getZ();
//        double z_noise  =  distance_distribution(generator_distance);
//        z               += z_noise;
        particle.setZ(z);
        modifiedParticlePos.push_back(particle);
    }

//    default_random_engine generator_mag(seed);
//    std::normal_distribution<double > mag_distribution(0.0, this->mag_mapping_noise);
//    float x_noise = (float) mag_distribution(generator_mag);
//    float y_noise = (float) mag_distribution(generator_mag);
//    float z_noise = (float) mag_distribution(generator_mag);
//
//    mag_x         += x_noise;
//    mag_y         += y_noise;
//    mag_z         += z_noise;

    measurement measurment(modifiedBeaconsData, modifiedWifiDetails, modifiedParticlePos, mag_x, mag_y, mag_z, device_mag_x, device_mag_y, device_mag_z, waypointpos);
    this->readings.push_back(measurment);

}

vector<measurement> Robot::getReadings() {
    return this->readings;
}

int Robot::getId() const {
    return id;
}

void Robot::setId(int id) {
    Robot::id = id;
}

bool Robot::isIsRobot() const {
    return isRobot;
}

void Robot::setIsRobot(bool isRobot) {
    Robot::isRobot = isRobot;
}

double Robot::getPreviousX() const {
    if(previousX == 0){
        return mX;
    }
    return previousX;
}

double Robot::getPreviousZ() const {
    if(previousZ == 0){
        return mZ;
    }
    return previousZ;
}

double Robot::getPreviousY() const {
    if(previousY == 0){
        return mY;
    }
    return previousY;
}

void Robot::setMX(double mX) {
   this->mX = mX;
}

void Robot::setMY(double mY) {
    this->mY = mY;
}

void Robot::setMZ(double mZ) {
    this->mZ = mZ;
}




