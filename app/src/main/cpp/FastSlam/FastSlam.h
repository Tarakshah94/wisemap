//
// Created by noob on 12/11/18.
//

#ifndef WISEMAP_FASTSLAM_H
#define WISEMAP_FASTSLAM_H
#include "../dist/json/json.h"
#include "Robot.h"
#include "Landmark.h"
#include "../models/Beaconsdata.h"
#include "../models/Wifidetails.h"
#include "../models/Particlepos.h"

class FastSlam {

private:
    int particleSize;
    vector<vector<double>> geofence;
    int minObservations;
    int maxObservations;
    bool isActive;
    Robot* robot;
    vector<Robot*>particles;
    vector<Landmark> landmarks;
    vector<Landmark>previousLandmarks;
    vector<Beaconsdata> bData;
    vector<Wifidetails> wData;
    vector<vector<Particlepos>>debugValues;

    float mag_x;
    float mag_y;
    float mag_z;
    float device_mag_x;
    float device_mag_y;
    float device_mag_z;
    double wx;
    double wy;
    double wz;
    string savePath;
    bool avoidReasample;



public:


    void initFastSlamGeofence(vector<vector<double>> geofence, string path);
    void initFastSlamParticleSize(int pSize);
    void setObservationsThreshold(int min, int max);
    string setStartFlag(bool flag);
    void initSimulation(double x, double y, double z, double orientation);
    void setRobotPosition(double x, double y, double z);
    double randd() {
        return (double)rand() / (RAND_MAX + 1.0);
    }
    void setLandMarks(vector<Landmark> landmarks);
    void moveRobotWorld(double distance, double turn);
    void senseAndUpdate();
    double* getUserPosition(double* result);
    double* returnRobotPosition(double* result);
    void resampleParticles();
    void addNearestNeighboursToAll();
    void setSensorReadings(vector<Beaconsdata> bData, vector<Wifidetails> wData, float mag_x, float mag_y,
                           float mag_z, float device_mag_x, float device_mag_y, float device_mag_z, double wx, double wy, double wz);

};


#endif //WISEMAP_FASTSLAM_H
