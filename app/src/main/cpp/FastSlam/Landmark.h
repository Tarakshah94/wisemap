//
// Created by Tarak on 15-10-2018.
//

#ifndef WISEMAP_LANDMARK_H
#define WISEMAP_LANDMARK_H

//#include <src/Core/Matrix.h>
#include "../Eigen/Dense"
using namespace Eigen;


class Landmark {
private:
    double mX;
    double mY;
    double mZ;
    double distance;
public:
    double getDistance() const;

    void setDistance(double rssi);

private:
    MatrixXd mu;
    MatrixXd sig;

public:
    Landmark(double x, double y, double z);

    void update(MatrixXd mu, MatrixXd sig);

    double * getPos();

    MatrixXd getMu();

    MatrixXd getSigma();

};


#endif //WISEMAP_LANDMARK_H
