//
// Created by Tarak on 15-10-2018.
//

#include "DataAssociation.h"


DataAssociation::DataAssociation(vector<double>obs, int landmark_index, double probability) {
    this->observation       =   obs;
    this->landmark_index    =   landmark_index;
    this->probability       =   probability;
}

vector<double> DataAssociation::getObservation() {
    return this->observation;
}

int DataAssociation::getLandmark_index() {
    return this->landmark_index;
}

double DataAssociation::getProbability() {
    return this->probability;
}
