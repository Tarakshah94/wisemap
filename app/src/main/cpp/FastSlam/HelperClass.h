//
// Created by Tarak on 15-10-2018.
//

#ifndef WISEMAP_HELPERCLASS_H
#define WISEMAP_HELPERCLASS_H

#include "../Eigen/Dense"
#include<cmath>
#include <iostream>
#define ROUNDD(f, c) (((double)((int)((f) * (c))) / (c)))

using namespace std;
using Eigen::MatrixXd;

class HelperClass {
public:
    double getEuclideanDistance(double landmarks[], double robot[], double distanceNoise,int size) {

        double diff = 0.0;
        distanceNoise =  (((double) rand() / (RAND_MAX))*(distanceNoise));
        for (int i = 0; i < size; i++) {
            diff += pow((landmarks[i] - robot[i]), 2);
        }
        if (distanceNoise + diff > 0) {
            diff += distanceNoise;
        }
        return sqrt(diff);
    }

//    double getDirection(double landmarks[], double robot[], double bearingNoise) {
//        double earthRadius = 6371000;
//
//        double a = earthRadius * ((robot[0] * landmarks[1]) - (landmarks[0] * robot[1]));
//        double b = landmarks[2] * (pow(robot[0], 2) + pow(robot[1], 2)) -
//                   robot[2] * (landmarks[0] * robot[0] + landmarks[1] * robot[1]);
//        double direction = atan2(a, b);
//        double result = 0.0;
//
//        bearingNoise         =  (((double) rand() / (RAND_MAX))*(bearingNoise*M_PI/180));
//
//        if(bearingNoise > 0 ) {
//            result = bearingNoise + direction;
//        }else{
//            result = direction;
//        }
//
//        if (result < 0) {
//            result += 2 * M_PI;
//        }
//
//        result = fmod(result,2 * M_PI);
//
//        return result;
//    }

    double correctedGetDirection(double landmarks[], double robot[], double bearingNoise){
        double earthRadius = 6371000;//metres
        //A.B/R2 = x1.x2 + y1.y2 + z1.z2 / R * R
        double noise         =  (((double) rand() / (RAND_MAX))*(bearingNoise*M_PI/180));

        double costhetadenom = pow(earthRadius,2);
        //double costhetanum   = (((1/costhetadenom)*landmarks[0] * robot[0])) + (((1/costhetadenom) * landmarks[1] * robot[1])) + (((1/costhetadenom) * landmarks[2] * robot[2]));

        double x_factor      = (landmarks[0]/costhetadenom)*robot[0];
        double y_factor      = (landmarks[1]/costhetadenom)*robot[1];
        double z_factor      = (landmarks[2]/costhetadenom)*robot[2];


        double costheta      = ROUNDD(x_factor + y_factor + z_factor,100);
        if(costheta > 1){
            costheta = 1;
        }
        double result = acos(costheta)+noise;
        if (result < 0) {
            result += 2 * M_PI;
        }

        result = fmod(result,2 * M_PI);
        return result;
    }






    double multi_normal(MatrixXd x, MatrixXd mean, MatrixXd cov){
        double det          = cov.determinant();
        if(det < 0){
            std::cout<<"catch here"<<endl;
            //det =0;
        }
        if(det < -0.5){
            std::cout<<"catch here too"<<endl;
        }
        det = abs(det);
        double denominator  = sqrt(2*M_PI*det)+0.0000001;
        //double denominator = sqrt(2*M_PI *det);
        MatrixXd gaussMatrix = ((x - mean).transpose())*(cov.inverse())*(x - mean);
        double insideGaussian = gaussMatrix(0,0);
        double num = exp(-0.5 * insideGaussian);
        double val = num/denominator;
        //val = pow(0.3,val);
        return val;
    }

    double getLandMarkdistance(double distance,double noise){
        double random_noise =  (((double) rand() / (RAND_MAX))*(noise));
        distance                += random_noise;

        return distance;

    }

    double getMeasurementProbability(double mu, double sigma,double value){
        double numerator = -1*pow((mu - value)/sigma ,2)*0.5;
        numerator        = exp(numerator);
        double denominator      = sqrt(2 * M_PI)*sigma;

        double propbability  = numerator/denominator;
    }

};
#endif //WISEMAP_HELPERCLASS_H
