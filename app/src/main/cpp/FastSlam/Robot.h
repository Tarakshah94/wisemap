//
// Created by Tarak on 15-10-2018.
//

#ifndef WISEMAP_ROBOT_H
#define WISEMAP_ROBOT_H

#include <string>
#include <string.h>


#include <vector>
#include "Landmark.h"
#include "Eigenmvn.h"
#include "../models/measurement.h"

using namespace std;
using namespace Eigen;


class Robot {
private:
    double mX;
public:
    void setMX(double mX);

    void setMY(double mY);

    void setMZ(double mZ);

private:
    double mY;
    double mZ;

    double previousX = 0;
    double previousZ = 0;
    double previousY = 0;
public:
    double getPreviousX() const;

    double getPreviousZ() const;

    double getPreviousY() const;

private:

    double orientation;
    double forward_noise    = 0.0;
    double turn_noise       = 0.0;
    double bearing_noise    = 0.0;
    double distance_noise   = 0.0;
    double weight           = 1.0;
    vector<vector<double> >geofence;
    double TOL              = 1E-2;

    MatrixXd control_noise;
    MatrixXd obs_noise;
    vector<Landmark> landMarks;

    int bluetooth_mapping_noise     = 0;
    double bluetooth_distance_noise = 0.0;
    double wifi_mapping_noise          = 0.0;
    float mag_mapping_noise        = 0.0;
    double particle_noise            = 0.0;

    vector<measurement> readings;
    int id;
    bool isRobot;
public:
    bool isIsRobot() const;

    void setIsRobot(bool isRobot);

public:
    int getId() const;

    void setId(int id);

public:
    Robot(double x, double y, double z, double heading, vector<vector<double> > geofence);

    Robot(const Robot& robot);

    void setValues(double x, double y, double z, double orientation);

    void setNoise(double forward_noise, double bearing_noise, double turnNoise, double distance_noise);

    void setMappingNoise(int bluetooth_mapping_noise, double bluetooth_distance_noise, int wifi_mapping_noise,
                    float map_mapping_noise, double particle_noise);

    Robot* robotMove(double stepLength, double new_heading);

    vector<vector<double> > sense(vector<Landmark>landmarks, int min,int max);

    Robot* robotUpdate(vector<vector<double>>);

    double getWeight();

    const double getConstantWeight();

    double getX();

    double getY();

    double getZ();

    void setWeight(double weight);

    void addMeasurement(vector<Beaconsdata> bdata,vector<Wifidetails> wData, float mag_x, float mag_y, float mag_z, vector<Particlepos>particlepos, float device_mag_x, float device_mag_y, float device_mag_z, vector<Particlepos> waypointpos);

    vector<measurement> getReadings();

private:
    double adjustCoordinates(double value, int index);

    double * nextPosition(double meters, double heading, double returnValue[]);

    double * distance(double * from, double distance, double heading, double latlng[]);

    double * convertSphericalToCartesian(double latitude, double longitude, double returnarray[]);

    double degToRad(double deg);

    double * convertCartesianToSpherical(double * cartesian, double latlng[]);

    double radToDeg(double deg);

    double * getPos(double pos[]);

    double * pre_compute_data_association(vector<double>obsValues, double computed[]);

    vector<MatrixXd> computeJacobians(Landmark landmark);

    void createLandMark(vector<double> obs);

    Landmark guessLandMark(vector<double> obs);

    void updateLandMark(MatrixXd obs,int landmark_idx,MatrixXd ass_obs, MatrixXd ass_jacobian, MatrixXd ass_adjcov);


};



#endif //WISEMAP_ROBOT_H
