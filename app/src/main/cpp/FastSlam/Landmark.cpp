//
// Created by Tarak on 15-10-2018.
//

#include "Landmark.h"

Landmark::Landmark(double x, double y, double z) {
    this->mX = x;
    this->mY = y;
    this->mZ = z;

    double posData[3][1] = {{this->mX}, {this->mY}, {this->mZ}};
    this->mu = Map<MatrixXd>(*posData ,3, 1);
    this->sig = MatrixXd::Identity(3, 3) ;


}

void Landmark::update(MatrixXd mu, MatrixXd sig) {
        this->mu    = mu;
        this->sig   = sig;
        this->mX    = mu(0,0);
        this->mY    = mu(1,0);
        this->mZ    = mu(2,0);
}

double* Landmark::getPos() {
    return new double[3]{this->mX, this->mY, this->mZ};
}

MatrixXd Landmark::getMu() {
    return this->mu;
}

MatrixXd Landmark::getSigma() {
    return this->sig;
}

double Landmark::getDistance() const {
    return distance;
}

void Landmark::setDistance(double rssi) {
    Landmark::distance = rssi;
}
