//
// Created by noob on 12/11/18.
//

#include "FastSlam.h"
#include "cmath"
#include "random"
#include<algorithm>
#include <fstream>
#include <memory>
#include <cstdio>
#include <stdio.h>
#include <stdlib.h>
#include <android/log.h>
#include <iostream>


void FastSlam::initFastSlamGeofence(vector<vector<double>> geofence, string path) {
    this->geofence      = geofence;
    this->savePath          = path;
}

void FastSlam::initFastSlamParticleSize(int pSize) {
    this->particleSize  = pSize;
}

void FastSlam::setObservationsThreshold(int min, int max) {
    this->minObservations = min;
    this->maxObservations = max;
}

string FastSlam::setStartFlag(bool flag) {
    this->isActive = flag;

    if(!this->isActive){
        //save file to disk



        Json::Value readingValues(Json::arrayValue);

        //robots measurwement values
        vector<measurement> robotmeasurement = this->robot->getReadings();



        for (unsigned long int m =0;m<robotmeasurement.size();m++){
            measurement robotreading = robotmeasurement.at(m);
            Json::Value singlereading(Json::objectValue);

            Json::Value beacons(Json::arrayValue);
            Json::Value wifi(Json::arrayValue);
            Json::Value particles(Json::arrayValue);
            Json::Value magnetic(Json::arrayValue);
            Json::Value deviceMagnetic(Json::arrayValue);
            Json::Value waypointsPos(Json::arrayValue);
            vector<Beaconsdata>bdata = robotreading.getBData();

            for(int b=0;b<bdata.size();b++){
                Beaconsdata data = bdata.at(b);
                Json::Value singlebeacon(Json::objectValue);
                singlebeacon["rssi"]     = data.getRssi();
                singlebeacon["distance"] = data.getBeacon_distance();
                singlebeacon["kalman_distance"] = data.getKalman_distance();
                singlebeacon["beacon_x"] = data.getBeacon_pos_x();
                singlebeacon["beacon_y"] = data.getBeacon_pos_y();
                singlebeacon["beacon_z"] = data.getBeacon_pos_z();
                beacons.append(singlebeacon);
            }

            vector<Wifidetails>wdata = robotreading.getWData();
            for(int b=0;b<wdata.size();b++){
                Wifidetails data = wdata.at(b);
                Json::Value singlebeacon(Json::objectValue);
                singlebeacon["rssi"]     = data.getRssi();
                singlebeacon["id"]       = data.getBssid();

                wifi.append(singlebeacon);
            }

            vector<Particlepos>pdata = robotreading.getParticles();
            for(int b=0;b<pdata.size();b++){
                Particlepos data = pdata.at(b);
                Json::Value singlebeacon(Json::objectValue);
                singlebeacon["x"]     = data.getX();
                singlebeacon["y"]       = data.getY();
                singlebeacon["z"]       = data.getZ();

                particles.append(singlebeacon);
            }

            vector<Particlepos>waypointdata = robotreading.getWaypoint();
            for(int b=0;b<waypointdata.size();b++){
                Particlepos data = waypointdata.at(b);
                Json::Value singlebeacon(Json::objectValue);
                singlebeacon["x"]     = data.getX();
                singlebeacon["y"]       = data.getY();
                singlebeacon["z"]       = data.getZ();

                waypointsPos.append(singlebeacon);
            }


            magnetic.append(robotreading.getMag_x());
            magnetic.append(robotreading.getMag_y());
            magnetic.append(robotreading.getMag_z());

            deviceMagnetic.append(robotreading.getDevice_mag_x());
            deviceMagnetic.append(robotreading.getDevice_mag_y());
            deviceMagnetic.append(robotreading.getDevice_mag_z());

            singlereading["beacons"]          = beacons;
            singlereading["wifi"]             = wifi;
            singlereading["particles"]        = particles;
            singlereading["waypoints"]        = waypointsPos;
            singlereading["magnetic"]         = magnetic;
            singlereading["device_magnetic"]  = deviceMagnetic;

            readingValues.append(singlereading);
        }



//        for(int i=0;i<this->particleSize;i++) {
//            Robot* particle = this->particles.at(i);
//            vector<measurement> robotmeasurement = particle->getReadings();
//
//
//            for (unsigned long int m =0;m<robotmeasurement.size();m++){
//                measurement robotreading = robotmeasurement.at(m);
//                Json::Value singlereading(Json::objectValue);
//
//                Json::Value beacons(Json::arrayValue);
//                Json::Value wifi(Json::arrayValue);
//                Json::Value particles(Json::arrayValue);
//                Json::Value magnetic(Json::arrayValue);
//
//                vector<Beaconsdata>bdata = robotreading.getBData();
//                for(int b=0;b<bdata.size();b++){
//                    Beaconsdata data = bdata.at(b);
//                    Json::Value singlebeacon(Json::objectValue);
//                    singlebeacon["rssi"]     = data.getRssi();
//                    singlebeacon["distance"] = data.getBeacon_distance();
//                    singlebeacon["beacon_x"] = data.getBeacon_pos_x();
//                    singlebeacon["beacon_y"] = data.getBeacon_pos_y();
//                    singlebeacon["beacon_z"] = data.getBeacon_pos_z();
//                    beacons.append(singlebeacon);
//                }
//
//                vector<Wifidetails>wdata = robotreading.getWData();
//                for(int b=0;b<wdata.size();b++){
//                    Wifidetails data = wdata.at(b);
//                    Json::Value singlebeacon(Json::objectValue);
//                    singlebeacon["rssi"]     = data.getRssi();
//                    singlebeacon["id"]       = data.getBssid();
//
//                    wifi.append(singlebeacon);
//                }
//
//                vector<Particlepos>pdata = robotreading.getParticles();
//                for(int b=0;b<pdata.size();b++){
//                    Particlepos data = pdata.at(b);
//                    Json::Value singlebeacon(Json::objectValue);
//
//                    singlebeacon["x"]       = data.getX();
//                    singlebeacon["y"]       = data.getY();
//                    singlebeacon["z"]       = data.getZ();
//
//                    particles.append(singlebeacon);
//                }
//                magnetic.append(robotreading.getMag_x());
//                magnetic.append(robotreading.getMag_y());
//                magnetic.append(robotreading.getMag_z());
//
//                singlereading["beacons"]   = beacons;
//                singlereading["wifi"]      = wifi;
//                singlereading["particles"] = particles;
//                singlereading["magnetic"]  = magnetic;
//
//                readingValues.append(singlereading);
//            }
//        }


        std::ofstream file(this->savePath);
        Json::StyledStreamWriter writer;
        writer.write(file,readingValues);
        file.close();
    }
    return "";
}

void FastSlam::initSimulation(double x, double y, double z, double orientation) {
    this->robot = new Robot(x,y,z,orientation,this->geofence);
    this->robot->setValues(x, y, z, orientation);
    this->robot->setNoise(0.1,0.1,0.1,0.1);
    this->robot->setIsRobot(true);
    this->robot->setMappingNoise(0,0.0,0,0.0f,0.0);
    for(int j=0;j<particleSize;j++){
        double ori = 2*M_PI*this->randd();
        Robot* particle = new Robot(x, y, z, ori,this->geofence);
        //particle->setNoise(0.0,0,0.0,2.0);
        particle->setId(j);
        particle->setIsRobot(false);
        particle->setValues(x, y, z, ori);
        this->particles.push_back(particle);
    }



}

void FastSlam::setLandMarks(vector<Landmark> landmarks) {

    this->previousLandmarks = this->landmarks;

    this->landmarks = landmarks;

}

void FastSlam::moveRobotWorld(double distance, double turn) {
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "move called");
    this->robot = this->robot->robotMove(distance, turn);
    for(int i=0;i<particleSize;i++){
        Robot* particle = this->particles.at(i);
        int id = particle->getId();
        particle = particle->robotMove(distance, turn);
        this->particles.at(i) = particle;
    }
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "move ended");
}

void FastSlam::senseAndUpdate() {
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "update called");
    vector<vector<double>>obs = this->robot->sense(landmarks,minObservations,maxObservations);
    for(int i=0;i<particleSize;i++){
        Robot* particle = this->particles.at(i);
        particle = particle->robotUpdate(obs);
        this->particles.at(i) = particle;
    }
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "update ended");
}

double *FastSlam::getUserPosition(double* result) {
    double xMean = 0;
    double yMean = 0;
    double zMean = 0;
    double count = 0;

    double x = 0;
    double y = 0;
    double z = 0;

    double sumWeight = 0;

    for(int i=0;i<particleSize;i++){
        if(!std::isnan(particles.at(i)->getWeight())) {
            sumWeight += particles.at(i)->getWeight();
        }
    }

    if(sumWeight == 0){
        std::cout<<"catch here";
        __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "sum weight zero");
        sumWeight = 0.00001;
    }

    for (int i = 0; i < particleSize; i++) {
        double weight = particles.at(i)->getWeight();
        if(std::isnan(weight)){
            weight = 0;
        }
        double weightFactor = (weight / sumWeight);
        //double weightFactor = weight;
        if(std::isnan(weight/sumWeight)){
            std::cout<<"catch here";
        }
        count += weightFactor;
        xMean += particles.at(i)->getX() * weightFactor;
        yMean += particles.at(i)->getY() * weightFactor;
        zMean += particles.at(i)->getZ() * weightFactor;

        x += particles.at(i)->getX();
        y += particles.at(i)->getY();
        z += particles.at(i)->getZ();
    }

    if (count == 0) {
//        result[0] = x/particleSize;
//        result[1] = y/particleSize;
//        result[2] = z/particleSize;

        result[1] = -1;
        result[0] = -1;
        result[2] = -1;

        result[3] =  0.0;
        avoidReasample = true;
        __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "Count zero");
        return  result;
    }
    avoidReasample = false;

    xMean /= count;
    yMean /= count;
    zMean /= count;
    count = 0;
    for (int i = 0; i < particleSize; i++) {
        double euclidean = sqrt(pow(particles.at(i)->getX()- xMean, 2) +
                                     pow(particles.at(i)->getY() - yMean, 2) + pow(particles.at(i)->getZ() - zMean, 2));
        if (euclidean < 5) {
            count++;
        }
    }
//    if(Double.isNaN(xMean) || Double.isNaN(yMean) || Double.isNaN(zMean)){
//        System.out.println("xxx");
//    }

    float status = 1;
    if (count < (0.90 * particleSize)) {
        status = 0;
    }
    result[0] = xMean;
    result[1] = yMean;
    result[2] = zMean;
    result[3] = status;
    return result;

}

double *FastSlam::returnRobotPosition(double* result) {
    result[0]   = robot->getX();
    result[1]   = robot->getY();
    result[2]   = robot->getZ();

    return result;
}

void FastSlam::resampleParticles() {
    //check if no real observation was made
//    if(equal(this->previousLandmarks.begin(),this->previousLandmarks.end(),this->landmarks.begin(), []( Landmark& l1,  Landmark& l2){
//        double *pos = l1.getPos();
//        double *pos2 = l2.getPos();
//
//
//        bool pos_equal = pos[0] == pos2[0] && pos[1] == pos2[1] && pos[2] == pos2[2];
//        bool distance_equal = true;
//
//        return false;
//    })){
//        for(int j=0;j<particleSize;j++){
//            Robot* particle = this->particles.at(j);
//            particle->setWeight(1.0);
//            this->particles.at(j) = new Robot(*particle);
//        }
//        //don't resample
//        return;
//    }
    if(avoidReasample){

        for(int j=0;j<particleSize;j++){
            Robot* particle = this->particles.at(j);
            particle->setWeight(1.0);
            this->particles.at(j) = new Robot(*particle);
        }
        return;

    }
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "resample called");
   vector<Robot*> newparticles;
   vector<double>weights;
    for(int i=0;i<particleSize;i++){
        weights.push_back(particles.at(i)->getWeight());
    }

    int rand_index                    = (int)(((double) rand() / (RAND_MAX))*(particleSize));
    double beta                       = 0.0;
    double max_weight = *max_element(weights.begin(),weights.end());

    for(int i=0; i<particleSize; i++){
        beta    += ((randd())*(2.0)*(max_weight));
        while (beta >= weights.at(rand_index)){
            beta -= weights.at(rand_index);
            rand_index = (rand_index+1) % particleSize;
        }

        Robot* particle = particles.at(rand_index);
        particle->setWeight(1.0);
        newparticles.push_back(new Robot(*particle));
    }
    this->particles = newparticles;
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "resample ended");
}

void FastSlam::addNearestNeighboursToAll() {
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "neighbours called");
    if(this->isActive){

        vector<Particlepos>neighbours;

        Particlepos pos(this->robot->getX(), this->robot-> getY(), this->robot->getZ());
        neighbours.push_back(pos);

//        Particlepos pos_prev(this->robot->getPreviousX(), this->robot-> getPreviousY(), this->robot->getPreviousZ());
//        neighbours.push_back(pos_prev);




//        for (int i=0;i<particleSize;i++){
//            Robot* particle = this->particles.at(i);
//            particle->addMeasurement(this->bData, this->wData, this->mag_x, this->mag_y, this->mag_z,neighbours);
//            this->particles.at(i) = particle;
//        }
        Particlepos wayPointPos(this->wx, this->wy, this->wz);
        vector<Particlepos> wayPointLoc;
        wayPointLoc.push_back(wayPointPos);
        this->robot->addMeasurement(this->bData, this->wData, this->mag_x, this->mag_y, this->mag_z,neighbours, this->device_mag_x, this->device_mag_y, this->device_mag_z, wayPointLoc);
//        vector<Particlepos>particledetails;
//        for (int k=0;k<particleSize;k++){
//            Robot* particle = this->particles.at(k);
//            Particlepos pos(particle->getX(), particle->getY(), particle->getZ(), false);
//            particledetails.push_back(pos);
//        }
//        Particlepos rob(this->robot->getX(), this->robot->getY(), this->robot->getZ(), true);
//        particledetails.push_back(rob);
//        this->debugValues.push_back(particledetails);
    }
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "neighbours ended");

}

void FastSlam::setSensorReadings(vector<Beaconsdata> bData, vector<Wifidetails> wData, float mag_x,
                                 float mag_y, float mag_z, float device_mag_x, float device_mag_y, float device_mag_z, double wx, double wy, double wz) {

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem start data");
    if(bData.size() > 0){
        this->bData = bData;
    } else{
        this->bData.clear();
    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem beacon");
    if(wData.size() > 0){
        this->wData = wData;
    } else{
        this->wData.clear();
    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem wifi data");
    this->mag_x = mag_x;
    this->mag_y = mag_y;
    this->mag_z = mag_z;

    this->device_mag_x = device_mag_x;
    this->device_mag_y = device_mag_y;
    this->device_mag_z = device_mag_z;

    this->wx = wx;
    this->wy = wy;
    this->wz = wz;

}

void FastSlam::setRobotPosition(double x, double y, double z) {
    if(this->robot != nullptr) {
        this->robot->setMX(x);
        this->robot->setMY(y);
        this->robot->setMZ(z);
    }

}






