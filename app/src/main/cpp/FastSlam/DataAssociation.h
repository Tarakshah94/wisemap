//
// Created by Tarak on 15-10-2018.
//

#ifndef WISEMAP_DATAASSOCIATION_H
#define WISEMAP_DATAASSOCIATION_H

#include <string>
#include <string.h>


#include <vector>
using namespace std;
class DataAssociation {
//    typedef double *(*p_array)[2];
public:
    DataAssociation (vector<double > obs, int landmark_index, double probability);

    vector<double> getObservation();

    int getLandmark_index();

    double getProbability();

private:
    int landmark_index;
    double probability;
    vector<double> observation;
};


#endif //WISEMAP_DATAASSOCIATION_H
