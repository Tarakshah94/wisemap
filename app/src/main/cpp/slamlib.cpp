#include <jni.h>
#include <string>

#include <iostream>

#include "FastSlam.h"
#include "json/json.h"
#include "LocProvider.h"
#include "NeighbourMeasurements.h"

#include "models/Beaconsdata.h"
#include "models/Wifidetails.h"
#include "models/MagneticMap.h"

#include <android/log.h>
//
//#define LOGV(TAG,...) //__android_log_print(ANDROID_LOG_VERBOSE, TAG,__VA_ARGS__)
//#define LOGD(TAG,...) //__android_log_print(ANDROID_LOG_DEBUG  , TAG,__VA_ARGS__)
//#define LOGI(TAG,...) //__android_log_print(ANDROID_LOG_INFO   , TAG,__VA_ARGS__)
//#define LOGW(TAG,...) //__android_log_print(ANDROID_LOG_WARN   , TAG,__VA_ARGS__)
//#define LOGE(TAG,...) //__android_log_print(ANDROID_LOG_ERROR  , TAG,__VA_ARGS__)





//extern "C"
//JNIEXPORT void JNICALL
//Java_com_example_tarak_wisemap_slam_FastSlam_initFastSlam(JNIEnv *env, jobject instance,
//                                                          jobjectArray geofencearray,
//                                                          jint mParticleSize, jlong id, ) {
//
//
//    int len1         = env -> GetArrayLength(geofencearray);
//    vector<double*> geofenceVector;
//
//    for(int i=0; i<len1; ++i){
//        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
//        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
//
//        geofenceVector.push_back(element);
//
//        env->ReleaseDoubleArrayElements(oneDim, element, 0);
//        env->DeleteLocalRef(oneDim);
//    }
//    FastSlam* fasterSlam = (FastSlam*) id;
//    fasterSlam->initFastSlamGeofence(geofenceVector);
//    fasterSlam->initFastSlamParticleSize(mParticleSize);
//    //delete geofenceVector;
//    //cout<<mParticleSize;
//
//}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_setObservations(JNIEnv *env, jobject instance,
                                                             jint min, jint max, jlong id) {

    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->setObservationsThreshold(min,max);

}

//extern "C"
//JNIEXPORT void JNICALL
//Java_com_example_tarak_wisemap_slam_FastSlam_setStartStopNative(JNIEnv *env, jobject instance,
//                                                                jboolean flag, jlong id) {
//    FastSlam* fasterSlam = (FastSlam*) id;
//    string s = fasterSlam->setStartFlag(flag);
//    return env->NewStringUTF(s.c_str());
//}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_initSimulatorNative(JNIEnv *env, jobject instance,
                                                                 jdouble x, jdouble y, jdouble z,
                                                                 jdouble orientation, jlong id) {

    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->initSimulation(x, y, z, orientation);

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_setLandMarksNative(JNIEnv *env, jobject instance,
                                                                jobject landmarks, jlong id) {

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem landmarks landmarks start");
    jclass alCls = env->FindClass("java/util/ArrayList");
    jclass ptCls = env->FindClass("com/example/tarak/wisemap/models/LandMark");

    if (alCls == nullptr || ptCls == nullptr) {
        return;
    }

    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");
    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");
    jmethodID ptGetXId = env->GetMethodID(ptCls, "getmX", "()D");
    jmethodID ptGetYId = env->GetMethodID(ptCls, "getmY", "()D");
    jmethodID ptGetZId = env->GetMethodID(ptCls, "getmZ", "()D");
    jmethodID ptGetDistance = env->GetMethodID(ptCls, "getRssi", "()D");

    if (alGetId == nullptr || alSizeId == nullptr || ptGetXId == nullptr || ptGetYId == nullptr || ptGetZId ==
                                                                                                           nullptr) {
        env->DeleteLocalRef(alCls);
        env->DeleteLocalRef(ptCls);
        return;
    }

    int landmarksCount = static_cast<int>(env->CallIntMethod(landmarks, alSizeId));

    if (landmarksCount < 1) {
        env->DeleteLocalRef(alCls);
        env->DeleteLocalRef(ptCls);
        return;
    }



    vector<Landmark> landmarksVector;
    landmarksVector.reserve(landmarksCount);
    double x, y, z, distance;
    for (int i = 0; i < landmarksCount; ++i) {
        jobject point = env->CallObjectMethod(landmarks, alGetId, i);
        x = static_cast<double>(env->CallDoubleMethod(point, ptGetXId));
        y = static_cast<double>(env->CallDoubleMethod(point, ptGetYId));
        z = static_cast<double>(env->CallDoubleMethod(point, ptGetZId));
        distance = static_cast<double>(env->CallDoubleMethod(point, ptGetDistance));
        env->DeleteLocalRef(point);
        Landmark lm(x, y, z);
        lm.setDistance(distance);
        landmarksVector.push_back(lm);
    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem landmarks before update");
    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->setLandMarks(landmarksVector);
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem landmarks after update");
    //delete landmarksVector;

    env->DeleteLocalRef(alCls);
    env->DeleteLocalRef(ptCls);

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem landmarks end");

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_moveRobotWorldNative(JNIEnv *env, jobject instance,
                                                                  jdouble distance, jdouble turn, jlong id) {

    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->moveRobotWorld(distance, turn);

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_senseAndUpdateNative(JNIEnv *env, jobject instance, jlong id) {

    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->senseAndUpdate();

}

extern "C"
JNIEXPORT jdoubleArray JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_getUserPositionNative(JNIEnv *env, jobject instance, jlong id) {
    FastSlam* fasterSlam = (FastSlam*) id;
    double userpos[4];
    fasterSlam->getUserPosition(userpos);
    jdoubleArray  returnarray = env->NewDoubleArray(4);
    env->SetDoubleArrayRegion(returnarray,0,4,userpos);
    return returnarray;

}

extern "C"
JNIEXPORT jdoubleArray JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_getRobotPositionNative(JNIEnv *env, jobject instance, jlong id) {
    FastSlam* fasterSlam = (FastSlam*) id;
    double robotpos[3];
    fasterSlam->returnRobotPosition(robotpos);
    jdoubleArray  returnarray = env->NewDoubleArray(3);
    env->SetDoubleArrayRegion(returnarray,0,3,robotpos);
    return returnarray;

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_resampleParticlesNative(JNIEnv *env,
                                                                     jobject instance, jlong id) {
    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->resampleParticles();

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_addNearestNeighboursNative(JNIEnv *env,
                                                                        jobject instance, jlong id) {

    FastSlam* fasterSlam = (FastSlam*) id;
    fasterSlam->addNearestNeighboursToAll();

}



extern "C"
JNIEXPORT jlong JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_initSlamNative(JNIEnv *env, jobject obj) {

    FastSlam *fasterslam = new FastSlam();
    return (long) fasterslam;

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_setSensorReadingsNative(JNIEnv *env, jobject instance,
                                                                     jobject beaconsData,
                                                                     jobject wifiDetails,
                                                                     jfloatArray magValues_,
                                                                     jfloatArray deviceMagValues_,
                                                                     jlong address, jdoubleArray waypointloc_) {
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem init");
    float *magValues = env->GetFloatArrayElements(magValues_, 0);

    float mag_x      = magValues[0];
    float mag_y      = magValues[1];
    float mag_z      = magValues[2];
    env->ReleaseFloatArrayElements(magValues_, magValues, 0);
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem after init");

    float *deviceMagValues = env->GetFloatArrayElements(deviceMagValues_, 0);

    float device_mag_x      = deviceMagValues[0];
    float device_mag_y      = deviceMagValues[1];
    float device_mag_z      = deviceMagValues[2];
    env->ReleaseFloatArrayElements(deviceMagValues_, deviceMagValues, 0);


    double  *waypointValues = env->GetDoubleArrayElements(waypointloc_, 0);
    double wayx = waypointValues[0];
    double wayy = waypointValues[1];
    double wayz = waypointValues[2];
    env->ReleaseDoubleArrayElements(waypointloc_, waypointValues, 0);

    // TODO


    jclass alCls     = env->FindClass("java/util/ArrayList");
    jclass beaconCls = env->FindClass("com/example/tarak/wisemap/models/BeaconsData");
    jclass wifiCls   = env->FindClass("com/example/tarak/wisemap/models/WifiDetails");

    if (alCls == nullptr || beaconCls == nullptr || wifiCls == nullptr) {
        return;
    }
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem class");
    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");
    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");


    jmethodID beaconGetDistance   = env->GetMethodID(beaconCls, "getBeacon_distance", "()D");
    jmethodID beaconKalmanGetDistance   = env->GetMethodID(beaconCls, "getBeacon_kalman_distance", "()D");
    jmethodID beaconGetPosition   = env->GetMethodID(beaconCls, "getBeaconPosition", "()[D");
    jmethodID beaconGetRssi       = env->GetMethodID(beaconCls, "getBeaconRssi", "()I");

    jmethodID ptGetX = env->GetMethodID(beaconCls, "getX", "()D");
    jmethodID ptGetY = env->GetMethodID(beaconCls, "getY", "()D");
    jmethodID ptGetZ = env->GetMethodID(beaconCls, "getZ", "()D");


    jmethodID wifiRssi            = env->GetMethodID(wifiCls, "getRssi", "()I");
    jmethodID wifiBssi            = env->GetMethodID(wifiCls, "getBssid", "()Ljava/lang/String;");







    if (alGetId == nullptr || alSizeId == nullptr || beaconGetDistance == nullptr || beaconGetPosition == nullptr || beaconGetRssi ==
                                                                                                   nullptr || wifiRssi == nullptr || beaconKalmanGetDistance == nullptr) {
        env->DeleteLocalRef(alCls);
        env->DeleteLocalRef(beaconCls);
        env->DeleteLocalRef(wifiCls);
        return;
    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem methods");

    int beaconsCount = static_cast<int>(env->CallIntMethod(beaconsData, alSizeId));
    int wifiCount    = static_cast<int>(env->CallIntMethod(wifiDetails, alSizeId));

//    if (beaconsCount < 1 || wifiCount < 1) {
//        env->DeleteLocalRef(alCls);
//        env->DeleteLocalRef(ptCls);
//        return;
//    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem count");
    vector<Beaconsdata> beaconsVector;
    vector<Wifidetails> wifiVector;

    if(beaconsCount > 0){
        beaconsVector.reserve(beaconsCount);
    }
    if(wifiCount > 0){
        wifiVector.reserve(wifiCount);
    }


    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem reserve");
    if(beaconsCount > 0) {
        double distance;
        double kalmandistance;
        double position_x;
        double position_y;
        double position_z;

        int rssi;
        for (int i = 0; i < beaconsCount; ++i) {
            jobject point = env->CallObjectMethod(beaconsData, alGetId, i);
            distance = static_cast<double>(env->CallDoubleMethod(point, beaconGetDistance));
            kalmandistance = static_cast<double>(env->CallDoubleMethod(point, beaconKalmanGetDistance));
            rssi = static_cast<int>(env->CallIntMethod(point, beaconGetRssi));

            position_x = static_cast<double>(env->CallDoubleMethod(point, ptGetX));
            position_y = static_cast<double>(env->CallDoubleMethod(point, ptGetY));
            position_z = static_cast<double>(env->CallDoubleMethod(point, ptGetZ));

            beaconsVector.push_back(
                    Beaconsdata(distance, position_x, position_y, position_z, rssi, kalmandistance));



            env->DeleteLocalRef(point);
        }

    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem beacon");

    if(wifiCount > 0) {
        int wifirssi;
        string id;
        for (int i = 0; i < wifiCount; ++i) {
            jobject point = env->CallObjectMethod(wifiDetails, alGetId, i);
            wifirssi = static_cast<int>(env->CallIntMethod(point, wifiRssi));
            jstring rv = (jstring)env->CallObjectMethod(point, wifiBssi, 0);
           const char *strReturn = env->GetStringUTFChars(rv, 0);
//        env->DeleteLocalRef(point);
            wifiVector.push_back(Wifidetails(wifirssi, strReturn));
        }
    }

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem wifi");

    FastSlam* fasterSlam = (FastSlam*) address;
    fasterSlam->setSensorReadings(beaconsVector, wifiVector, mag_x, mag_y, mag_z, device_mag_x, device_mag_y, device_mag_z, wayx, wayy, wayz);

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem update");
    env->DeleteLocalRef(alCls);
    env->DeleteLocalRef(beaconCls);
    env->DeleteLocalRef(wifiCls);
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem delete");




}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_initFastSlam(JNIEnv *env, jobject instance,
                                                          jobjectArray geofencearray, jint mParticleSize,
                                                          jlong address, jstring savePath_) {
    const char *savePath = env->GetStringUTFChars(savePath_, 0);


    int len1         = env -> GetArrayLength(geofencearray);
    vector<vector<double>> geofenceVector;

    for(int i=0; i<len1; ++i){
        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
        vector<double> inside_elem;
        inside_elem.push_back(element[0]);
        inside_elem.push_back(element[1]);
        geofenceVector.push_back(inside_elem);

        env->ReleaseDoubleArrayElements(oneDim, element, 0);

        env->DeleteLocalRef(oneDim);
    }
    double val = geofenceVector.at(0)[0];
    double val2 = geofenceVector.at(1)[0];
    FastSlam* fasterSlam = (FastSlam*) address;
    fasterSlam->initFastSlamGeofence(geofenceVector,savePath);
    fasterSlam->initFastSlamParticleSize(mParticleSize);



    env->ReleaseStringUTFChars(savePath_, savePath);
}extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_invalidateNative(JNIEnv *env, jobject instance,
                                                              jlong address) {

    // TODO
    FastSlam* fasterSlam = (FastSlam*) address;
    delete fasterSlam;

}extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_setStartStopNative(JNIEnv *env, jobject instance,
                                                                jboolean flag, jlong address) {

    // TODO
    FastSlam* fasterSlam = (FastSlam*) address;
    string s = fasterSlam->setStartFlag(flag);
    return env->NewStringUTF(s.c_str());


}



extern "C"
JNIEXPORT jlong JNICALL
Java_com_example_tarak_wisemap_managers_KDtreeManager_nativeInitlocProvider(JNIEnv *env,
                                                                            jobject instance) {

    // TODO

    LocProvider* locProvider = new LocProvider();
    return (long) locProvider;

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_managers_KDtreeManager_deleteNativeReference(JNIEnv *env,
                                                                            jobject instance,
                                                                            jlong address) {

    // TODO
    LocProvider* locProvider = (LocProvider*) address;
    delete locProvider;
}




//extern "C"
//JNIEXPORT jdoubleArray JNICALL
//Java_com_example_tarak_wisemap_managers_KDtreeManager_nativeMoveSlaves(JNIEnv *env,
//                                                                       jobject instance,
//                                                                       jdouble distance,
//                                                                       jdouble turn,
//                                                                       jdoubleArray observations_,
//                                                                       jlong address, jint length) {
//    jdouble* observations = env->GetDoubleArrayElements(observations_, NULL);
//
//    vector<double> sensed(observations,observations+length);
//    // TODO
//    LocProvider* locProvider = (LocProvider*) address;
//    double userpos[4];
//    locProvider->move(distance, turn, sensed, userpos);
//    jdoubleArray  returnarray = env->NewDoubleArray(4);
//    env->SetDoubleArrayRegion(returnarray,0,4,userpos);
//    env->ReleaseDoubleArrayElements(observations_, observations, 0);
//    return returnarray;
//}

extern "C"
JNIEXPORT jdoubleArray JNICALL
Java_com_example_tarak_wisemap_managers_KDtreeManager_nativeMoveSlaves(JNIEnv *env,
                                                                       jobject instance,
                                                                       jdouble distance,
                                                                       jdouble turn,
                                                                       jdoubleArray magobservations_,
                                                                       jlong address,
                                                                       jint maglength,
                                                                       jdoubleArray beaconsobservations_,
                                                                       jboolean isBeacon,
                                                                       jdoubleArray wifiobservations_,
                                                                       jboolean isWifi,jint beaconssize) {
    jdouble *magobservations = env->GetDoubleArrayElements(magobservations_, NULL);
    jdouble *beaconsobservations = env->GetDoubleArrayElements(beaconsobservations_, NULL);
    jdouble *wifiobservations = env->GetDoubleArrayElements(wifiobservations_, NULL);

    vector<double>beacon;
    vector<double>wifi;

    vector<double> magnetic(magobservations,magobservations+maglength);

    if(isBeacon) {
        beacon  = vector<double>(beaconsobservations, beaconsobservations + 3);
    }
    if(isWifi){
        wifi    = vector<double>(wifiobservations, wifiobservations+2);
    }


    // TODO
    LocProvider* locProvider = (LocProvider*) address;
    double userpos[4];
    locProvider->move(distance, turn, magnetic, userpos, beacon, isBeacon, wifi, isWifi, beaconssize);
    jdoubleArray  returnarray = env->NewDoubleArray(4);
    env->SetDoubleArrayRegion(returnarray,0,4,userpos);


    env->ReleaseDoubleArrayElements(magobservations_, magobservations, 0);
    env->ReleaseDoubleArrayElements(beaconsobservations_, beaconsobservations, 0);
    env->ReleaseDoubleArrayElements(wifiobservations_, wifiobservations, 0);
    return returnarray;


}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_example_tarak_wisemap_managers_KDtreeManager_getParticlesLocations(JNIEnv *env,
                                                                            jobject instance,
                                                                            jlong address) {

    // TODO
    LocProvider* locProvider = (LocProvider*) address;
    double particlePos[100][3];
    locProvider->getParticleLocations(particlePos);

    jclass doubleArray1DClass = env->FindClass("[D");
    jobjectArray retData = env->NewObjectArray(
            100, doubleArray1DClass, NULL);;
    for(int i=0;i<100;i++){
        jdoubleArray particleloc = env->NewDoubleArray(3);
        env->SetDoubleArrayRegion(particleloc,0,3,particlePos[i]);
        env->SetObjectArrayElement(retData, i, particleloc);
    }

    return retData;
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_managers_KDtreeManager_initNativeLocalizerAndMapper(JNIEnv *env,
                                                                                   jobject instance,
                                                                                   jlong address,
                                                                                   jint dimensions,
                                                                                   jobject measurements,
                                                                                   jint particleSize,
                                                                                   jobjectArray geofencearray,
                                                                                   jobject magMap) {

    // TODO
    // TODO
    LocProvider* locProvider = (LocProvider*) address;

    int len1         = env -> GetArrayLength(geofencearray);
    vector<vector<double>> geofenceVector;

    for(int i=0; i<len1; ++i){
        jdoubleArray oneDim= (jdoubleArray)env->GetObjectArrayElement(geofencearray, i);
        jdouble *element=env->GetDoubleArrayElements(oneDim, 0);
        vector<double> inside_elem;
        inside_elem.push_back(element[0]);
        inside_elem.push_back(element[1]);
        geofenceVector.push_back(inside_elem);

        env->ReleaseDoubleArrayElements(oneDim, element, 0);

        env->DeleteLocalRef(oneDim);
    }


    jclass alCls     = env->FindClass("java/util/ArrayList");
    //jclass pojoCls = env->FindClass("com/example/tarak/wisemap/models/MeasurementsPojo");
    jclass magMapPojoCls = env->FindClass("com/example/tarak/wisemap/models/MagneticMapPojo");



    if (alCls == nullptr || magMapPojoCls == nullptr) {
        return;
    }
    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem class");
    jmethodID alGetId  = env->GetMethodID(alCls, "get", "(I)Ljava/lang/Object;");
    jmethodID alSizeId = env->GetMethodID(alCls, "size", "()I");





    //magneticmap methods
    jmethodID getMX        = env->GetMethodID(magMapPojoCls, "getXMag", "()D");
    jmethodID getMY       = env->GetMethodID(magMapPojoCls, "getYMag", "()D");
    jmethodID getWayX           = env->GetMethodID(magMapPojoCls, "getParticleX", "()D");
    jmethodID getWayY           = env->GetMethodID(magMapPojoCls, "getParticleY", "()D");
    jmethodID getWayZ           = env->GetMethodID(magMapPojoCls, "getParticleZ", "()D");
    jmethodID getMZ           = env->GetMethodID(magMapPojoCls, "getZMag", "()D");






    int measurementCount = static_cast<int>(env->CallIntMethod(measurements, alSizeId));
    int magneticMapCount = static_cast<int>(env->CallIntMethod(magMap, alSizeId));

    //__android_log_print(ANDROID_LOG_VERBOSE, "Debug", "No problem count");
    //vector<NeighbourMeasurements> neighMeasurements;
    vector<MagneticMap> magneticMap;



    double mag_x;
    double mag_y;
    double wayX;
    double wayY;
    double wayZ;
    double mag_z;

    for(int j=0; j<magneticMapCount; j++){
        jobject point = env->CallObjectMethod(magMap, alGetId, j);
        mag_x       = static_cast<double>(env->CallDoubleMethod(point, getMX));
        mag_y       = static_cast<double>(env->CallDoubleMethod(point, getMY));
        wayX          = static_cast<double>(env->CallDoubleMethod(point, getWayX));
        wayY          = static_cast<double>(env->CallDoubleMethod(point, getWayY));
        wayZ          = static_cast<double>(env->CallDoubleMethod(point, getWayZ));
        mag_z          = static_cast<double>(env->CallDoubleMethod(point, getMZ));

        magneticMap.push_back(MagneticMap(mag_x, mag_y, mag_z, wayX, wayY, wayZ));
        env->DeleteLocalRef(point);
    }


    locProvider->initLocalizerAndMapper(dimensions, particleSize, geofenceVector, magneticMap);


    env->DeleteLocalRef(alCls);
    //env->DeleteLocalRef(pojoCls);
    env->DeleteLocalRef(magMapPojoCls);

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_tarak_wisemap_slam_FastSlam_setRobotPositionNative(JNIEnv *env, jobject instance,
                                                                    jdouble x, jdouble y, jdouble z,
                                                                    jlong address) {

    // TODO
    FastSlam* fasterSlam = (FastSlam*) address;
    fasterSlam->setRobotPosition(x, y, z);
}


