//
// Created by noob on 15/11/18.
//

#ifndef WISEMAP_MEASUREMENT_H
#define WISEMAP_MEASUREMENT_H

#include <vector>
#include "Beaconsdata.h"
#include "Wifidetails.h"
#include "Particlepos.h"

class measurement {
private:
    vector<Beaconsdata> bData;
    vector<Wifidetails> wData;
    vector<Particlepos> particles;
    vector<Particlepos> waypoint;
    float mag_x;
    float mag_y;
    float mag_z;
    float device_mag_x;
    float device_mag_y;
    float device_mag_z;
public:
    float getDevice_mag_x() const {
        return device_mag_x;
    }

    void setDevice_mag_x(float device_mag_x) {
        measurement::device_mag_x = device_mag_x;
    }

    float getDevice_mag_y() const {
        return device_mag_y;
    }

    void setDevice_mag_y(float device_mag_y) {
        measurement::device_mag_y = device_mag_y;
    }

    float getDevice_mag_z() const {
        return device_mag_z;
    }

    void setDevice_mag_z(float device_mag_z) {
        measurement::device_mag_z = device_mag_z;
    }

public:
    measurement(const vector<Beaconsdata> &bData, const vector<Wifidetails> &wData,
                const vector<Particlepos> &particles, float mag_x, float mag_y, float mag_z, float device_mag_x, float device_mag_y, float device_mag_z,vector<Particlepos> waypoint)
            : bData(bData), wData(wData), particles(particles), mag_x(mag_x), mag_y(mag_y),
              mag_z(mag_z), device_mag_x(device_mag_x), device_mag_y(device_mag_y), device_mag_z(device_mag_z), waypoint(waypoint){}

    const vector<Beaconsdata> &getBData() const {
        return bData;
    }

    void setBData(const vector<Beaconsdata> &bData) {
        measurement::bData = bData;
    }

    const vector<Wifidetails> &getWData() const {
        return wData;
    }

    void setWData(const vector<Wifidetails> &wData) {
        measurement::wData = wData;
    }

    const vector<Particlepos> &getParticles() const {
        return particles;
    }

    const vector<Particlepos> &getWaypoint() const {
        return waypoint;
    }

    void setParticles(const vector<Particlepos> &particles) {
        measurement::particles = particles;
    }

    float getMag_x() const {
        return mag_x;
    }

    void setMag_x(float mag_x) {
        measurement::mag_x = mag_x;
    }

    float getMag_y() const {
        return mag_y;
    }

    void setMag_y(float mag_y) {
        measurement::mag_y = mag_y;
    }

    float getMag_z() const {
        return mag_z;
    }

    void setMag_z(float mag_z) {
        measurement::mag_z = mag_z;
    }
};


#endif //WISEMAP_MEASUREMENT_H
