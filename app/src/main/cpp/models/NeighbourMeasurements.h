//
// Created by noob on 5/3/19.
//

#ifndef WISEMAP_NEIGHBOURMEASUREMENTS_H
#define WISEMAP_NEIGHBOURMEASUREMENTS_H


class NeighbourMeasurements {
private:
    double magnitude;

private:
    double magnitude_world;
    double beacon_x;
    double beacon_y;
    double beacon_z;
    double particle_x;
    double particle_y;
    double particle_z;
    double xy_mag;
    double wifi_mean;
    double wifi_std;
public:
    double getWifi_mean() const {
        return wifi_mean;
    }

    void setWifi_mean(double wifi_mean) {
        NeighbourMeasurements::wifi_mean = wifi_mean;
    }

    double getWifi_std() const {
        return wifi_std;
    }

    void setWifi_std(double wifi_std) {
        NeighbourMeasurements::wifi_std = wifi_std;
    }

public:
    double getXy_mag() const {
        return xy_mag;
    }

    void setXy_mag(double xy_mag) {
        NeighbourMeasurements::xy_mag = xy_mag;
    }

public:
    double getMagnitude() const {
        return magnitude;
    }

    void setMagnitude(double magnitude) {
        NeighbourMeasurements::magnitude = magnitude;
    }

    double getMagnitude_world() const {
        return magnitude_world;
    }

    void setMagnitude_world(double magnitude_world) {
        NeighbourMeasurements::magnitude_world = magnitude_world;
    }

    double getBeacon_x() const {
        return beacon_x;
    }

    void setBeacon_x(double beacon_x) {
        NeighbourMeasurements::beacon_x = beacon_x;
    }

    double getBeacon_y() const {
        return beacon_y;
    }

    void setBeacon_y(double beacon_y) {
        NeighbourMeasurements::beacon_y = beacon_y;
    }

    double getBeacon_z() const {
        return beacon_z;
    }

    void setBeacon_z(double beacon_z) {
        NeighbourMeasurements::beacon_z = beacon_z;
    }

    double getParticle_x() const {
        return particle_x;
    }

    void setParticle_x(double particle_x) {
        NeighbourMeasurements::particle_x = particle_x;
    }

    double getParticle_y() const {
        return particle_y;
    }

    void setParticle_y(double particle_y) {
        NeighbourMeasurements::particle_y = particle_y;
    }

    double getParticle_z() const {
        return particle_z;
    }

    void setParticle_z(double particle_z) {
        NeighbourMeasurements::particle_z = particle_z;
    }
public:
    NeighbourMeasurements(double magnitude, double magnitude_world, double beacon_x,
                          double beacon_y, double beacon_z, double particle_x, double particle_y,
                          double particle_z, double xymag, double wifi_mean, double wifi_std) : magnitude(magnitude),
                                               magnitude_world(magnitude_world), beacon_x(beacon_x),
                                               beacon_y(beacon_y), beacon_z(beacon_z),
                                               particle_x(particle_x), particle_y(particle_y),
                                               particle_z(particle_z),xy_mag(xymag),wifi_mean(wifi_mean),wifi_std(wifi_std) {}






};


#endif //WISEMAP_NEIGHBOURMEASUREMENTS_H
