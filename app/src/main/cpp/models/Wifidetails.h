//
// Created by noob on 14/11/18.
//

#ifndef WISEMAP_WIFIDETAILS_H
#define WISEMAP_WIFIDETAILS_H

#include "string"
using namespace std;

class Wifidetails {

private:
    int rssi;
    string bssid;

public:
//    Wifidetails(int rssi, const string &bssid) : rssi(rssi), bssid(bssid) {}

    Wifidetails(int rssi,string bssid) : rssi(rssi),bssid(bssid) {}

    int getRssi() const {
        return rssi;
    }

    void setRssi(int rssi) {
        Wifidetails::rssi = rssi;
    }

    const string &getBssid() const {
        return bssid;
    }

    void setBssid(const string &bssid) {
        Wifidetails::bssid = bssid;
    }

};


#endif //WISEMAP_WIFIDETAILS_H
