//
// Created by noob on 15/11/18.
//

#ifndef WISEMAP_PARTICLEPOS_H
#define WISEMAP_PARTICLEPOS_H


class Particlepos {
private:
    double x;
    double y;
    double z;
    bool isRobot;
public:
    bool isIsRobot() const {
        return isRobot;
    }

    void setIsRobot(bool isRobot) {
        Particlepos::isRobot = isRobot;
    }

public:
    Particlepos(double x, double y, double z) : x(x), y(y), z(z) {}
    Particlepos(double x, double y, double z, bool isRobot) : x(x), y(y), z(z) ,isRobot(isRobot){}




    double getX()  {
        return x;
    }

    void setX(double x) {
        this->x = x;
    }

    double getY()  {
        return y;
    }

    void setY(double y) {
        this->y = y;
    }

    double getZ()  {
        return z;
    }

    void setZ(double z) {
        this->z = z;
    }
};


#endif //WISEMAP_PARTICLEPOS_H
