//
// Created by noob on 5/3/19.
//

#ifndef WISEMAP_LOCPROVIDER_H
#define WISEMAP_LOCPROVIDER_H

#include <map>
#include "Particle.h"
#include <math.h>
#include <NeighbourMeasurements.h>
#include "PCA.h"
#include "KDTree.h"
#include "IntervalTree.h"
#include "MagneticMap.h"
using namespace std;
using namespace Eigen;


class LocProvider {
private:
    int dimensions;
    int particleSize;
    vector<vector<double>>geofence;
    map<string,MagneticMap> measurementsHash;
    map<string,NeighbourMeasurements> beaconHash;
//    tree for magnetic and wifi readings
    KDTree neighboursTree;
//    tree for beacons
    KDTree beaconNeigboursTree;
    vector<NeighbourMeasurements>listMeasurements;
    vector<Particle*> slaves;
    double FORWARD_NOISE = 0.1;
    double TURN_NOISE    = 20;
    double unCertainity = 0;
    double previousHeading = -1;
    bool avoidReasample;
    int steps = 0;
    int randomCount = 0;
    double xPos,yPos,zPos;
    IntervalTree* augmentedTree;
    vector<double> beaconprediction;
    vector<double> magPrediction;


public:
    void initLocalizerAndMapper(int dimensions, int particleSize, vector<vector<double>> geofence,
                                    vector<MagneticMap> magneticMapData);



    void initParticleFilter(vector<vector<double>> geofence);
    void reInitParticleFilter(vector<vector<double>> geofence);
    double randd() {
        return (double)rand() / (RAND_MAX + 1.0);
    }
    void move(double distance, double turn, vector<double> measurements, double position[], vector<double> beaconmsts, bool isBeacon, vector<double> wifimsts, bool isWifi, int beaconSize);
    void getNearestReadings(vector<double> obs, double returnValue[]);
    vector<double> getNearestReadingsBeacon(vector<double> sensed);
    void getUserPosition(vector<double> weights,vector<Particle*>slaves, double position[]);
    void reSample(vector<double> weights,vector<Particle*>slaves);
    double returnMax(vector<double> weights);
    double returnMin(vector<double> weights);
    void calculateBias(double turnInDegrees);
    void getParticleLocations(double locs[100][3]);



    void removeDuplicatesReverse(vector<MagneticMap> measurements);
    void removeDuplicates(vector<NeighbourMeasurements> measurements);

    void buildTreeReverse(vector<point_t> vector);
    void buildTree(vector<point_t> vector);

    void insertMagneticMapIntoTree(vector<MagneticMap> vector);
    //void getWayPointsLoc(double minimum, double maximum, vector<vector<double>> result);


    void getUserCorrectedPosition(double d, vector<double> beacon, vector<double> magnetic,
                                  double pDouble[2]);

    void normalizeWeightsForSlaves();

    void checkNeedForResample();
};


#endif //WISEMAP_LOCPROVIDER_H
