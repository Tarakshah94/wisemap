//
// Created by noob on 5/3/19.
//

#include "Particle.h"


Particle::Particle(double x, double y, double z, double heading, vector <vector<double>> geofence) {
    this->mX = x;
    this->mY = y;
    this->mZ = z;



    this->orientation = heading;
    this->geofence = geofence;
    mX = adjustCoordinates(mX, 0);
    mY = adjustCoordinates(mY, 1);
    mZ = adjustCoordinates(mZ, 2);
}

Particle::Particle(const Particle &robot) {
    this->mX          = robot.mX;
    this->mY          = robot.mY;
    this->mZ          = robot.mZ;
    this->orientation =  robot.orientation;
    this->geofence    = robot.geofence;
    this->forward_noise = robot.forward_noise;
    this->turn_noise  = robot.turn_noise;
    this->weight      = robot.weight;

}

void Particle::setValues(double x, double y, double z, double orientation) {
    if(x < this->geofence.at(0).at(1) || x > this->geofence.at(0).at(0)){
        this->mX = x;
    } else{
        throw "Index out of bounds: X";
    }

    if(y < this->geofence.at(1).at(1) || y > this->geofence.at(1).at(0)){
        this->mY = y;
    } else{
        throw "Index out of bounds: Y";
    }

    if(z < this->geofence.at(2).at(1) || z > this->geofence.at(2).at(0)){
        this->mZ = z;
    } else{
        throw "Index out of bounds: Z";
    }

    if (!(orientation < 0 ||  orientation > 2*M_PI)){
        this->orientation = orientation;
    } else{
        if(orientation < 0){
            orientation += 2 * M_PI;
            this->orientation = orientation;
        } else{
            orientation -= 2 * M_PI;
            this->orientation = orientation;
        }
    }

}

void Particle::setNoise(double forward_noise, double bearing_noise) {
    this->forward_noise = forward_noise;
    this->turn_noise = bearing_noise;
}

double Particle::getX() {
    return this->mX;
}

double Particle::getY() {
    return this->mY;
}

double Particle::getZ() {
    return this->mZ;
}

double Particle::getOrientation() {
    return this->orientation;
}

double Particle::adjustCoordinates(double value, int index) {
    double adjustedvalue = value;
    if (value < this->geofence.at(index).at(0)){
        adjustedvalue = this->geofence.at(index).at(1) - (this->geofence.at(index).at(0) - value);
    } else if(value > this->geofence.at(index).at(1)){
        adjustedvalue = this->geofence.at(index).at(0) + (value - this->geofence.at(index).at(1));
    }

    return adjustedvalue;
}

Particle* Particle::move(double distance, double turn, std::normal_distribution<double> turn_distribution, std::default_random_engine generator) {
    //std::normal_distribution<double> turn_distribut(0, this->turn_noise * M_PI/180);
    double noise   = turn_distribution(generator);

    std::normal_distribution<double> forward_distribution(0, this->forward_noise);
    double noise2 = forward_distribution(generator);

    double heading = degToRad(turn) + noise;
    heading        = fmod(heading,2 * M_PI);
    double distance_travelled = distance+noise2;
    double newPosition[3];
    nextPosition(distance_travelled, radToDeg(heading),newPosition);
    mX = adjustCoordinates(newPosition[0], 0);
    mY = adjustCoordinates(newPosition[1], 1);
    mZ = adjustCoordinates(newPosition[2], 2);
    orientation = heading;
    Particle* newRobot = new Particle(*this);
    return newRobot;
}



double Particle::radToDeg(double deg) {
    if(deg < 0){
        deg += 2 * M_PI;
    }else if(deg > 2*M_PI){
        deg -= 2*M_PI;
    }
    double radians =  deg * 180 / M_PI;
    return radians;


}

double Particle::degToRad(double deg) {
    return (deg * M_PI)/180;
}

void Particle::nextPosition(double meters, double heading, double returnValue[]) {
    double position[2];
    double coords[] = {this->mX, this->mY, this->mZ};
    convertCartesianToSpherical(coords,position);
    double latLng[2];
    distance(position, meters, heading,latLng);

    convertSphericalToCartesian(latLng[0], latLng[1],returnValue);
}

void  Particle::convertSphericalToCartesian(double latitude, double longitude, double returnValue[]) {

    double earthRadius = 6371000; //radius in m
    double lat = degToRad(latitude);
    double lon = degToRad(longitude);
    double x = earthRadius * cos(lat) * cos(lon);
    double y = earthRadius * cos(lat) * sin(lon);
    double z = earthRadius * sin(lat);
    returnValue[0] =  x;
    returnValue[1] =  y;
    returnValue[2] =  z;
}

void Particle::distance(double *from, double distance, double heading, double latlng[]) {
    distance    /= 6371000;
    heading      = degToRad(heading);
    double fromLat = degToRad(from[0]);
    double fromLng = degToRad(from[1]);
    double cosdistance = cos(distance);
    double sindistance = sin(distance);


    double sinFromLat = sin(fromLat);
    double cosFromLat = cos(fromLat);
    double sinLat = cosdistance * sinFromLat + sindistance * cosFromLat * cos(heading);
    double dLng = atan2(
            sindistance * cosFromLat * sin(heading),
            cosdistance - sinFromLat * sinLat);
    latlng[0] = radToDeg(asin(sinLat));
    latlng[1] = radToDeg(fromLng + dLng);
}

void Particle::convertCartesianToSpherical(double *cartesian, double latlng[]) {
    double radius = sqrt((cartesian[0] * cartesian[0])+(cartesian[1] * cartesian[1])+(cartesian[2] * cartesian[2]));
    double lat = radToDeg(asin(cartesian[2] / radius));
    double lng = radToDeg(atan2(cartesian[1], cartesian[0]));
    latlng[0] = lat;
    latlng[1] = lng;
}

double Particle::gaussian(double mu, double sigma, double value) {
    double numerator = -1*pow((mu - value)/sigma ,2)*0.5;
    numerator        = exp(numerator);
    double denominator      = sqrt(2 * M_PI)*sigma+0.00000000000000000000000000005;

    return numerator/denominator;
}

double Particle::measurementProbability(vector<double> sensed, double readings[]) {
    double probability = 1.0;
//    bool notPresent = true;
//    if(readings[0] != 0){
//        notPresent = false;
//    }
//
//    if(notPresent){
//        return exp(-50);
//    }
    int length = sensed.size();
    double sigma = 2.0;

    double squaredDiff = 0.0;
    for (int i = 0; i < length; i++) {
        double val  = sensed.at(i);
        double val_ = readings[i];
        double diff = pow((sensed.at(i)-readings[i]),2);
        double gauss = gaussian(0.0,sigma,sqrt(diff))+exp(-70);
        double prob  = pow(gauss, 1.0/length);
        probability *= prob;
        squaredDiff += diff;
    }

//    double exponent = 1.0/length;
//
//
//    double gauss = gaussian(0.0,sigma,sqrt(squaredDiff))+exp(-70);
//    double prob  = pow(gauss, exponent);
//    probability *= prob;
    //probability += 0.0000000000001;
    this->weight *= probability;
    return probability;
}

double Particle::measurementProbabilityBeacon(vector<double> sensed, vector<double> readings) {
    double probability = 1.0;
    int length = readings.size();
    bool notPresent = true;
    int readingsSize = sensed.size();
    for(int i=0;i<readingsSize;i++){
        if(sensed.at(i) != 0){
            notPresent = false;
        }
    }

    if(notPresent){
        return exp(-10);
    }

    double sigma = 2.0;

    double squaredDiff = 0.0;
    for (int i = 0; i < length; i++) {
        squaredDiff += pow(sensed[i]-readings[i],2);
    }

    double exponent = 1.0/length;


    double gauss = gaussian(0.0,sigma,sqrt(squaredDiff))+exp(-70);
    double prob  = pow(gauss, exponent);
    probability *= prob;
    this->weight *= probability;
    return probability;
}

double Particle::measurementProbabilityBeaconPseudo(vector<double> sensed, vector<double> readings) {
    double probability = 1.0;
    int length = readings.size();
    bool notPresent = true;
    int readingsSize = sensed.size();
    for(int i=0;i<readingsSize;i++){
        if(sensed.at(i) != 0){
            notPresent = false;
        }
    }

    if(notPresent){
        return exp(-10);
    }

    double sigma = 0.5;

    double squaredDiff = 0.0;
    for (int i = 0; i < length; i++) {
        squaredDiff += pow(sensed[i]-readings[i],2);
    }

    double exponent = 1.0/length;


    double gauss = gaussian(0.0,sigma,sqrt(squaredDiff))+exp(-70);
    double prob  = pow(gauss, exponent);
    probability *= prob;

    return probability;
}

double
Particle::measurementProbabilityMagnitude(vector<double> location, vector<vector<double>> result) {
    double minimumSqrt = INFINITY;
    double probability = 1;
    for(unsigned long i=0;i<result.size();i++){
        vector<double>waypoint = result.at(i);
        double dist = sqrt(pow(location.at(0)-waypoint.at(0), 2) + pow(location.at(1)-waypoint.at(1), 2) + pow(location.at(2)-waypoint.at(2), 2));
        if(dist < minimumSqrt){
            minimumSqrt = dist;
        }
    }
    double exponent = 1.0/3.0;

    double sigma = 5.0;
    double gauss = gaussian(0.0,sigma,sqrt(minimumSqrt))+exp(-70);
    double prob  = pow(gauss, exponent);
    probability *= prob;

    return probability;
}
double Particle::measurementProbabilityCalibratedMagnitude(double magVal, double sensedVal, double std) {

    double probability = 1;



    double gauss = gaussian(magVal,std,sensedVal)+exp(-70);
    //double prob  = pow(gauss, exponent);
    probability *= gauss;
    this->weight *= probability;
    return probability;
}

double Particle::getWeight() const {
    return weight;
}

void Particle::setWeight(double weight) {
    Particle::weight = weight;
}




