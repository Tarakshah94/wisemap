//
// Created by noob on 5/3/19.
//

#include <vector>
#include "LocProvider.h"
#include <string>
#include <android/log.h>

#include <vector>
#include "math.h"

#include <chrono>
#include <sstream>
#include <iostream>
#include<cmath>
#include <random>

using namespace std;


void LocProvider::initLocalizerAndMapper(int dimensions, int particleSize,
                                         vector<vector<double>> geofence,
                                         vector<MagneticMap> magneticMapData) {


    this->dimensions     = dimensions;
    this->particleSize   = particleSize;
    //insertMagneticMapIntoTree(magneticMapData);
    removeDuplicatesReverse(magneticMapData);
    //removeDuplicates(measurements);
    initParticleFilter(geofence);

}

void LocProvider::removeDuplicatesReverse(vector<MagneticMap> measurements) {
   vector<point_t>values;
    for (unsigned long i=0;i<measurements.size();i++){
        MagneticMap measurement = measurements.at(i);
        double  x = measurement.getMX();
        double  y = measurement.getMY();
        double  z = measurement.getMZ();

        string  key = to_string(x) +"->"+to_string(y)+"->"+to_string(z);
        point_t point;
        point = {x, y, z};
        if(measurementsHash.find(key) == measurementsHash.end()){
            values.push_back(point);
            measurementsHash.insert(pair<string,MagneticMap>(key,measurement));
        }

    }


        buildTreeReverse(values);

}

void LocProvider::removeDuplicates(vector<NeighbourMeasurements> measurements) {
    vector<point_t>values;
    for (unsigned long i=0;i<measurements.size();i++){
        NeighbourMeasurements measurement = measurements.at(i);
        double beacon_x                   = measurement.getBeacon_x();
        double beacon_y                   = measurement.getBeacon_y();
        double beacon_z                   = measurement.getBeacon_z();
        //double mag_world                  = measurement.getMagnitude_world();

        //string  key = to_string(beacon_x) +"->"+to_string(beacon_y)+"->"+to_string(beacon_z)+"->"+to_string(mag_world);;
        string  key = to_string(beacon_x) +"->"+to_string(beacon_y)+"->"+to_string(beacon_z);
        point_t point;
        point = {beacon_z, beacon_y, beacon_x};
        if(beaconHash.find(key) == beaconHash.end()){
            values.push_back(point);
            beaconHash.insert(pair<string,NeighbourMeasurements>(key,measurement));
        }

    }
    buildTree(values);

}



void LocProvider::buildTree(vector<point_t> values) {
    this->beaconNeigboursTree = KDTree(values);
}

void LocProvider::initParticleFilter(vector<vector<double>> geofence) {
    this->slaves.clear();

    vector<double> weights;
    weights.reserve(particleSize);

    std::random_device rd;


    std::mt19937 e2(rd());

    std::uniform_real_distribution<> dist_x(0, 1);
    std::uniform_real_distribution<> dist_y(0, 1);
    std::uniform_real_distribution<> dist_z(0, 1);

//
    for(int i=0;i<particleSize;i++){

        double x = (dist_x(e2)*(geofence.at(0).at(1) - geofence.at(0).at(0)))+geofence.at(0).at(0);
        double y = (dist_y(e2)*(geofence.at(1).at(1) - geofence.at(1).at(0)))+geofence.at(1).at(0);
        double z = (dist_z(e2)*(geofence.at(2).at(1) - geofence.at(2).at(0)))+geofence.at(2).at(0);



        double orientation = 2*M_PI*this->randd();

        Particle* slave = new Particle(x, y, z, orientation,geofence);
        slave->setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        this->slaves.push_back(slave);
        weights.push_back(1);
    }



}

void LocProvider::move(double distance, double turn, vector<double> magneticmeasurements, double position[], vector<double> beaconmeasurements, bool isBeacon, vector<double> wifimeasurements, bool isWifi, int bSize) {
    avoidReasample = false;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();


    vector<double> weights;
    weights.reserve(particleSize);


//   if(previousHeading == -1){
//       unCertainity = 180;
//   }else{
//       unCertainity = abs(turn - previousHeading)+15;
//   }
    unCertainity = 45;
//    if(abs(previousHeading - turn) > 25){
//        unCertainity = 45;
//    }

    previousHeading = turn;
    double heading = turn;
    double std_dev = this->TURN_NOISE * M_PI/180;


    std::default_random_engine generator (seed);
    std::normal_distribution<double> turn_distribution(0, std_dev);
    std::random_device rd;


    std::mt19937 e2(rd());
    double min_val = heading - unCertainity;
    double max_val = heading + unCertainity;
    std::uniform_real_distribution<> dist_heading(min_val, max_val);

    for (int i = 0; i < particleSize; i++) {
        Particle *slave = this->slaves.at(i);
        heading = dist_heading(e2);
        std::normal_distribution<double> turn_distribution(0, 0);
        this->slaves.at(i) = slave->move(distance, heading, turn_distribution, generator);
    }



    for(int i=0;i<particleSize;i++){
        Particle* slave = this->slaves.at(i);
        vector<double> location;
        location.push_back(slave->getX());
        location.push_back(slave->getY());
        location.push_back(slave->getZ());

        double magMeasurements[3];
        getNearestReadings(location, magMeasurements);
        double magnitude = sqrt(pow(magMeasurements[0],2) + pow(magMeasurements[1],2) + pow(magMeasurements[2],2));
        double sensedmagnitude = sqrt(pow(magneticmeasurements.at(0),2) + pow(magneticmeasurements.at(1),2) + pow(magneticmeasurements.at(2),2));
        double weight = 1;
        weight *= slave->measurementProbability(magneticmeasurements, magMeasurements);

        if(isBeacon) {
            weight *= slave->measurementProbabilityBeacon(beaconmeasurements, location);
        }
        weights.push_back(weight);
    }
    if(distance > 0){
        steps += 1;
    }

    double min_weights = returnMin(weights);
    double max_weights = returnMax(weights);
    double check = min_weights / max_weights;

    getUserPosition(weights, slaves, position);
    this->normalizeWeightsForSlaves();
    this->checkNeedForResample();
    if(steps < 2){
        avoidReasample = true;
    }
    position[3]         = check;
    xPos = position[0];
    yPos = position[1];
    zPos = position[2];
    reSample(weights,slaves);
    //calculateBias(turn);


}

void LocProvider::getNearestReadings(vector<double> sensed, double returnValue[]) {

    point_t p(sensed);


    auto res2 = this->neighboursTree.nearest_point(p);

    point_t a = res2;
    double diff = sqrt(pow(a.at(0) - p.at(0), 2) + pow(a.at(1) - p.at(1), 2) + pow(a.at(2) - p.at(2), 2));
    if(diff > 2.0){
        returnValue[0] = 9999999999;
        returnValue[1] = 9999999999;
        returnValue[2] = 9999999999;
        return;
    }else{
        cout<<"here"<<endl;
    }
    string key = to_string(a.at(0)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(2));
    MagneticMap reading = measurementsHash.at(key);

    returnValue[0] = reading.getMag_x();
    returnValue[1] = reading.getMag_y();
    returnValue[2] = reading.getMag_z();

}

vector<double> LocProvider::getNearestReadingsBeacon(vector<double> sensed) {

    point_t p(sensed);


    auto res2 = this->beaconNeigboursTree.nearest_point(p);
    vector<double> prediction;
    //double magnitude=0,beacon_x=0,beacon_y=0,beacon_z=0,magnitude_world=0,xy_mag=0,beaconrssi_x=0,beaconrssi_y=0,beaconrssi_z = 0;
    double x = 0, y =0, z =0;
    int index =0;
    point_t a = res2;
    string key = to_string(a.at(2)) +"->"+to_string(a.at(1)) +"->"+to_string(a.at(0));
    NeighbourMeasurements mst   = beaconHash.at(key);
    double x_                   = mst.getParticle_x();
    double y_                   = mst.getParticle_y();
    double z_                   = mst.getParticle_z();



        x               += x_;
        y               += y_;
        z               += z_;
    prediction.push_back(x);
    prediction.push_back(y);
    prediction.push_back(z);
    return prediction;
}

void LocProvider::getUserPosition(vector<double> weights, vector<Particle *> slaves, double position[]) {
    double xMean = 0;
    double yMean = 0;
    double zMean = 0;
    double count = 0;
    double sumWeight = 0;

//    for (int i = 0; i < particleSize; i++) {
//        sumWeight += weights[i];
//    }
    for (int i = 0; i < particleSize; i++) {
        Particle* slave = slaves.at(i);
        sumWeight += slave->getWeight();
    }
    if(sumWeight == 0){
        sumWeight = 0.00000001;
        //avoidReasample = true;
    }

    for (int i = 0; i < particleSize; i++) {
        Particle* slave = slaves.at(i);
        double weight = slave->getWeight();
        double weightFactor = weight / sumWeight;
        count += weightFactor;

        xMean += slave->getX() * weightFactor;
        yMean += slave->getY() * weightFactor;
        zMean += slave->getZ() * weightFactor;
    }
    if(count == 0){
        count = 0.0000000001;
    }
    xMean /= count;
    yMean /= count;
    zMean /= count;
//    count = 0;
//
//    for (int i = 0; i < particleSize; i++) {
//        Particle* slave = slaves.at(i);
//        double euclidean = sqrt(pow(slave->getX() - xMean, 2) +
//                                     pow(slave->getY() - yMean, 2) + pow(slave->getZ() - zMean, 2));
//        if (euclidean < 3) {
//            count++;
//        }
//    }
//
//    double status = 1;
//    if (count < (0.90 * particleSize)) {
//        status = 0;
//    }
    position[0] = xMean;
    position[1] = yMean;
    position[2] = zMean;
    position[3] = count;
}

void LocProvider::reSample(vector<double> weights, vector<Particle *> slaves) {
    if(avoidReasample){

        for(int j=0;j<particleSize;j++){
            Particle* particle = this->slaves.at(j);
            this->slaves.at(j) = new Particle(*particle);
        }
        return;

    }
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "resample called");
    vector<Particle*> newparticles;


    int rand_index                    = (int)(((double) rand() / (RAND_MAX))*(particleSize));
    double beta                       = 0.0;
    double max_weight = *max_element(weights.begin(),weights.end());

    for(int i=0; i<particleSize; i++){
        beta    += ((randd())*(2.0)*(max_weight));
        while (beta >= weights.at(rand_index)){
            beta -= weights.at(rand_index);
            rand_index = (rand_index+1) % particleSize;
        }

        Particle* particle = this->slaves.at(rand_index);
        particle->setWeight(1);

        newparticles.push_back(new Particle(*particle));
    }

    this->slaves = newparticles;
    __android_log_print(ANDROID_LOG_VERBOSE, "Debug", "resample ended");
}

double LocProvider::returnMax(vector<double> weights) {
    return *max_element(weights.begin(),weights.end());
}

double LocProvider::returnMin(vector<double> weights) {
    return *min_element(weights.begin(),weights.end());
}



void LocProvider::reInitParticleFilter(vector<vector<double>> geofence) {
    this->slaves.clear();

    vector<double> weights;
    weights.reserve(particleSize);

    for(int i=0;i<particleSize;i++){

        double x = (this->randd()*(geofence.at(0).at(1) - geofence.at(0).at(0))/2);
        double y = (this->randd()*(geofence.at(1).at(1) - geofence.at(1).at(0))/2);
        double z = (this->randd()*(geofence.at(2).at(1) - geofence.at(2).at(0))/2);
        double orientation = 2*M_PI*this->randd();

        Particle* slave = new Particle(x, y, z, orientation,geofence);
        slave->setNoise(this->FORWARD_NOISE, this->TURN_NOISE);
        this->slaves.push_back(slave);
        weights.push_back(1);
    }
    double values[4];
    this->getUserPosition(weights, slaves, values);
    xPos = values[0];
    yPos = values[1];
    zPos = values[2];

}

void LocProvider::calculateBias(double turnInDegrees) {
    double totalLeftOver = 0.0;
    for(int i=0;i<particleSize;i++){
        double particleHeadingInradians = this->slaves.at(i)->getOrientation();
        double particleHeadingInDegrees = particleHeadingInradians*180/M_PI;
        double leftOver                 = particleHeadingInDegrees - turnInDegrees;
        totalLeftOver                   += leftOver;
    }
//    bias    = totalLeftOver/particleSize;
}

void LocProvider::getParticleLocations(double locs[100][3]) {
    for(int i=0;i<particleSize;i++){
        Particle* slave = this->slaves.at(i);
        locs[i][0] = slave->getX();
        locs[i][1] = slave->getY();
        locs[i][2] = slave->getZ();
    }

}

void LocProvider::buildTreeReverse(vector<point_t> vector) {
    this->neighboursTree = KDTree(vector);
}

//void LocProvider::insertMagneticMapIntoTree(vector<MagneticMap> magMapVector) {
//    this->augmentedTree = new IntervalTree();
//    for(unsigned long i=0;i<magMapVector.size();i++){
//        MagneticMap map = magMapVector.at(i);
//        Node* node = new Node(map.getMinimum(), map.getMaximum(), map.getMX(), map.getMY(), map.getMZ());
//        this->augmentedTree->root = this->augmentedTree->insertNode(this->augmentedTree->root, node);
//    }
//    cout<<"here";
//}



void LocProvider::normalizeWeightsForSlaves() {
    double sum = 0;
    for (int i = 0; i < particleSize; i++) {
        Particle* slave = this->slaves.at(i);
        double weight = slave->getWeight();
        sum += weight;
    }
    if(sum == 0){
        sum = 0.00000001;
    }
    for (int i = 0; i < particleSize; i++) {
        Particle* slave = this->slaves.at(i);
        double normalizedWeight = slave->getWeight()/sum;
        slave->setWeight(normalizedWeight);
        this->slaves.at(i) = slave;
    }
}

void LocProvider::checkNeedForResample() {
    double sumWeights = 0;
    for(int i=0;i<particleSize;i++){
        Particle* slave = this->slaves.at(i);
        double val = slave->getWeight();
        val = pow(val, 2);
        sumWeights += val;
    }

    if(sumWeights == 0){
        sumWeights = 0.0000000000000001;
    }
    double invWeight = 1/sumWeights;
    this->avoidReasample = invWeight >= particleSize / 2;
}













