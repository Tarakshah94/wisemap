//
// Created by noob on 5/3/19.
//

#ifndef WISEMAP_PARTICLE_H
#define WISEMAP_PARTICLE_H
#include <vector>
#include "math.h"
#include <chrono>
#include <string>
#include <sstream>
#include <iostream>
#include<cmath>
#include <random>
using namespace std;
class Particle {
private:
    double mX;
    double mY;
    double mZ;
    double orientation;
    double forward_noise    = 0.0;
    double turn_noise       = 0.0;
public:
    double getWeight() const;

    void setWeight(double weight);

private:
    vector<vector<double> >geofence;
    bool isRobot;
    double weight = 1;
public:
    Particle(double x, double y, double z, double heading, vector<vector<double> > geofence);

    Particle(const Particle& robot);

    void setValues(double x, double y, double z, double orientation);

    void setNoise(double forward_noise, double bearing_noise);

    double getX();
    double getY();
    double getZ();
    double adjustCoordinates(double value, int index);
    Particle* move(double distance, double turn, std::normal_distribution<double> turn_distribution, std::default_random_engine generator);
    double randd() {
        return (double)rand() / (RAND_MAX + 1.0);
    }
    double radToDeg(double radians);
    double degToRad(double degrees);

    void nextPosition(double meters, double heading, double returnValue[]);
    void convertSphericalToCartesian(double meters, double heading, double returnarray[]);
    void convertCartesianToSpherical(double * cartesian, double latlng[]);
    void distance(double * from, double distance, double heading, double latlng[]);
    double gaussian(double mu, double sigma, double value);
    double measurementProbability(vector<double> sensed, double readings[]);
    double measurementProbabilityBeacon(vector<double> sensed, vector<double> readings);

    double getOrientation();

    double measurementProbabilityBeaconPseudo(vector<double> sensed, vector<double> readings);

    double measurementProbabilityMagnitude(vector<double> location, vector<vector<double>> result);
    double measurementProbabilityCalibratedMagnitude(double magVal, double sensedVal, double std);
};


#endif //WISEMAP_PARTICLE_H
