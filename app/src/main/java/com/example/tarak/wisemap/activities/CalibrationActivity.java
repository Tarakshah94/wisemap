package com.example.tarak.wisemap.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tarak.wisemap.R;
import com.example.tarak.wisemap.managers.SharedPreferenceManager;
import com.example.tarak.wisemap.utils.HardwareUtility;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.ZeroException;
import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealMatrixChangingVisitor;
import org.apache.commons.math3.linear.RealMatrixPreservingVisitor;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class CalibrationActivity extends AppCompatActivity implements SensorEventListener {

    private SharedPreferenceManager mSharedManager;
    private SensorManager mSensorManager;
    private boolean startCalibrating = false;
    private Button calibrate;
    private Button testPosition;
    private Timer timer;
    private ArrayList<float[]> gyroUncalibrated = new ArrayList<>();
    private ArrayList<Float> magUncalibratedX  = new ArrayList<>();
    private ArrayList<Float> magUncalibratedY  = new ArrayList<>();
    private ArrayList<Float> magUncalibratedZ  = new ArrayList<>();
    private ProgressBar progress;
    private boolean calibrateClicked = false;
    private static double[] xa0 = new double[4];
    private static double[] ya0 = new double[4];
    private static double[] xa1 = new double[4];
    private static double[] ya1 = new double[4];
    private static double[] xa2 = new double[4];
    private static double[] ya2 = new double[4];
    private static float[] tpA = new float[3];
    private static float[] tpM = new float[3];
    private static double[] xm0 = new double[4];
    private static double[] ym0 = new double[4];
    private static double[] xm1 = new double[4];
    private static double[] ym1 = new double[4];
    private static double[] xm2 = new double[4];
    private static double[] ym2 = new double[4];

    public static float[] linear = new float[4];
    public static float[] magn = new float[4];
    public static float[] gravity = new float[3];
    private static float ugainA;
    private static float ugainM;
    private static float[] iMatrix = new float[9];
    private static float[] RMatrix = new float[16];
    private static float[] RMatrixRemapped = new float[16];
    private static float[] RMatrixTranspose = new float[16];
//    private TextView progressText;
//    int progressValue = 0;
    int axis = 0;
    int number = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedManager      =  new SharedPreferenceManager(getApplicationContext());
        //Listen for gyroscope if available and filter the drift errors out

            mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        magn[0] = magn[1] = magn[2] = gravity[0] = gravity[1] = 0;
        gravity[2] = 9.81f;
        ugainM = ugainA = 154994.3249f;
        tpA[0] = tpM[0] = 0.9273699683f;
        tpA[1] = tpM[1] = -2.8520278186f;
        tpA[2] = tpM[2] = 2.9246062355f;

        double nw[] = convertSphericalToCartesian(19.168053, 72.852904);
        double ne[] = convertSphericalToCartesian(19.168075, 72.853295);
        double sw[] = convertSphericalToCartesian(19.167213, 72.852893);
        double se[] = convertSphericalToCartesian(19.167259, 72.853227);
//        double et[] = convertSphericalToCartesian(19.098936677915077,72.87374082254247);
//        double an[] = convertSphericalToCartesian(19.098944281615246,72.87427458213644);

        System.out.print("here");
        //1772782.4299007724,5753356.622226032,2084529.8569032913
        //1772876.0073400505,5753400.237312276,2084594.3059899271

    }

    @Override
    protected void onResume() {
        super.onResume();
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }else{
                //already permission given
                setupUi();
            }
        }

        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_GAME);
    }

    public void setupUi(){
        calibrate    = findViewById(R.id.calibrate);
        testPosition = findViewById(R.id.positiontester);
        progress     = findViewById(R.id.progressBar);
        progress.setVisibility(View.GONE);
        //progressText = findViewById(R.id.progressText);
        //mSharedManager.putBoolean("fingering", true);
        calibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long timeDiff = System.currentTimeMillis() - mSharedManager.getLong("lasttime");
                if(mSharedManager.getBoolean("fingering") && timeDiff <= 10*60*1000){

                    progress.setVisibility(View.GONE);
                    proceedToNextActivity();
                    finish();


                }else {
//                    startCalibrating = true;
//                    startTimer();
//                    progressText.setVisibility(View.VISIBLE);
//                    progressText.setText("Progress : "+String.valueOf(progressValue)+"%");
                    calibrateClicked = true;
                    Toast.makeText(CalibrationActivity.this,"Place the device on a flat surface",Toast.LENGTH_LONG).show();
                    calibrate.setVisibility(View.GONE);
                    testPosition.setVisibility(View.GONE);
                    progress.setVisibility(View.VISIBLE);
                }
               // proceedToNextActivity();

            }
        });

        testPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                proceedToPositionTestActivity();
                finish();
            }
        });
    }

    private void requestForSpecificPermission() {
        String[] permsList = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_STATE};
        ActivityCompat.requestPermissions(this, permsList, 101);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(timer != null){
            timer.cancel();
        }

        mSensorManager.unregisterListener(this);
    }

    public void startTimer(){
        new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {
               // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                calculateMean();
            }
        }.start();
    }


    private void calculateMean(){
        calibrateClicked = false;
        startCalibrating = false;

        float minX = Collections.min(magUncalibratedX);
        float maxX = Collections.max(magUncalibratedX);

        float minY = Collections.min(magUncalibratedY);
        float maxY = Collections.max(magUncalibratedY);

        float minZ = Collections.min(magUncalibratedZ);
        float maxZ = Collections.max(magUncalibratedZ);

        float hardIronoffsetX = (minX+maxX)/2;
        float hardIronoffsetY = (minY+maxY)/2;
        float hardIronoffsetZ = (minZ+maxZ)/2;

        ArrayList<Float> calibratedX = new ArrayList<>();
        ArrayList<Float> calibratedY = new ArrayList<>();
        ArrayList<Float> calibratedZ = new ArrayList<>();

        for(int k=0;k<magUncalibratedX.size();k++){
            calibratedX.add(magUncalibratedX.get(k) - hardIronoffsetX);
        }

        for(int k=0;k<magUncalibratedY.size();k++){
            calibratedY.add(magUncalibratedY.get(k) - hardIronoffsetY);
        }

        for(int k=0;k<magUncalibratedZ.size();k++){
            calibratedZ.add(magUncalibratedZ.get(k) - hardIronoffsetZ);
        }

        float calibMaxX = Collections.max(calibratedX);
        float calibMinX = Collections.min(calibratedX);

        float calibMaxY = Collections.max(calibratedY);
        float calibMinY = Collections.min(calibratedY);

        float calibMaxZ = Collections.max(calibratedZ);
        float calibMinZ = Collections.min(calibratedZ);


        float avgdeltaX = (calibMaxX - calibMinX)/2;
        float avgdeltaY = (calibMaxY - calibMinY)/2;
        float avgdeltaZ = (calibMaxZ - calibMinZ)/2;

        float avgDelta = (avgdeltaX + avgdeltaY + avgdeltaZ) / 3;

        float softIronScaleX = avgDelta/avgdeltaX;
        float softIronScaleY = avgDelta/avgdeltaY;
        float softIronScaleZ = avgDelta/avgdeltaZ;


        mSharedManager.putFloat("hardironx",hardIronoffsetX);
        mSharedManager.putFloat("hardirony",hardIronoffsetY);
        mSharedManager.putFloat("hardironz",hardIronoffsetZ);

        mSharedManager.putFloat("softironx",softIronScaleX);
        mSharedManager.putFloat("softirony",softIronScaleY);
        mSharedManager.putFloat("softironz",softIronScaleZ);


        mSharedManager.putBoolean("fingering", true);
        mSharedManager.putLong("lasttime",System.currentTimeMillis());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(View.GONE);
                proceedToNextActivity();
                finish();
            }
        });


    }

    private void proceedToNextActivity(){
        //Intent fingerprintactivity = new Intent(this,PositionTester.class);
        Intent fingerprintactivity = new Intent(this,WaypointFileSelector.class);
        startActivity(fingerprintactivity);
    }

    private void proceedToPositionTestActivity(){
        Intent fingerprintactivity = new Intent(this,PositionTester.class);
        //Intent fingerprintactivity = new Intent(this,FingerPrintActivity.class);
        startActivity(fingerprintactivity);
    }

    private boolean checkIfAlreadyhavePermission() {
        int result_fine_location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result_coarse_location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int result_write_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result_read_storage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        int result_wifi_access = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);
        int result_wifi_change_access = ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE);
        if (result_fine_location == PackageManager.PERMISSION_GRANTED && result_coarse_location == PackageManager.PERMISSION_GRANTED && result_write_storage == PackageManager.PERMISSION_GRANTED && result_read_storage == PackageManager.PERMISSION_GRANTED && result_wifi_access == PackageManager.PERMISSION_GRANTED && result_wifi_change_access == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                    setupUi();
                } else {
                    //not granted
                    Toast.makeText(getApplicationContext(),"Permissions denied",Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_GYROSCOPE:
                if(!startCalibrating){
                    return;
                }

                float[] values = sensorEvent.values;
                gyroUncalibrated.add(values);

                break;
            case Sensor.TYPE_MAGNETIC_FIELD:



//
                imbaMagnetic(sensorEvent.values.clone());

                break;

            case Sensor.TYPE_ACCELEROMETER:
                imbaGravity(sensorEvent.values.clone());
                imbaLinear(sensorEvent.values.clone());

                break;
            default:
                break;
        }

    }

    private void imbaMagnetic(float magnetic[]) {





        if(startCalibrating) {
            magn[0] = magnetic[0];
            magn[1] = magnetic[1];
            magn[2] = magnetic[2];
//            SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magn);
//            Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
//
//            //SensorManager.remapCoordinateSystem(RMatrix, x_world, y_world, RMatrixRemapped);
//            float magn_remapped[] = new float[4];
//            Matrix.multiplyMV(magn_remapped, 0, RMatrixTranspose, 0, magn, 0);

            magUncalibratedX.add(magn[0]);
            magUncalibratedY.add(magn[1]);
            magUncalibratedZ.add(magn[2]);

        }

    }

    public  void imbaGravity(float[] accel) {
        // LowPass 0.5Hz for alpha0
//        double magnitude = Math.sqrt(accel[0]*accel[0] + accel[1]*accel[1]+accel[2]*accel[2]);
//        xa0[0] = xa0[1];
//        xa0[1] = xa0[2];
//        xa0[2] = xa0[3];
//        xa0[3] = accel[0] / ugainA;
//        ya0[0] = ya0[1];
//        ya0[1] = ya0[2];
//        ya0[2] = ya0[3];
//        ya0[3] = (xa0[0] + xa0[3]) + 3 * (xa0[1] + xa0[2]) + (tpA[0] * ya0[0]) + (tpA[1] * ya0[1]) + (tpA[2] * ya0[2]);
//        gravity[0] = (float) ya0[3];
//
//        // LowPass 0.5Hz for alpha1
//        xa1[0] = xa1[1];
//        xa1[1] = xa1[2];
//        xa1[2] = xa1[3];
//        xa1[3] = accel[1] / ugainA;
//        ya1[0] = ya1[1];
//        ya1[1] = ya1[2];
//        ya1[2] = ya1[3];
//        ya1[3] = (xa1[0] + xa1[3]) + 3 * (xa1[1] + xa1[2]) + (tpA[0] * ya1[0]) + (tpA[1] * ya1[1]) + (tpA[2] * ya1[2]);
//        gravity[1] = (float) ya1[3];
//
//        // LowPass 0.5Hz for alpha2
//        xa2[0] = xa2[1];
//        xa2[1] = xa2[2];
//        xa2[2] = xa2[3];
//        xa2[3] = accel[2] / ugainA;
//        ya2[0] = ya2[1];
//        ya2[1] = ya2[2];
//        ya2[2] = ya2[3];
//        ya2[3] = (xa2[0] + xa2[3]) + 3 * (xa2[1] + xa2[2]) + (tpA[0] * ya2[0]) + (tpA[1] * ya2[1]) + (tpA[2] * ya2[2]);
//        gravity[2] = (float) ya2[3];
        gravity = accel;


    }

    public  void imbaLinear(float[] accel) {
        linear[0] = accel[0] - gravity[0];
        linear[1] = accel[1] - gravity[1];
        linear[2] = accel[2] - gravity[2];

        double diff = Math.pow(linear[0],2) + Math.pow(linear[1],2) + Math.pow(linear[2],2);
        if(diff < 0.05){
            if(!startCalibrating && calibrateClicked){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CalibrationActivity.this,"start rotating",Toast.LENGTH_LONG).show();
                    }
                });
                startCalibrating = true;
                startTimer();
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }



    private double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = Math.toRadians(latitude);
        double lon = Math.toRadians(longitude);
        double x = earthRadius * cos(lat) * cos(lon);
        double y = earthRadius * cos(lat) * sin(lon);
        double z = earthRadius * sin(lat);
        return new double[]{x, y, z};
    }
}
