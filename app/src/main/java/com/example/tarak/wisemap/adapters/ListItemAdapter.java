package com.example.tarak.wisemap.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tarak.wisemap.R;
import com.example.tarak.wisemap.activities.WaypointFileSelector;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.MyViewHolder> {
    private String[] mDataSet;
    WaypointFileSelector mActivity;
    public  ListItemAdapter(String[] myDataSet, Activity activity){
        mDataSet  = myDataSet;
        mActivity = (WaypointFileSelector) activity;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        TextView v = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem, viewGroup, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        String name[] = mDataSet[i].split("/");
        myViewHolder.textView.setText(name[name.length-1]);
        myViewHolder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mActivity.listItemClicked(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        public MyViewHolder(TextView v){
            super(v);
            textView = v;

        }
    }
}
