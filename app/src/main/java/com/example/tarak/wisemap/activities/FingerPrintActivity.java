package com.example.tarak.wisemap.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tarak.wisemap.R;
import com.example.tarak.wisemap.hardwaresensors.StepAndAzimutDetector;
import com.example.tarak.wisemap.helpers.Kalman;
import com.example.tarak.wisemap.helpers.Matrixf4x4;
import com.example.tarak.wisemap.managers.LocationUpdater;
import com.example.tarak.wisemap.managers.PositioningManager;
import com.example.tarak.wisemap.managers.SharedPreferenceManager;
import com.example.tarak.wisemap.models.BeaconsData;
import com.example.tarak.wisemap.models.BeaconsDetail;
import com.example.tarak.wisemap.models.FilteredBeacon;
import com.example.tarak.wisemap.models.LandMark;
import com.example.tarak.wisemap.models.WifiDetails;
import com.example.tarak.wisemap.utils.FileUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kontakt.sdk.android.ble.configuration.ScanMode;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class FingerPrintActivity extends AppCompatActivity implements OnMapReadyCallback,MapboxMap.OnMapLongClickListener,StepAndAzimutDetector.onStepUpdateListener,LocationUpdater {
    //private SensorManager mSensorManager;
    public static float x_error = 0;
    public static float y_error = 0;
    public static float z_error = 0;
    private MapView mapView;
    private MapboxMap mMap;
    private Marker mUserLocationMarker;
    private Marker mParticleLocationMarker;
    private Marker mRobotLocationMarker;
    private Icon userIcon;
    private Icon particleIcon;
    private Icon robotIcon;
    private boolean initializationDone = false;
    private StepAndAzimutDetector sensorController;
    private LatLng startLatLng;
    PositioningManager locManager;
    private double heading;
    private double headingDegrees;
    private double turn;
    private WifiManager wifiManager;
    ArrayList<WifiDetails> wifiResults = new ArrayList<>();
    private ProximityManager proximityManager;
    HashMap<String, BeaconsDetail> beaconsHashMap = new HashMap<>();
    private float[] currentMagValues = {0f,0f,0f};
    private DrawerLayout mDrawerLayout;
    private SeekBar seekBar;
    private EditText name;
    private TextView value;
    private ImageButton startMapping;
    private boolean started = false;
    private TextView statusTextView;
    private LatLng firstPoint;
    private double[] firstloc;
    private boolean isAccurate;
    private ArrayList<FilteredBeacon> modifiedList = new ArrayList<>();
    private BluetoothAdapter BTAdapter;
    private float deviceMagObserved[] = {0f,0f,0f};
    private HashMap<String, Kalman> beaconsFilterHashMap = new HashMap<>();
    private double bias = 0;
    Kalman[] kalMagnetic = new Kalman[3];
    Button logger;
    private int biasStatus = 0;
    private ArrayList<Double> headingCollection = new ArrayList<>();
    private LatLng initialLatLng;
    private LatLng secondLatLng;
    private double strideLength = 0.6;
    private JSONArray wayPointsList = new JSONArray();
    private Icon wayPointIcon;
    private Icon nearestWaypointIcon;
    private ArrayList<Marker> wayPointsMarker = new ArrayList<>();
    private double[] waypointLoc = new double[3];
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice>devices = new ArrayList<>();
    private LatLng destinationPoint;
    private String fileName;
    private JSONObject wayPointMagData;
    private HashMap<String, Kalman> kalmanMagneticMap = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_print);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

       SharedPreferenceManager manager = new SharedPreferenceManager(getApplicationContext());



        //Sensors
        wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        proximityManager = ProximityManagerFactory.create(this);
        proximityManager.configuration()
                .scanPeriod(ScanPeriod.RANGING)
                .scanMode(ScanMode.BALANCED)
                .deviceUpdateCallbackInterval(100);

        proximityManager.setIBeaconListener(createIBeaconListener());


        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        IconFactory iconFactory = IconFactory.getInstance(this);
        userIcon = iconFactory.fromResource(R.mipmap.user_marker);
        particleIcon = iconFactory.fromResource(R.mipmap.dot);
        robotIcon = iconFactory.fromResource(R.mipmap.beacon1);
        wayPointIcon = iconFactory.fromResource(R.mipmap.beacon);
        nearestWaypointIcon = iconFactory.fromResource(R.mipmap.beacon1);
        locManager = new PositioningManager(this);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });

        seekBar      = findViewById(R.id.seekBar);
        seekBar.setMax(8);
        seekBar.setProgress(2);
        value        = findViewById(R.id.textView2);
        name         = findViewById(R.id.textView);
        startMapping = findViewById(R.id.startMap);
        statusTextView       = findViewById(R.id.textView3);
        logger      = findViewById(R.id.logger);


        logger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(StepAndAzimutDetector.isLogging){
                        StepAndAzimutDetector.isLogging = false;
                        sensorController.saveToFile();

                        logger.setText("Start Logging");
                }else{
                    StepAndAzimutDetector.isLogging = true;
                    logger.setText("Stop Logging");

                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                value.setText("Max number of beacons "+String.valueOf(i));
                locManager.passMaxBeacons(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        startMapping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!initializationDone){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(FingerPrintActivity.this,"User Position not set",Toast.LENGTH_LONG).show();

                        }
                    });
                    return;
                }
                started = !started;
                if(started){
                    //startmapping

                    startMapping.setImageResource(R.mipmap.stop_mapping);
                }else{
                    //save mapping
                    startMapping.setImageResource(R.mipmap.start_mapping);
                    saveBackTheFile();
                }
                locManager.passStartStopMessage(started);
            }
        });
        sensorController = new StepAndAzimutDetector(this);
        sensorController.startSensors(false);

        if(getIntent() !=null) {
            if (getIntent().getExtras() != null) {
                fileName = getIntent().getExtras().getString("filename");
                String name = FileUtils.loadJSONFromFile(FingerPrintActivity.this, fileName);

                if(name == null){
                    wayPointsList = new JSONArray();
                }else {
                    try {
                        wayPointsList = new JSONArray(name);
                    } catch (JSONException e) {
                        wayPointsList = new JSONArray();
                        e.printStackTrace();
                    }
                }
            }
        }
        readFromJSONFile();


    }

    private void readFromJSONFile() {
        String directoryPath = WaypointFileSelector.directoryPath+"/";
        String file = fileName.replace(directoryPath,"");
        String fileWithoutExtension = file.replace(".json", "");
        String waypointmagneticname = WaypointFileSelector.magDirectoryPath+"/"+fileWithoutExtension+"_fingdata"+".json";

        String name = FileUtils.loadJSONFromFile(FingerPrintActivity.this, waypointmagneticname);

        if(name == null){
            wayPointMagData = new JSONObject();
        }else {
            try {
                wayPointMagData = new JSONObject(name);
            } catch (JSONException e) {
                wayPointMagData = new JSONObject();
                e.printStackTrace();
            }
        }
    }

    private void saveBackTheFile(){
        String directoryPath = WaypointFileSelector.directoryPath+"/";
        String file = fileName.replace(directoryPath,"");
        String fileWithoutExtension = file.replace(".json", "");
        String waypointmagneticname = WaypointFileSelector.magDirectoryPath+"/"+fileWithoutExtension+"_fingdata"+".json";
        FileUtils.saveFileWithoutExtension(FingerPrintActivity.this, wayPointMagData.toString(), waypointmagneticname);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();


    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        if(sensorController != null){
            sensorController.pauseSensors();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        if(sensorController != null){
            sensorController.pauseSensors();
        }
        proximityManager.stopScanning();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if(sensorController != null){
            sensorController.shutdown(this);
        }
        if(locManager != null){
            locManager.invalidate();
        }
        proximityManager.stopScanning();
        proximityManager.disconnect();
        proximityManager = null;
        //unregisterReceiver(mReceiver);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMap = mapboxMap;
        mMap.addOnMapLongClickListener(this);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                readBeaconFile();
                startScanning();
            }
        });
        addAllWaypointsAsMarkers();
    }

    private void addAllWaypointsAsMarkers() {
        wayPointsMarker.clear();
        for(int i=0;i<wayPointsList.length();i++){
            try {
                JSONObject insideObj = wayPointsList.getJSONObject(i);
                double latitude      = insideObj.getDouble("latitude");
                double longitude     = insideObj.getDouble("longitude");

                Marker landmarkMarker = mMap.addMarker(new MarkerViewOptions()
                        .position(new LatLng(latitude, longitude))
                        .icon(wayPointIcon));

                wayPointsMarker.add(landmarkMarker);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMapLongClick(@NonNull LatLng point) {
        if(biasStatus > 0){
            point = getNearestPointWayPoint(point);
        }
        if(biasStatus == 0) {
            //deprecated
//            if (mUserLocationMarker == null) {
//                mUserLocationMarker = mMap.addMarker(new MarkerViewOptions()
//                        .position(point)
//                        .icon(userIcon));
//            } else {
//                mUserLocationMarker.setPosition(point);
//            }


            if (mRobotLocationMarker == null) {
                mRobotLocationMarker = mMap.addMarker(new MarkerViewOptions()
                        .anchor(0.5f,0.5f)
                        .position(point)
                        .icon(userIcon));
            }
            this.firstPoint = point;
            if (!initializationDone) {
                startSense(point);
            }
            headingCollection.clear();
            biasStatus = 1;
            initialLatLng = point;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(FingerPrintActivity.this, "Walk to a nearest point and mark it on map by long click",Toast.LENGTH_LONG).show();
                }
            });

        }else if(biasStatus == 1 && secondLatLng == null){
            secondLatLng = point;
            calculateBiasForHeading(point);
            mRobotLocationMarker.setPosition(point);
            highlightNearestMarker(point);
            biasStatus = 2;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(FingerPrintActivity.this, "Choose your destination for route",Toast.LENGTH_LONG).show();
                }
            });


        }else if(biasStatus >= 2){
            double newLocation[] = convertSphericalToCartesian(point.getLatitude(), point.getLongitude());
            locManager.passRobotPositionSetMessage(newLocation[0], newLocation[1], newLocation[2]);
        }

//        else if(biasStatus == 2){
//            destinationPoint = point;
//            drawAPolyLine();
//            biasStatus = 3;
//        }


    }

    private LatLng getNearestPointWayPoint(LatLng point) {
        double min = 999999999999999999.0;
        int index = -1;
        double point1[] = convertSphericalToCartesian(point.getLatitude(), point.getLongitude());
        for(int i=0;i<wayPointsMarker.size();i++){
            Marker marker = wayPointsMarker.get(i);
            marker.setIcon(wayPointIcon);
            double point2[] = convertSphericalToCartesian(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
            double distance = Math.sqrt(Math.pow(point1[0] - point2[0], 2)+ Math.pow(point1[1] - point2[1], 2)+Math.pow(point1[2] - point2[2], 2));
            if(distance < min){
                min = distance;
                index = i;
            }
        }
        if(min <= 1){
            Marker marker = wayPointsMarker.get(index);
            LatLng newPoint = new LatLng(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
            return newPoint;
        }else{
            return point;
        }
    }

    public void drawAPolyLine(){
        LatLng points[] = new LatLng[2];
        points[0] = secondLatLng;
        points[1] = destinationPoint;
        mMap.addPolyline(new PolylineOptions().add(points).color(Color.parseColor("#38afea")).width(5));
    }

    private void readBeaconFile() {
        Gson gson = new Gson();
        Type type = new TypeToken<List<BeaconsDetail>>() {
        }.getType();
        //iconFactory = IconFactory.getInstance(this);
        List<BeaconsDetail> details = gson.fromJson(com.example.tarak.wisemap.utils.FileUtils.loadJSONFromAsset(this, "beacons.json"), type);
        for (BeaconsDetail detail : details) {
            if (detail.getEnabled()) {
              /*  Icon icon = iconFactory.fromResource(R.drawable.beacons_marker);
                beaconMarker = mapboxMap.addMarker(new MarkerViewOptions()
                        .position(new LatLng(detail.getLat(), detail.getLng())).icon(icon));*/
                beaconsHashMap.put(detail.getUniqueId(), detail);
            }

        }
    }

    private void startSense(@NonNull LatLng point) {
        try {
            SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD).getName();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //uid = "" + (1 + (int) (Math.random() * ((10000000 - 1) + 1)));
        String stepLengthString = "165";

        if (stepLengthString != null) {
            try {
                stepLengthString = stepLengthString.replace(",", ".");
                Float savedBodyHeight = (Float.parseFloat(stepLengthString));
                if (savedBodyHeight < 241 && savedBodyHeight > 119) {
                    StepAndAzimutDetector.stepLength = savedBodyHeight / 222;
                } else if (savedBodyHeight < 95 && savedBodyHeight > 45) {
                    StepAndAzimutDetector.stepLength = (float) (savedBodyHeight * 2.54 / 222);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }

        setInitialPosition(point.getLatitude(), point.getLongitude());
    }

    private void setInitialPosition(double startLat,
                                    double startLon) {
        double altitude = 0;
        double middleLat = startLat * 0.01745329252;
        double distanceLongitude = 111.3 * Math.cos(middleLat);

        StepAndAzimutDetector.initialize(startLat, startLon, distanceLongitude, altitude);
        StepAndAzimutDetector.setLocation(startLat, startLon);
        startLatLng = new LatLng(StepAndAzimutDetector.startLat, StepAndAzimutDetector.startLon);
        sensorController.startSensors(true);
       //initializationDone = true;
    }

    public void calculateBiasForHeading(LatLng current){
        double mean = 0.0;
        for(int i=0;i<headingCollection.size();i++){
            mean += headingCollection.get(i);
        }
        if(headingCollection.size() > 0) {
            mean /= headingCollection.size();

            double groundTruth = angleFromCoordinate(initialLatLng.getLatitude(), initialLatLng.getLongitude(), current.getLatitude(), current.getLongitude());
            bias = groundTruth - mean;

            String xx = String.valueOf(mean) + "<---->" + String.valueOf(groundTruth);

            StepAndAzimutDetector.startLat = secondLatLng.getLatitude();
            StepAndAzimutDetector.startLon = secondLatLng.getLongitude();
            calculateStrideLength();
        }

    }

    private void calculateStrideLength(){
        double point1[] = convertSphericalToCartesian(initialLatLng.getLatitude(), initialLatLng.getLongitude());
        double point2[] = convertSphericalToCartesian(secondLatLng.getLatitude(), secondLatLng.getLongitude());

        double distance = Math.sqrt(Math.pow(point1[0] - point2[0], 2)+ Math.pow(point1[1] - point2[1], 2)+Math.pow(point1[2] - point2[2], 2));
        double numberOfSteps = headingCollection.size();
        if(numberOfSteps > 0) {
            strideLength = distance / numberOfSteps;
        }
    }

    @Override
    public void onStepUpdate(int event, double headingDegrees, double turn, float rotations[], float[] linearacceleration, String orientation, double stepLength){

        String xx = String.valueOf(headingDegrees)+"->"+String.valueOf(headingDegrees+bias)+"->"+String.valueOf(strideLength)+"XXXX"+String.valueOf(stepLength);

        this.headingDegrees = headingDegrees;
        headingDegrees += bias;
        this.heading = Math.toRadians(headingDegrees);
        if(biasStatus == 1){
            if(event == 1) {
                headingCollection.add(headingDegrees);
            }
            return;
        }

        if(!initializationDone){
            double[] pos = this.convertSphericalToCartesian(secondLatLng.getLatitude(),secondLatLng.getLongitude());
            firstloc = pos;
            locManager.passInitializeMessage(pos[0], pos[1], pos[2],this.heading);
            initializationDone = true;
            return;
        }
        collectSensorReadings();
        if(event == 0){
            //orientation change

            if(initializationDone){
                locManager.passSimulateMessage(0.0, this.heading);
                if(mRobotLocationMarker != null) {
                    rotateMapbox(headingDegrees);
                }
            }

        }else{
            //step update
            //proceedWithLocationIncrease(dx, dy, dz);
            if(initializationDone){
                for(int i= 0;i<3;i++) {
                    locManager.passSimulateMessage(stepLength/3, this.heading);
                }
                //updateRotatedUserPosition(turn);
//                if(mUserLocationMarker != null){
//                    updateMarkerPosition();
//                }

            }


        }

        //collectSensorReadings(new ArrayList<FilteredBeacon>());


    }



    @Override
    public void onMagneticValueUpdate(float[] magValues, boolean isAccurate, float[] deviceMagObserved) {
        this.currentMagValues = magValues;
        this.deviceMagObserved = deviceMagObserved;
        double mag = Math.sqrt(Math.pow(currentMagValues[0], 2)+Math.pow(currentMagValues[1], 2)+Math.pow(currentMagValues[2], 2));
        statusTextView.setText(String.valueOf(mag));
        addToWayPointIfNeeded();



        if(!this.isAccurate && isAccurate){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(FingerPrintActivity.this,"Magnetic accuracy is reliable now",Toast.LENGTH_LONG).show();
                }
            });
        }
        this.isAccurate = isAccurate;

    }

    private void addToWayPointIfNeeded() {
        double magnitude = Math.sqrt(Math.pow(currentMagValues[0], 2)+Math.pow(currentMagValues[1], 2)+Math.pow(currentMagValues[2], 2));
        if(biasStatus > 1){
            String key = waypointLoc[0]+"-->"+waypointLoc[1]+"-->"+waypointLoc[2];
            if(wayPointMagData.has(key)){

                    Kalman kalman = kalmanMagneticMap.get(key);
                    if (kalman == null) {
                        kalman = new Kalman(magnitude, 1e-3d, 0.75d);
                    }
                    double magVals[] = kalman.estimatedFilter(new ArrayRealVector(1, magnitude));
                    double errorCovariance[][] = kalman.getErrorCovariance();
                    JSONObject val = new JSONObject();
                    try {

                        val.put("mean", magVals[0]);
                        val.put("error",kalman.getErrorCovariance()[0][0]);
                        wayPointMagData.put(key, val);
                    }catch (JSONException e){

                    }
                    kalmanMagneticMap.put(key, kalman);




            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(FingerPrintActivity.this, "something is wrong", Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    public void rotateMapbox(double heading){
        CameraPosition currentPlace = new CameraPosition.Builder().target(mRobotLocationMarker.getPosition()).bearing(heading).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace));
    }



    private void updateRotatedUserPosition(double turn) {
        if(mMap != null && StepAndAzimutDetector.startLat > 0){
            LatLng latLng;
            if(mParticleLocationMarker == null){
                latLng = new LatLng(StepAndAzimutDetector.startLat, StepAndAzimutDetector.startLon);
            }else{
                latLng = new LatLng(mParticleLocationMarker.getPosition().getLatitude(), mParticleLocationMarker.getPosition().getLongitude());
            }
            //LatLng latLng = new LatLng(mUserLocationMarker.getPosition().getLatitude(), mUserLocationMarker.getPosition().getLongitude());
            double[] newlatLng = distance(new double[]{latLng.getLatitude(), latLng.getLongitude()}, 0.7432, turn);
            LatLng newPos = new LatLng(newlatLng[0], newlatLng[1]);
            if (mParticleLocationMarker == null) {
                        mParticleLocationMarker = mMap.addMarker(new MarkerViewOptions()
                                .position(newPos)
                                .icon(particleIcon));
                    } else {
                        mParticleLocationMarker.setPosition(newPos);
                    }
        }
    }

    private double[] distance(double[] from, double distance, double heading) {
        distance /= 6371000;
        heading = Math.toRadians(heading);
        double fromLat = Math.toRadians(from[0]);
        double fromLng = Math.toRadians(from[1]);
        double cosDistance = Math.cos(distance);
        double sinDistance = Math.sin(distance);
        double sinFromLat = Math.sin(fromLat);
        double cosFromLat = Math.cos(fromLat);
        double sinLat = cosDistance * sinFromLat + sinDistance * cosFromLat * Math.cos(heading);
        double dLng = Math.atan2(
                sinDistance * cosFromLat * Math.sin(heading),
                cosDistance - sinFromLat * sinLat);
        return new double[]{Math.toDegrees(Math.asin(sinLat)), Math.toDegrees(fromLng + dLng)};
    }



    private double[] calculatePointAccel(float[] accelerations) {
        double sum        = Math.pow(accelerations[0], 2) + Math.pow(accelerations[1], 2) + Math.pow(accelerations[2], 2);
        double distance_x = Math.sqrt(Math.pow(0.7438, 2) * Math.pow(accelerations[0], 2) / sum);
        double distance_y = Math.sqrt(Math.pow(0.7438, 2) * Math.pow(accelerations[1], 2) / sum);
        double distance_z = Math.sqrt(Math.pow(0.7438, 2) * Math.pow(accelerations[2], 2)/ sum);
        return new double[]{distance_x, distance_y, distance_z};
    }

    private double[] calculatePoint(double[] coords, float rotationMatrix[]){
        double returnValue[] = new double[3];
        returnValue[0] = rotationMatrix[0] * coords[0] + rotationMatrix[1] * coords[1] + rotationMatrix[2] * coords[2];
        returnValue[1] = rotationMatrix[3] * coords[0] + rotationMatrix[4] * coords[1] + rotationMatrix[5] * coords[2];
        returnValue[2] = rotationMatrix[6] * coords[0] + rotationMatrix[7] * coords[1] + rotationMatrix[8] * coords[2];
        return returnValue;
    }

    private double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = this.degToRad(latitude);
        double lon = this.degToRad(longitude);
        double x = earthRadius * cos(lat) * cos(lon);
        double y = earthRadius * cos(lat) * sin(lon);
        double z = earthRadius * sin(lat);
        return new double[]{x, y, z};
    }
    private double degToRad(double deg) {
        return deg * Math.PI / 180;
    }

    private double angleFromCoordinate(double lat1, double long1, double lat2,
                                               double long2) {
        lat1 = Math.toRadians(lat1);
        long1 = Math.toRadians(long1);
        lat2 = Math.toRadians(lat2);
        long2 = Math.toRadians(long2);


        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        //brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

        return brng;
    }

    @Override
    public void onParticleLocationsUpdated(double[] values,double[] robot) {



            final double[] latlng = this.convertCartesianToSpherical(values);
            final double[] r_latlng = this.convertCartesianToSpherical(robot);
            final double status = values[3];
            LatLng pos = new LatLng(r_latlng[0], r_latlng[1]);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //
                    //LatLng point = new LatLng(latlng[0], latlng[1]);

                    LatLng r_point = new LatLng(r_latlng[0], r_latlng[1]);
                    if (mRobotLocationMarker == null) {
                        mRobotLocationMarker = mMap.addMarker(new MarkerViewOptions()
                                .position(r_point)
                                .icon(robotIcon));
                    } else {
                        mRobotLocationMarker.setPosition(r_point);
                    }
                    rotateMapbox(headingDegrees);
                }
            });
            //higlight nearest waypoint marker
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                highlightNearestMarker(pos);
            }
        });

    }

    private void highlightNearestMarker(LatLng pos){

        double min = 999999999999999999.0;
        int index = -1;
        double point1[] = convertSphericalToCartesian(pos.getLatitude(), pos.getLongitude());
        for(int i=0;i<wayPointsMarker.size();i++){
            Marker marker = wayPointsMarker.get(i);
            marker.setIcon(wayPointIcon);
            double point2[] = convertSphericalToCartesian(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
            double distance = Math.sqrt(Math.pow(point1[0] - point2[0], 2)+ Math.pow(point1[1] - point2[1], 2)+Math.pow(point1[2] - point2[2], 2));
            if(distance < min){
                min = distance;
                index = i;
            }
        }
        Marker marker = wayPointsMarker.get(index);
        waypointLoc   = convertSphericalToCartesian(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
        marker.setIcon(nearestWaypointIcon);
        wayPointsMarker.set(index, marker);


    }

    @Override
    public void showToast(String val,String name) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //FileUtils.saveFile(FingerPrintActivity.this,val,name);
                Toast.makeText(FingerPrintActivity.this,"File succesfully saved",Toast.LENGTH_LONG).show();
            }
        });
    }

    private double[] convertCartesianToSpherical(double[] cartesian) {
        double r = Math.sqrt(cartesian[0] * cartesian[0] + cartesian[1] * cartesian[1] + cartesian[2] * cartesian[2]);
        double lat = Math.toDegrees(asin(cartesian[2] / r));
        double lon = Math.toDegrees(atan2(cartesian[1], cartesian[0]));

        return new double[]{lat, lon};
    }

    private double radToDeg(double deg) {
        return deg * 180 / Math.PI;
    }

    private IBeaconListener createIBeaconListener() {
        return new IBeaconListener() {


            @Override
            public void onIBeaconDiscovered(IBeaconDevice iBeacon, IBeaconRegion region) {

                final String uniqueid = iBeacon.getUniqueId();
                if (uniqueid == null) {
                    return;
                }

                Kalman kalman = beaconsFilterHashMap.get(uniqueid);
                if (kalman == null) {
                    kalman = new Kalman(iBeacon.getDistance(), 1e-3d, 1.0d);
                    kalman.estimatedFilter(new ArrayRealVector(1, iBeacon.getDistance()));
                    beaconsFilterHashMap.put(uniqueid, kalman);
                }

            }

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> iBeacons, IBeaconRegion region) {
                modifiedList.clear();
                //ArrayList<FilteredBeacon> modifiedList = new ArrayList<>();
                //ArrayList<BeaconsDetail> observedBeacons = new ArrayList<>();
                for (IBeaconDevice iBeacon : iBeacons) {
                    FilteredBeacon filteredBeacon = new FilteredBeacon();
                    filteredBeacon.setRssi(iBeacon.getRssi());
                    filteredBeacon.setTxPower(iBeacon.getTxPower());
                    filteredBeacon.setDistance(iBeacon.getDistance());
                    filteredBeacon.setUniqueId(iBeacon.getUniqueId());
                    Kalman kalman = beaconsFilterHashMap.get(iBeacon.getUniqueId());
                    if(kalman != null){
                        double distance = iBeacon.getDistance();
                        double[] predictedDistance = kalman.estimatedFilter(new ArrayRealVector(1, distance));
                        filteredBeacon.setKalmanDistance(predictedDistance[0]);
                    }else{
                        continue;
                    }



                    if (iBeacon.getUniqueId() != null && beaconsHashMap.get(iBeacon.getUniqueId()) != null)
                    {
                        modifiedList.add(filteredBeacon);

                    }
                }
                Collections.sort(modifiedList, new Comparator<FilteredBeacon>() {
                    @Override
                    public int compare(FilteredBeacon filteredBeacon, FilteredBeacon t1) {
                        return Double.compare(filteredBeacon.getDistance(), t1.getDistance());
                    }
                });

            }

            @Override
            public void onIBeaconLost(IBeaconDevice iBeacon, IBeaconRegion region) {

                String uniqueId = iBeacon.getUniqueId();
                if (uniqueId == null) {
                    return;
                }
                beaconsFilterHashMap.remove(uniqueId);
            }
        };
    }



    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

    private synchronized void collectSensorReadings(){
        wifiResults = new ArrayList();
        List<ScanResult> scanResults = wifiManager.getScanResults();

        ArrayList<BeaconsData> beaconsData = new ArrayList();
        ArrayList<LandMark> observedBeacons = new ArrayList<>();
        ArrayList<FilteredBeacon>beaconsList = (ArrayList<FilteredBeacon>) modifiedList.clone();
        //modifiedList.clear();
            for (ScanResult scanResult : scanResults) {
                WifiDetails result = new WifiDetails();
                result.setRssi(scanResult.level);
                result.setBssid(scanResult.BSSID);
//                Gson gson = new GsonBuilder().create();
//                String text = gson.toJson(result);
                wifiResults.add(result);
            }

            for (int i = 0; i < beaconsList.size(); i++) {
                BeaconsDetail matchedBeacons = beaconsHashMap.get(beaconsList.get(i).getUniqueId());
                if (matchedBeacons == null) {
                    Log.e("TAG", " beacon id is null" + beaconsList.get(i).getUniqueId());
                    continue;
                }
                BeaconsData bData = new BeaconsData();
                bData.setBeacon_distance(beaconsList.get(i).getDistance());
                bData.setMax_beacon(beaconsList.size());

                bData.setBeacon_lat(matchedBeacons.getLat());
                bData.setBeacon_lng(matchedBeacons.getLng());

                double[] p1 = convertSphericalToCartesian(matchedBeacons.getLat(), matchedBeacons.getLng());
                bData.setBeaconPosition(p1);
                bData.setBeaconRssi(beaconsList.get(i).getRssi());
                bData.setBeacon_kalman_distance(beaconsList.get(i).getKalmanDistance());
                beaconsData.add(bData);
                LandMark lm = new LandMark(p1[0], p1[1], p1[2], beaconsList.get(i).getUniqueId(), beaconsList.get(i).getDistance());
                observedBeacons.add(lm);
            }

            if(beaconsList.size() < 1){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(FingerPrintActivity.this,"No beacons detected nearby. Please wait before mapping",Toast.LENGTH_LONG).show();
                        //return;
                    }
                });
            }
            String ids="";
            for(int i=0;i<beaconsList.size();i++){
                ids += beaconsList.get(i).getUniqueId()+",";
            }
            //statusTextView.setText(ids);








        if(isAccurate){
            locManager.passSetLandmarksMessage(observedBeacons);
            locManager.passSensorReadings(beaconsData, wifiResults, currentMagValues, deviceMagObserved, waypointLoc);
        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(FingerPrintActivity.this,"Magnetic accuracy is low or unreliable. Please wait before mapping",Toast.LENGTH_LONG).show();
                }
            });
        }




    }
}
