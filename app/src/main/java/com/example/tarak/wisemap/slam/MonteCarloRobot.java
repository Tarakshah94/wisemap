package com.example.tarak.wisemap.slam;

import com.google.gson.Gson;

import java.util.Random;

import static java.lang.Math.PI;
import java.lang.Math;
/**
 * Created by prathyush on 04/12/18.
 */

public class MonteCarloRobot {
    double [][]geofence;
    double mX;
    double mY;
    double mZ;
    double orientation;
    private double turnNoise = 0.0;
    private double forwardNoise = 0.0;
    private double[] senseNoise;
    private double[] previousReading ;
    private int count = 0;
    double sigma[] = {2.5, 1.0, 1.5, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8};

    public MonteCarloRobot(double[][] geofence){
        this.geofence = geofence;
//        mX = Math.random() * (geofence[0][1] - geofence[0][0]) + geofence[0][0];
//        mY = Math.random() * (geofence[1][1] - geofence[1][0]) + geofence[1][0];
//        mZ = Math.random() * (geofence[2][1] - geofence[2][0]) + geofence[2][0];
//        orientation = Math.random() * 2 * PI;


    }

    public void initLocation(double mx,double my,double mz,double orientation){
        mX = mx;
        mY = my;
        mZ = mz;
        this.orientation = orientation;
    }

    public void setNoise(double forwardNoise, double turnNoise) {
        this.forwardNoise = forwardNoise;
        this.turnNoise = turnNoise;
        //5, 5, 5, 7, 7, 7,
        //senseNoise     = new double[]{10/count,0.8,0.8};
    }

    public void setValues(double x, double y, double z, double orientation) throws Exception {
        if (x < geofence[0][1] || x > geofence[0][0]) {
            this.mX = x;
        } else {
            throw new Exception("X index out of bounds");
        }
        if (y < geofence[1][1] || y > geofence[1][0]) {
            this.mY = y;
        } else {
            throw new Exception("Y index out of bounds");
        }
        if (z < geofence[2][1] || z > geofence[2][0]) {
            this.mZ = z;
        } else {
            throw new Exception("Z index out of bounds");
        }
        if (!(orientation < 0 || orientation > (2 * PI))) {

            this.orientation = orientation;
        } else {
            throw new Exception("orientation out of bounds");
        }
    }

    public double getX() {
        return mX;
    }

    public double getY() {
        return mY;
    }

    public double getZ() {
        return mZ;
    }

    private double adjustCoordinates(double value, int index) {

        double adjustedvalue = value;
        if (value < geofence[index][0]) {
            adjustedvalue = geofence[index][1] - (geofence[index][0] - value);
        } else if (value > geofence[index][1]) {
            adjustedvalue = geofence[index][0] + (value - geofence[index][1]);
        }
        return adjustedvalue;
    }


    public MonteCarloRobot move(double stepLength, double turn,Random random_dir,Random random_len) {
        double randomValue = -1 + (1+1) * new Random().nextGaussian();
        double heading = Math.toRadians(turn) + randomValue * turnNoise;
        //double heading = new Random().nextGaussian() * Math.PI *2;
        heading %= 2 * PI;



        if(stepLength > 0) {
            double distanceTravel = stepLength + new Random().nextGaussian() * forwardNoise + 0/*desire mean*/;
            double[] newPosition = nextPosition(distanceTravel, Math.toDegrees(heading));
            mX = adjustCoordinates(newPosition[0], 0);
            mY = adjustCoordinates(newPosition[1], 1);
            mZ = adjustCoordinates(newPosition[2], 2);
        }
        orientation += heading;
        orientation %= 2 * PI;
        if (orientation < 0) {
            orientation += 2 * PI;
        }

        MonteCarloRobot robot_new = deepCopy(this, MonteCarloRobot.class);
        return robot_new;
    }

    public <T> T deepCopy(T object, Class<T> type) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(gson.toJson(object, type), type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private double[] distance(double[] from, double distance, double heading) {
        distance /= 6371000;
        heading = Math.toRadians(heading);
        double fromLat = Math.toRadians(from[0]);
        double fromLng = Math.toRadians(from[1]);
        double cosDistance = Math.cos(distance);
        double sinDistance = Math.sin(distance);
        double sinFromLat = Math.sin(fromLat);
        double cosFromLat = Math.cos(fromLat);
        double sinLat = cosDistance * sinFromLat + sinDistance * cosFromLat * Math.cos(heading);
        double dLng = Math.atan2(
                sinDistance * cosFromLat * Math.sin(heading),
                cosDistance - sinFromLat * sinLat);
        return new double[]{Math.toDegrees(Math.asin(sinLat)), Math.toDegrees(fromLng + dLng)};
    }

    private double[] nextPosition(double meters, double heading) {
        double[] position = convertCartesianToSpherical(new double[]{mX, mY, mZ});
        double[] latLng = distance(position, meters, heading);
        return convertSphericalToCartesian(latLng[0], latLng[1]);
    }

    private double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = Math.toRadians(latitude);
        double lon = Math.toRadians(longitude);
        double x = earthRadius * Math.cos(lat) * Math.cos(lon);
        double y = earthRadius * Math.cos(lat) * Math.sin(lon);
        double z = earthRadius * Math.sin(lat);
        return new double[]{x, y, z};
    }

    private double[] convertCartesianToSpherical(double[] cartesian) {
        double r = Math.sqrt(cartesian[0] * cartesian[0] + cartesian[1] * cartesian[1] + cartesian[2] * cartesian[2]);
        double lat = Math.toDegrees(Math.asin(cartesian[2] / r));
        double lon = Math.toDegrees(Math.atan2(cartesian[1], cartesian[0]));

        return new double[]{lat, lon};
    }

    public double measurementProbability(double[] sensedDistanceVector, double[] distanceVector) {
        double probability = 1.0;


        double sigma = 2.3;

        double squaredDiff = 0.0;
        for (int i = 0; i < sensedDistanceVector.length; i++) {
            squaredDiff += Math.pow(sensedDistanceVector[i]-distanceVector[i],2);
        }
        double gauss = gaussian(0.0,sigma,Math.sqrt(squaredDiff))+Math.exp(-70);
        double prob  = Math.pow(gauss, (0.3));
        probability *= gauss;
        previousReading = sensedDistanceVector;
        return probability;
    }

    public double measurementProbabilityReverse(double[] sensedDistanceVector, double[] distanceVector) {
        double probability = 1.0;


        double sigma = 1.8 * sensedDistanceVector.length;

        double squaredDiff = 0.0;
        for (int i = 0; i < sensedDistanceVector.length; i++) {
            squaredDiff += Math.pow(sensedDistanceVector[i]-distanceVector[i],2);
        }

        double exponent = 1.0/sensedDistanceVector.length;


        double gauss = gaussian(0.0,sigma,Math.sqrt(squaredDiff))+Math.exp(-70);
        double prob  = Math.pow(gauss, exponent);
        probability *= prob;

        return probability;
    }

//    public MonteCarloRobot setPreviousReading(double sensedDistanceVector[]){
//        previousReading = sensedDistanceVector;
//
//    }

    private double gaussian(double mu, double sigma, double x) {
        double numerator = Math.exp(-(Math.pow((mu - x), 2)) / Math.pow(sigma, 2) / 2.0);
        double denominator = Math.sqrt(2 * PI * Math.pow(sigma, 2));
        return Math.exp(-(Math.pow((mu - x), 2)) / Math.pow(sigma, 2) / 2.0) / Math.sqrt(2 * PI * Math.pow(sigma, 2));
    }


}
