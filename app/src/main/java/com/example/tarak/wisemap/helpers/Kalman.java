package com.example.tarak.wisemap.helpers;

import org.apache.commons.math3.filter.DefaultMeasurementModel;
import org.apache.commons.math3.filter.DefaultProcessModel;
import org.apache.commons.math3.filter.KalmanFilter;
import org.apache.commons.math3.filter.MeasurementModel;
import org.apache.commons.math3.filter.ProcessModel;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;


public class Kalman
{
    double constantVoltage = 2d;
    double measurementNoise = 1.0d;
    double processNoise = 1e-3d;

    // A = [ 1 ]
    RealMatrix A = new Array2DRowRealMatrix(new double[]{1d});
    // B = null
    RealMatrix B = null;
    // H = [ 1 ]
    RealMatrix H = new Array2DRowRealMatrix(new double[]{1d});
    // x = [ 10 ]
    RealVector x = new ArrayRealVector(new double[]{constantVoltage});
    // Q = [ 1e-5 ]
    RealMatrix Q = new Array2DRowRealMatrix(new double[]{processNoise});
    // P = [ 1 ]
    RealMatrix P0 = new Array2DRowRealMatrix(new double[]{1d});
    // R = [ 0.1 ]
    RealMatrix R = new Array2DRowRealMatrix(new double[]{measurementNoise});

    ProcessModel pm = new DefaultProcessModel(A, B, Q, x, P0);
    MeasurementModel mm = new DefaultMeasurementModel(H, R);
    KalmanFilter filter = new KalmanFilter(pm, mm);

    // process and measurement noise vectors
    RealVector pNoise = new ArrayRealVector(1);
    RealVector mNoise = new ArrayRealVector(1);

    RandomGenerator rand = new JDKRandomGenerator();


    public Kalman() {
//        this(constantVoltage, processNoise);
    }

    public Kalman(double constantReadingValue,
                  double processNoise,double measurementNoise ) {
        this.constantVoltage = constantReadingValue;
        this.processNoise = processNoise;
        this.measurementNoise = measurementNoise;
        initValues();

    }

    private void initValues() {
        A = new Array2DRowRealMatrix(new double[]{1d});
        H = new Array2DRowRealMatrix(new double[]{1d});
        x = new ArrayRealVector(new double[]{constantVoltage});
        Q = new Array2DRowRealMatrix(new double[]{processNoise});
        P0 = new Array2DRowRealMatrix(new double[]{1d});
        R = new Array2DRowRealMatrix(new double[]{measurementNoise});
        pm = new DefaultProcessModel(A, B, Q, x, P0);
        mm = new DefaultMeasurementModel(H, R);
        pNoise = new ArrayRealVector(1);
        mNoise = new ArrayRealVector(1);
        filter = new KalmanFilter(pm, mm);
    }

    public double[] estimatedFilter(RealVector inp) {
        filter.predict();

        // simulate the process
        pNoise.setEntry(0, processNoise * rand.nextGaussian());

        // x = A * x + p_noise
        x = A.operate(inp).add(pNoise);

        // simulate the measurement
        mNoise.setEntry(0, measurementNoise * rand.nextGaussian());

        // z = H * x + m_noise
        RealVector z = H.operate(x).add(mNoise);

        filter.correct(z);


        return filter.getStateEstimation();
    }


    public double[][] getErrorCovariance() {
        return filter.getErrorCovariance();
    }


}