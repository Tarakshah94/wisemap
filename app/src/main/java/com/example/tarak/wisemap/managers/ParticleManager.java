package com.example.tarak.wisemap.managers;

import android.util.Log;

import com.example.tarak.wisemap.helpers.Kalman;
import com.example.tarak.wisemap.slam.MonteCarloRobot;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeyDuplicateException;
import edu.wlu.cs.levy.CG.KeySizeException;

/**
 * Created by prathyush on 02/12/18.
 */

public class ParticleManager {
    int mdimensions = 0;
    JSONArray readings = new JSONArray();
    KDTree<Integer>tree;

    int particleSize;
    private MonteCarloRobot[] slaves;

    public static final double FORWARD_NOISE = 0.1;
    public static final double SENSE_NOISE = 0.81;
    public static final double TURN_NOISE = 10*Math.PI/180;
    private boolean avoidReasample;
    ArrayList<double[]>particlesLocation = new ArrayList<>();
    private double[] neighbourLocation = new double[3];
    private long previousUpdatedTime = 0;

    private  static final double LOW_PASS_FILTER_CONSTANT = 0.3;
    double neighbours = 8;

    double[] previousObs = new double[3];
    int steps = 0;


    HashMap<Integer,Kalman> neighboursKalman = new HashMap<>();

    public ParticleManager(int dimensions, JSONArray data,int particleSize, double [][] geofence){
        this.tree = null;
        this.mdimensions   = dimensions;
        this.particleSize  = particleSize;
        this.tree = new KDTree<>(this.mdimensions);
        removeDuplicates(data);
        buildTree();
//        removeDuplicatesReverse(data);
//        buildTreeReverse();
        //initParticleFilter(geofence);


    }

    private void initParticleFilter( double [][] geofence){
        slaves = new MonteCarloRobot[particleSize];
        Random r_x = new Random();
        Random r_y = new Random();
        Random r_z = new Random();

        for (int i = 0; i < particleSize; i++) {
            MonteCarloRobot slave = new MonteCarloRobot(geofence);
            double x = r_x.nextDouble() * (geofence[0][1] - geofence[0][0]) + geofence[0][0];
            double y = r_y.nextDouble() * (geofence[1][1] - geofence[1][0]) + geofence[1][0];
            double z = r_z.nextDouble() * (geofence[2][1] - geofence[2][0]) + geofence[2][0];
            double orientation = Math.random() * 2 * Math.PI;
            slave.initLocation(x,y,z,orientation);
            slave.setNoise(FORWARD_NOISE, TURN_NOISE);
            slaves[i] = slave;
        }
    }

    public double[] move(double distance, double turn, double measurements[]) {
        double observations[] = new double[3];
//        for(int i=0;i<measurements.length;i++){
//            measurements[i] =  ((measurements[i] - feature_min[i]) / (feature_max[i] - feature_min[i]));
//        }

        double output[] = new double[4];
//        if(distance == 0){
//            turn = new Random().nextGaussian() * Math.PI *2;
//        }

        for (int i = 0; i < slaves.length; i++) {
            slaves[i] = slaves[i].move(distance, turn, null,null);
        }
        double[] weights = new double[slaves.length];
        JSONArray val  = this.getNearestNeigboursLocation(measurements).get(0);
        for(int k=0;k<val.length();k++){
            try {
                neighbourLocation[k] = val.getDouble(k);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < slaves.length; i++) {
            MonteCarloRobot slave    = slaves[i];

            double userPos[] = {slave.getX(), slave.getY(), slave.getZ()};



            weights[i]               = slaves[i].measurementProbability(userPos, neighbourLocation);
        }
        double max_weight = arrayMax(weights);
        double min_weight = arrayMin(weights);
        double check = max_weight/min_weight;
        output = getUserPosition(weights, slaves);
//        if(steps != 0){
//            avoidReasample = true;
//        }
        slaves = reSample(weights, slaves);
//        steps += 1;
//        steps %= 10;

        return output;
    }

    public double[] getParticlesLocation(){
        return neighbourLocation;
    }

    private MonteCarloRobot[] reSample(double[] weights, MonteCarloRobot[] slaves) {
        MonteCarloRobot[] newSlaves = new MonteCarloRobot[slaves.length];
        int index = (int) (Math.random() * slaves.length);
        double beta = 0.0;
        double max = arrayMax(weights);
        //Robot[] newSlaves = new Robot[numOfSlaves]
        if(avoidReasample){

            for(int j=0;j<particleSize;j++){

                newSlaves[j]             = slaves[j].deepCopy(slaves[j],MonteCarloRobot.class);
            }
            return newSlaves;

        }
        for (int i = 0; i < slaves.length; i++) {
            beta += Math.random() * 2 * max;
            while (beta > weights[index]) {
                beta -= weights[index];
                index = (index + 1) % slaves.length;
            }
            newSlaves[i] = slaves[index].deepCopy(slaves[index],MonteCarloRobot.class);
        }
        return newSlaves;
    }

    private double arrayMax(double[] arr) {
        double max = Double.NEGATIVE_INFINITY;
        for (double cur : arr) {
            max = Math.max(max, cur);
        }
        return max;
    }

    private double arrayMin(double[] arr) {
        double max = Double.POSITIVE_INFINITY;
        for (double cur : arr) {
            max = Math.min(max, cur);
        }
        return max;
    }

    public double[] getUserPosition(){
        double dummyWeights[] = new double[particleSize];
        for (int i=0;i<particleSize;i++){
            dummyWeights[i] = 1.0;
        }
        return getUserPosition(dummyWeights,slaves);
    }

    private double[] getUserPosition(double[] weights, MonteCarloRobot[] slaves) {
        double xMean = 0;
        double yMean = 0;
        double zMean = 0;
        double count = 0;
        double sumWeight = 0;

        double x = 0;
        double y = 0;
        double z = 0;

        for (int i = 0; i < weights.length; i++) {
            sumWeight += weights[i];
        }
        if(sumWeight == 0){
            Log.d("particlefiltererror","sum of all particles equal to zero");
            sumWeight = 0.00001;
        }
        for (int i = 0; i < slaves.length; i++) {
            double weight = weights[i];
            double weightFactor = weight / sumWeight;
            count += weightFactor;
            xMean += slaves[i].getX() * weightFactor;
            yMean += slaves[i].getY() * weightFactor;
            zMean += slaves[i].getZ() * weightFactor;

            x     += slaves[i].getX();
            y     += slaves[i].getY();
            z     += slaves[i].getZ();
        }
        if (count == 0) {
            avoidReasample = true;
            //return new double[]{x/particleSize, y/particleSize, z/particleSize, 0d};
            return new double[]{-1.0,-1.0,-1.0,0.0};
        }
//        if(System.currentTimeMillis() - previousUpdatedTime > 500){
//            avoidReasample = false;
//            previousUpdatedTime = System.currentTimeMillis();
//        }
        avoidReasample = false;
        xMean /= count;
        yMean /= count;
        zMean /= count;
        count = 0;
        for (int i = 0; i < slaves.length; i++) {
            double euclidean = Math.sqrt(Math.pow(slaves[i].getX() - xMean, 2) +
                    Math.pow(slaves[i].getY() - yMean, 2) + Math.pow(slaves[i].getZ() - zMean, 2));
            if (euclidean < 3) {
                count++;
            }
        }

        double status = 1;
        if (count < (0.90 * particleSize)) {
            status = 0;
        }
        return new double[]{xMean, yMean, zMean, status};
    }

    private void removeDuplicates(JSONArray data){
        JSONObject readingsHash = new JSONObject();
        for (int i=0;i<data.length();i++) {
            try {
                JSONObject obj = data.getJSONObject(i);


                double beacon_x = Double.parseDouble(obj.get("beacon_x").toString());
                double beacon_y = Double.parseDouble(obj.get("beacon_y").toString());
                double beacon_z = Double.parseDouble(obj.get("beacon_z").toString());

//                beacon_x        = (beacon_x -  feature_min[2])/(feature_max[2] - feature_min[2]);
//                beacon_y        = (beacon_y -  feature_min[3])/(feature_max[3] - feature_min[3]);
//                beacon_z        = (beacon_z -  feature_min[4])/(feature_max[4] - feature_min[4]);


//                //magnetic related

                double magnitude = Double.parseDouble(obj.get("magnitude").toString());
                double xy_mag    = Double.parseDouble(obj.get("xy_mag").toString());

                //magnitude        = (magnitude -  feature_min[0])/(feature_max[0] - feature_min[0]);
                //xy_mag           = (xy_mag -  feature_min[1])/(feature_max[1] - feature_min[1]);


                //String key = magnitude +"magnitude"+beacon_x+"1_x"+beacon_y+"1_y"+beacon_z+"1_z"+x_mag+"3_x"+xz_mag+"3_y"+z_mag+"3_z";
                String key = beacon_x+"1_x"+beacon_y+"1_y"+beacon_z+"1_z";

                //String key = magnitude +"magnitude"+beacon_x+"1_x"+beacon_y+"1_y"+beacon_z;


                readingsHash.put(key,obj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Iterator<String> keys = readingsHash.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            try {
                if (readingsHash.get(key) instanceof JSONObject) {
                    readings.put(readingsHash.get(key));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void removeDuplicatesReverse(JSONArray data){
        JSONObject readingsHash = new JSONObject();
        for (int i=0;i<data.length();i++) {
            try {
                JSONObject obj = data.getJSONObject(i);

                JSONArray particles = new JSONArray(obj.get("particles").toString());
                String key = particles.get(0).toString() +"x"+particles.get(0).toString()+"y"+particles.get(0).toString()+"z";

                readingsHash.put(key,obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Iterator<String> keys = readingsHash.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            try {
                if (readingsHash.get(key) instanceof JSONObject) {
                    readings.put(readingsHash.get(key));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void buildTree() {
        double [][] keys = new double [readings.length()][this.mdimensions];
        ArrayList<Integer>invalidkeys = new ArrayList<>();
        for (int i=0;i<readings.length();i++){
            try {
                JSONObject obj = readings.getJSONObject(i);

                double beacon_x = Double.parseDouble(obj.get("beacon_x").toString());
                double beacon_y = Double.parseDouble(obj.get("beacon_y").toString());
                double beacon_z = Double.parseDouble(obj.get("beacon_z").toString());

//                beacon_x        = (beacon_x -  feature_min[2])/(feature_max[2] - feature_min[2]);
//                beacon_y        = (beacon_y -  feature_min[3])/(feature_max[3] - feature_min[3]);
//                beacon_z        = (beacon_z -  feature_min[4])/(feature_max[4] - feature_min[4]);


//                //magnetic related

                //double magnitude = Double.parseDouble(obj.get("magnitude").toString());
                //double xy_mag    = Double.parseDouble(obj.get("xy_mag").toString());

                //magnitude        = (magnitude -  feature_min[0])/(feature_max[0] - feature_min[0]);
                //xy_mag           = (xy_mag -  feature_min[1])/(feature_max[1] - feature_min[1]);



                //third_beacon_x, third_beacon_y, third_beacon_z,
                double observations[] = { beacon_x, beacon_y, beacon_z};
                //double observations[] = {beacon_x, beacon_y, beacon_z, nearest_beacon_x, nearest_beacon_y, nearest_beacon_z,nearest1_beacon_x, nearest1_beacon_y, nearest1_beacon_z, magnitude};
                //double observations[] = {beacon_x, beacon_y, beacon_z, magnitude};
                keys[i]                = observations;
                this.tree.insert(keys[i],i);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (KeyDuplicateException e) {

                e.printStackTrace();
            } catch (KeySizeException e) {
                e.printStackTrace();
            }
        }


    }

    private void buildTreeReverse() {
        double [][] keys = new double [readings.length()][this.mdimensions];
        ArrayList<Integer>invalidkeys = new ArrayList<>();
        for (int i=0;i<readings.length();i++){
            try {
                JSONObject obj = readings.getJSONObject(i);

                JSONArray particles = new JSONArray(obj.get("particles").toString());

                double observations[] = {Double.parseDouble(particles.get(0).toString()), Double.parseDouble(particles.get(1).toString()), Double.parseDouble(particles.get(2).toString())};
                keys[i]                = observations;
                this.tree.insert(keys[i],i);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (KeyDuplicateException e) {

                e.printStackTrace();
            } catch (KeySizeException e) {
                e.printStackTrace();
            }
        }


    }

    public double[] getNearestNeigboursLocationReverse(double obs[]){
        double magnitude=0,beacon_x=0,beacon_y=0,beacon_z=0,magnitude_world=0,xy_mag=0,beaconrssi_x=0,beaconrssi_y=0,beaconrssi_z = 0;
        try {
            List<Integer> nbrs = this.tree.nearest(obs, (int) neighbours);
            int inde = nbrs.get(0);
            JSONObject reading  = readings.getJSONObject(inde);
            JSONArray particles = new JSONArray(reading.get("particles").toString());


            double nearestDistance = Math.sqrt(Math.pow(Double.parseDouble(particles.get(0).toString()) - obs[0], 2) + Math.pow(Double.parseDouble(particles.get(1).toString()) - obs[1], 2) + Math.pow(Double.parseDouble(particles.get(2).toString()) - obs[2], 2));
            if(nearestDistance > 3){
                return new double[]{999999.0,999999.0,9999999.0, 0.0, 0.0, 0.0};
            }
            for(int ind : nbrs){
                JSONObject obj  = readings.getJSONObject(ind);

//                beacon_x        = Double.parseDouble(obj.get("beacon_x").toString());
//                beacon_y        = Double.parseDouble(obj.get("beacon_y").toString());
//                beacon_z        = Double.parseDouble(obj.get("beacon_z").toString());
                double magnitude_       = Double.parseDouble(obj.get("magnitude").toString());
                double magnitude_world_ = Double.parseDouble(obj.get("magnitude_world").toString());
                double xy_mag_          = Double.parseDouble(obj.get("xy_mag").toString());
                double beaconrssi_x_        = Double.parseDouble(obj.get("beaconrssi_x").toString());
                double beaconrssi_y_        = Double.parseDouble(obj.get("beaconrssi_y").toString());
                double beaconrssi_z_        = Double.parseDouble(obj.get("beaconrssi_z").toString());




//                magnitude                   = ((magnitude - feature_min[0])/(feature_max[0] - feature_min[0]));
//                magnitude_world                   = ((magnitude_world - feature_min[1])/(feature_max[1] - feature_min[1]));
//                xy_mag                   = ((xy_mag - feature_min[2])/(feature_max[2] - feature_min[2]));
//                beacon_x                   = ((beacon_x - feature_min[3])/(feature_max[3] - feature_min[3]));
//                beacon_y                   = ((beacon_y - feature_min[4])/(feature_max[4] - feature_min[4]));
//                beacon_z                   = ((beacon_z - feature_min[5])/(feature_max[5] - feature_min[5]));
//                beaconrssi_x                   = ((beaconrssi_x - feature_min[6])/(feature_max[6] - feature_min[6]));
//                beaconrssi_y                   = ((beaconrssi_y - feature_min[7])/(feature_max[7] - feature_min[7]));
//                beaconrssi_z                   = ((beaconrssi_z - feature_min[8])/(feature_max[8] - feature_min[8]));


                magnitude               += magnitude_;
                magnitude_world         += magnitude_world_;
                xy_mag                  += xy_mag_;
                beaconrssi_x            += beaconrssi_x_;
                beaconrssi_y            += beaconrssi_y_;
                beaconrssi_z            += beaconrssi_z_;



            }
        } catch (KeySizeException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return new double[]{magnitude_world/neighbours, magnitude/neighbours,beaconrssi_x/neighbours, beaconrssi_y/neighbours, beaconrssi_z/neighbours};
    }


    public List<JSONArray> getNearestNeigboursLocation(double obs[]){
        List<JSONArray> neigbours_particles = new ArrayList<>();
        try {
            List<Integer> nbrs = this.tree.nearest(obs,1);
            for(int ind : nbrs){
                JSONObject reading  = readings.getJSONObject(ind);
                JSONArray particles = new JSONArray(reading.get("particles").toString());
                JSONArray measurement = new JSONArray();
                measurement.put(particles.get(0));
                measurement.put(particles.get(1));
                measurement.put(particles.get(2));
                neigbours_particles.add(measurement);
            }
        } catch (KeySizeException e) {
            e.printStackTrace();
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return neigbours_particles;
    }


}
