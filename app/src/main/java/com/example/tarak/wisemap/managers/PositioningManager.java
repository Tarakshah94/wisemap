package com.example.tarak.wisemap.managers;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.example.tarak.wisemap.models.BeaconsData;
import com.example.tarak.wisemap.models.LandMark;
import com.example.tarak.wisemap.models.WifiDetails;
import com.example.tarak.wisemap.slam.FastSlam;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PositioningManager {
    HandlerThread handlerThread;
    Handler messenger;
    private static final int RUN_SIMULATION = 0;
    private static final int INIT_SLAM      = 1;
    private static final int SET_LANDMARKS  = 2;
    private static final int SET_SENSOR_READINGS  = 3;
    private static final int SET_MAX_BEACONS = 4;
    private static final int SET_START_STOP = 5;
    private static final int SET_ROBOT_POSITION = 6;
    private FastSlam slam;
    //private WeakReference<FastSlam>slam;
    int messagenumber = 0;
//    //this is for mumbai airport below
//    double[][] geofence = {{1772612.5964885126-5, 1772947.8879604405+5}, {5753319.554939809-5, 5753471.8145920625+5},{2084466.295558238-5, 2084623.9072973288+5}};
    //this is for office ecostar
    double[][] geofence = {{1774160.4411612363-5, 1774210.0650170543+5}, {5750304.43580926-5, 5750342.137329916+5},{2091770.9366060568-5, 2091856.6414782405+5}};

    //1774172.0771493486 , 5750342.4457867, 2091773.4573436473
    //1774212.153703176, 5750329.125517453, 2091776.0831115805
    //1774213.4929817605, 5750298.9105272535, 2091858.0068710546
    //1774154.2795148077, 5750317.18008396, 2091858.0068710546
    public PositioningManager(LocationUpdater updater) {


        handlerThread = new HandlerThread("PositioningThread", Thread.MAX_PRIORITY);
        handlerThread.start();

        messenger = new Handler(handlerThread.getLooper(), callback);
        slam      = new FastSlam(2, geofence, updater);

        slam.setMinMaxObservations(0,2);
    }

    public void invalidate(){
        if(handlerThread != null && handlerThread.isAlive()){
            handlerThread.quit();
        }
        slam.invalidate();
        slam    =   null;
    }

    private Handler.Callback callback = new Handler.Callback(){
        @Override
        public boolean handleMessage(Message msg) {
            messagenumber += 1;
            if(messagenumber == 500){
                System.gc();
                System.runFinalization();
                messagenumber = 0;
            }
            if (msg.what == RUN_SIMULATION) {
                double[] params = (double[]) msg.obj;
                runSimulation(params);
            }else if(msg.what == INIT_SLAM){
                double[] params = (double[])msg.obj;
                initSlam(params);
            }else if(msg.what == SET_LANDMARKS){
                ArrayList<LandMark>landmarks = (ArrayList<LandMark>) msg.obj;
                setLandMarks(landmarks);
            }
            else if(msg.what == SET_SENSOR_READINGS){
                try {
                    System.out.print("xxx");
                    String x = msg.obj.toString();
                    JSONObject readings = new JSONObject(msg.obj.toString());
                    setSetSensorReadings(readings);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }else if(msg.what == SET_MAX_BEACONS){

                slam.setMinMaxObservations(0,(int)msg.obj);
            }else if(msg.what == SET_START_STOP){
                slam.setStartStop((boolean)msg.obj);
//                if(shouldshow){
//                    loc
//                }

            }else if(msg.what == SET_ROBOT_POSITION){
                double[] loc = (double[])msg.obj;
                setRobotPosition(loc);

            }
            return false;
        }
    };

    private void setRobotPosition(double[] loc) {
        slam.setRobotPosition(loc[0], loc[1], loc[2]);
    }

    private void initSlam(double[] params){
        slam.initManager(params[0], params[1], params[2], params[3]);
    }
    private void runSimulation(double[] params){
        slam.simulateRobotWorld(params[0], params[1]);
    }
    private void setLandMarks(ArrayList<LandMark>landmarks){
        slam.setLandMarks(landmarks);
    }
    private void setSetSensorReadings(JSONObject obj){
        //Log.d("debug","before sensor reading");
        slam.setSensorReadings(obj);
    }

    public void passSimulateMessage(double distance,double turn){
        Message msg = new Message();
        msg.what    = RUN_SIMULATION;
        msg.obj     = new double[]{distance, turn};
        messenger.sendMessage(msg);
        //runSimulation(new double[]{distance, turn});
    }

    public void passInitializeMessage(double x, double y,double z,double orientation){
        Message msg = new Message();
        msg.what    = INIT_SLAM;
        msg.obj     = new double[]{x, y, z, orientation};
        messenger.sendMessage(msg);
        //initSlam(new double[]{x, y, z, orientation});
    }

    public void passRobotPositionSetMessage(double x, double y, double z){
        Message msg = new Message();
        msg.what    = SET_ROBOT_POSITION;
        msg.obj     = new double[]{x, y, z};
        messenger.sendMessage(msg);
    }

    public void passSetLandmarksMessage(ArrayList<LandMark>landMarks){
        Message msg = new Message();
        msg.what    = SET_LANDMARKS;
        msg.obj     = landMarks;
        messenger.sendMessage(msg);
        //setLandMarks(landMarks);
    }
    public void passSensorReadings(ArrayList<BeaconsData> bData, ArrayList<WifiDetails> wData, float[] magValues, float[] deviceMag, double waypointLoc[]){
        Message msg = new Message();
        msg.what    = SET_SENSOR_READINGS;
        JSONObject obsObject = new JSONObject();
        Gson gson = new Gson();
        try {
            JsonElement element = gson.toJsonTree(bData, new TypeToken<List<BeaconsData>>() {}.getType());
            if (! element.isJsonArray()) {

                System.out.print("check");
            }

            JsonElement element2 = gson.toJsonTree(wData, new TypeToken<List<WifiDetails>>() {}.getType());
            if (! element2.isJsonArray()) {

                System.out.print("check");
            }

            obsObject.put("bluetooth_data", element.getAsJsonArray());
            obsObject.put("wifi_data", element2.getAsJsonArray());
            obsObject.put("mag_data", new JSONArray(magValues));
            obsObject.put("device_mag_data", new JSONArray(deviceMag));
            obsObject.put("waypointloc", new JSONArray(waypointLoc));
            //setSetSensorReadings(obsObject);
            msg.obj = obsObject;
            messenger.sendMessage(msg);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void passMaxBeacons(int number){
        Message msg = new Message();
        msg.what    = SET_MAX_BEACONS;
        msg.obj     = number;
        messenger.sendMessage(msg);
    }

    public void passStartStopMessage(boolean flag){
        Message msg = new Message();
        msg.what    = SET_START_STOP;
        msg.obj     = flag;
        messenger.sendMessage(msg);
    }
}
