package com.example.tarak.wisemap.models;

/**
 * Created by prathyush on 01/07/17.
 */
public class FilteredBeacon {
    int rssi;
    int txPower;
    double coveriError;
    double kalmanDistance;
    double kalmanRssi;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    double distance;

    public double getKalmanDistance() {
        return kalmanDistance;
    }

    public void setKalmanDistance(double kalmanDistance) {
        this.kalmanDistance = kalmanDistance;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getTxPower() {
        return txPower;
    }

    public void setTxPower(int txPower) {
        this.txPower = txPower;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public double getKalmanRssi() {
        return kalmanRssi;
    }

    public void setKalmanRssi(double kalmanRssi) {
        this.kalmanRssi = kalmanRssi;
    }

    String uniqueId;


    @Override
    public String toString() {
        return "Unique Id " + uniqueId + "," + coveriError + "";
    }

    public double getCoveriError() {
        return coveriError;
    }

    public void setCoveriError(double coveriError) {
        this.coveriError = coveriError;
    }
}
