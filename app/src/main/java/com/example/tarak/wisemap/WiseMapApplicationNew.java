package com.example.tarak.wisemap;

import android.app.Application;

import com.kontakt.sdk.android.common.KontaktSDK;
import com.mapbox.mapboxsdk.Mapbox;

public class WiseMapApplicationNew extends Application {
    private static boolean initialized = false;
    @Override
    public void onCreate() {
        super.onCreate();
        KontaktSDK.initialize(this);
        Mapbox.getInstance(getApplicationContext(), getString(R.string.mapbox_access_token));
    }

    public  void callifNotCalled(){
        if(!initialized){
            Mapbox.getInstance(getApplicationContext(), getString(R.string.mapbox_access_token));
        }
    }

}
