package com.example.tarak.wisemap.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.tarak.wisemap.R;
import com.example.tarak.wisemap.adapters.ListItemAdapter;

import java.io.File;

public class WaypointFileSelector extends AppCompatActivity {

    static final String directoryPath = Environment.getExternalStorageDirectory().getPath()+"/insuide";
    static final String magDirectoryPath = Environment.getExternalStorageDirectory().getPath()+"/finginsuide";
    String filesList[];
    ImageButton addNewFile;
    RecyclerView fileListView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter mAdapter;
    final static int REQUEST_CODE_WAYPOINT = 1;
    final static int BACKTO_FINGER_PRINT = 2;
    String filename;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waypoint_file_selector);
        addNewFile    = findViewById(R.id.addWayPointButton);
        fileListView  = findViewById(R.id.waypointsList);
        //fileListView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        checkDirAndCreate();
        getFilesList();
        createAndAttachListAdapter();
        addNewFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNameFromDialog();
            }
        });
    }

    public void getNameFromDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(WaypointFileSelector.this);
        alertDialog.setTitle("Name");
        alertDialog.setMessage("Enter the name of file to Save");
        final EditText input = new EditText(WaypointFileSelector.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        //alertDialog.setIcon(android.support.design.R.drawable.navigation_empty_icon);
        alertDialog.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();
                if(!name.equals("")){
                    filename = directoryPath+"/"+name+".json";
                    proceedToWaypointEditorActivity();
                    dialog.cancel();
                }else{
                    Toast.makeText(WaypointFileSelector.this, "Filename should not be empty", Toast.LENGTH_LONG).show();
                }
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void proceedToWaypointEditorActivity() {
        Intent intent = new Intent(WaypointFileSelector.this, WayPointEditor.class);
        intent.putExtra("filename",filename);
        startActivityForResult(intent, REQUEST_CODE_WAYPOINT);
    }



    private void createAndAttachListAdapter() {
        mAdapter = new ListItemAdapter(filesList, this);
        fileListView.setLayoutManager(layoutManager);
        fileListView.setAdapter(mAdapter);
    }

    private void getFilesList() {
        File wayPointsDir = new File(directoryPath);
        File[] files = wayPointsDir.listFiles();
        filesList = new String[files.length];
        for(int i=0;i<files.length;i++){
            Uri file         = Uri.fromFile(files[i]);
            String extension = MimeTypeMap.getFileExtensionFromUrl(file.toString());
            if(extension.equals("json")) {
                filesList[i] = files[i].getPath();
            }


        }
    }

    private void checkDirAndCreate() {
        File file = new File(directoryPath);
        if(!file.exists()){
            file.mkdirs();
        }
    }

    public void listItemClicked(int index){
        Toast.makeText(this,"Item number "+String.valueOf(index)+" clicked",Toast.LENGTH_LONG).show();
        filename = filesList[index];
        proceedToWaypointEditorActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == REQUEST_CODE_WAYPOINT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(WaypointFileSelector.this, FingerPrintActivity.class);
                intent.putExtra("filename", filename);
                startActivity(intent);
            }
        }
    }
}
