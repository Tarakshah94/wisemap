package com.example.tarak.wisemap.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Furqan on 30-06-2017.
 */

public class BeaconsDetail {
    @SerializedName("geo")
    private List<Double> geo = null;
    @SerializedName("nodes")
    private List<Object> nodes = null;
    @SerializedName("insertedby")
    private String insertedby;
    @SerializedName("venue")
    private String venue;
    @SerializedName("appname")
    private String appname;
    @SerializedName("status")
    private String status;
    @SerializedName("txPower")
    private Integer txPower;
    @SerializedName("major")
    private Integer major;
    @SerializedName("minor")
    private Integer minor;
    @SerializedName("sliceid")
    private String sliceid;
    @SerializedName("level")
    private String level;
    @SerializedName("proximity")
    private String proximity;
    @SerializedName("uniqueId")
    private String uniqueId;
    @SerializedName("namespace")
    private String namespace;
    @SerializedName("name")
    private String name;
    @SerializedName("lng")
    private double lng;
    @SerializedName("lat")
    private double lat;
    @SerializedName("interval")
    private Integer interval;
    @SerializedName("instanceId")
    private String instanceId;
    @SerializedName("id")
    private String id;
    @SerializedName("firmware")
    private String firmware;
    @SerializedName("deviceType")
    private String deviceType;
    @SerializedName("enabled")
    private Boolean enabled;
    @SerializedName("alias")
    private String alias;
    @SerializedName("access")
    private String access;
    @SerializedName("__v")
    private Integer v;

    @SerializedName("userlng")
    private Double userLng;
    @SerializedName("userlat")
    private Double userLat;
    public List<Double> getGeo() {
        return geo;
    }

    public void setGeo(List<Double> geo) {
        this.geo = geo;
    }

    public List<Object> getNodes() {
        return nodes;
    }

    public void setNodes(List<Object> nodes) {
        this.nodes = nodes;
    }

    public String getInsertedby() {
        return insertedby;
    }

    public void setInsertedby(String insertedby) {
        this.insertedby = insertedby;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTxPower() {
        return txPower;
    }

    public void setTxPower(Integer txPower) {
        this.txPower = txPower;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public String getSliceid() {
        return sliceid;
    }

    public void setSliceid(String sliceid) {
        this.sliceid = sliceid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getProximity() {
        return proximity;
    }

    public void setProximity(String proximity) {
        this.proximity = proximity;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Double getUserLng() {
        return userLng;
    }

    public void setUserLng(Double userLng) {
        this.userLng = userLng;
    }

    public Double getUserLat() {
        return userLat;
    }

    public void setUserLat(Double userLat) {
        this.userLat = userLat;
    }
}
