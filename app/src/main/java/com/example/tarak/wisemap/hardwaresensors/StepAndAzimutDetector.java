package com.example.tarak.wisemap.hardwaresensors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.opengl.Matrix;
import android.view.Display;
import android.view.OrientationEventListener;

import com.example.tarak.wisemap.helpers.PCA;
import com.example.tarak.wisemap.managers.SharedPreferenceManager;
import com.example.tarak.wisemap.models.AccelSamples;
import com.example.tarak.wisemap.utils.FileUtils;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import static android.view.OrientationEventListener.ORIENTATION_UNKNOWN;

public class StepAndAzimutDetector implements SensorEventListener2 {

    private static final float SHAKE_THRESHOLD = 800;
    private static final int ACCEL_SAMPLE_SIZE = 100;
    public static float[] gravity = new float[3];
    public static float[] linear = new float[4];
    public static float[] linearRemapped = new float[4];
    public static float[] origMagn = new float[3];
    public static float[] magn = new float[4];
    public static float[] calibratedmagn = new float[3];
    public static float[] magn_remapped = new float[4];
    public static float[] origAcl = new float[3]; //only needed for logging/debug purposes
    public static double startLat;
    public static double startLon;
    public static int stepCounter = 0;
    public static double azimuth;
    public static int altitude = 150;
    public static double distanceLongitude;
    public static float stepLength;
    public static boolean export;
    public static String version;
    public static float lastErrorGPS;
    public static int units = 0;
    static File posFile;
    static File sensorFile;
    private static double oldAzimuth = 0;
    private static float frequency;
    private static boolean stepBegin = false;
    private static float[] iMatrix = new float[9];
    private static float[] RMatrix = new float[16];
    private static float[] RMatrixRemapped = new float[16];
    private static float[] RMatrixTranspose = new float[16];
    private static float[] orientation = new float[3];
    private static double deltaLat;
    private static double deltaLon;
    private static float iStep = 1;
    private static float ugainA;
    private static float ugainM;
    private static double[] xa0 = new double[4];
    private static double[] ya0 = new double[4];
    private static double[] xa1 = new double[4];
    private static double[] ya1 = new double[4];
    private static double[] xa2 = new double[4];
    private static double[] ya2 = new double[4];
    private static float[] tpA = new float[3];
    private static float[] tpM = new float[3];
    private static double[] xm0 = new double[4];
    private static double[] ym0 = new double[4];
    private static double[] xm1 = new double[4];
    private static double[] ym1 = new double[4];
    private static double[] xm2 = new double[4];
    private static double[] ym2 = new double[4];
    private static float stepThreshold = 2.0f;
    private static boolean sensorFileNotExisting = true;
    private static boolean positionsFileNotExisting = true;
    private static float decl = 0;
    private static boolean initialStep;
    private static boolean newStepDetected = false;
    private static boolean startedToExport = false;
    private static long startTime;
    public boolean gyroExists = false;
    private AdvancedOrientationSensorProvider mOrientationProvider;
    private SensorManager mSensorManager;
    private boolean alreadyWaitingForAutoCorrect = false;
    private int stepsToWait = 0;
    private int autoCorrectFactor = 1;
    private int magnUnits;
    private int aclUnits;
    private boolean autoCorrect = false;
    private SharedPreferences settings;
    private onStepUpdateListener stepUpdateListener;
    public static boolean  isPositionSet = false;
    private boolean isAccurate = false;
    private long timePassed;
    double distance = 0.0;
    long lasttime = 0;
    int inclination = 0;
    //JSONArray data = new JSONArray();
    double strideLength = 0.6;
    String direction = "0";
    int axisX;
    int axisY;

    private final int ORIENTATION_PORTRAIT = ExifInterface.ORIENTATION_ROTATE_90; // 6
    private final int ORIENTATION_LANDSCAPE_REVERSE = ExifInterface.ORIENTATION_ROTATE_180; // 3
    private final int ORIENTATION_LANDSCAPE = ExifInterface.ORIENTATION_NORMAL; // 1
    private final int ORIENTATION_PORTRAIT_REVERSE = ExifInterface.ORIENTATION_ROTATE_270; // 8

    int smoothness = 1;
    private float averagePitch = 0;
    private float averageRoll = 0;
    private int screenOrientation = ORIENTATION_PORTRAIT;

    private float[] pitches =  new float[smoothness];
    private float[] rolls   = new float[smoothness];

    float pitch;
    float roll;
    private long gyroscopestarttime = 0l;
    private float azimuthdevice;

    private SharedPreferenceManager mSharedManager;
    float error_x;
    float error_y;
    float error_z;
    private double[] phoneLinearAccel = new double[3];
    private double[] userLinearAccel = new double[3];
    private float accelSmoothingFactor = 0.25f;

    private RealMatrix pcaMatrix;
    private long pcaWindow = 10;
    private long pcaStart = 0L;
    private double userOrientation;
    private ArrayDeque<AccelSamples> accelSampleQueue = new ArrayDeque<>();

    private OrientationEventListener surfaceRotationListener;
    public  static boolean isLogging=false;
    private float magazimuth =0.0f;
    private int axis = 2;


    private ArrayList<Float> verticalAccValues = new ArrayList<>();

    private ArrayList<ArrayList<Float>>forVelocity = new ArrayList<>();
    private long timeForVelocity = 0;
    private float hardIronOffsetX,hardIronOffsetY,hardIronOffsetZ = 0f;
    private float softIronScaleX,softIronScaleY,softIronScaleZ = 1f;
    public  void saveToFile(){
//        FileUtils.saveFile((Context)this.stepUpdateListener, data.toString(), "logger");
//        data = new JSONArray();
    }

    public  StepAndAzimutDetector(Context context) {

        if (context instanceof onStepUpdateListener) {
            stepUpdateListener = (onStepUpdateListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mSharedManager      =  new SharedPreferenceManager(context);

        hardIronOffsetX     = mSharedManager.getFloat("hardironx");
        hardIronOffsetY     = mSharedManager.getFloat("hardirony");
        hardIronOffsetZ     = mSharedManager.getFloat("hardironz");

        softIronScaleX      = mSharedManager.getFloat("softironx");
        softIronScaleY      = mSharedManager.getFloat("softirony");
        softIronScaleZ      = mSharedManager.getFloat("softironz");



        settings = context.getApplicationContext().getSharedPreferences(context.getApplicationContext().getPackageName() + "_preferences", Context.MODE_PRIVATE);
        autoCorrect = settings.getBoolean("autocorrect", false);

        positionsFileNotExisting = true;
        sensorFileNotExisting = true;

        stepCounter = 0;
        initialStep = true;

        magn[0] = magn[1] = magn[2] = gravity[0] = gravity[1] = 0;
        gravity[2] = 9.81f;
        ugainM = ugainA = 154994.3249f;
        tpA[0] = tpM[0] = 0.9273699683f;
        tpA[1] = tpM[1] = -2.8520278186f;
        tpA[2] = tpM[2] = 2.9246062355f;



        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        axisX = SensorManager.AXIS_X;
        axisY = SensorManager.AXIS_Y;


        if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            gyroExists = true;
            mOrientationProvider = new AdvancedOrientationSensorProvider((SensorManager) context.getSystemService(Context.SENSOR_SERVICE), context);
//            if (gyroExists) {
//                //use gyroscope with impovedOrientationProvider
//                mOrientationProvider.start();
//            }
        }

        //initOrientationProvider();


    }



    /**
     * Initializing
     *
     * @param startLat
     * @param startLon
     * @param distanceLongitude
     */
    public static  void initialize(double startLat, double startLon, double distanceLongitude, double altitude) {
        StepAndAzimutDetector.startLat = startLat;
        StepAndAzimutDetector.startLon = startLon;
        StepAndAzimutDetector.distanceLongitude = distanceLongitude;
        StepAndAzimutDetector.altitude = (int) altitude;
        //StepAndAzimutDetector.lastErrorGPS = lastErrorGPS;
        trueNorth();
    }

    public static  void setLocation(double lat, double lon) {
        startLat = lat;
        startLon = lon;
    }

    private static  void trueNorth() {
        long time = System.currentTimeMillis();
        GeomagneticField geo = new GeomagneticField((float) startLat, (float) startLon, altitude, time);
        decl = geo.getDeclination();
        //decl =0;
    }



    public  void startSensors(boolean initialStep) {
        isPositionSet = initialStep;
        aclUnits = 0;
        magnUnits = 0;
        startTime = System.nanoTime();
        try {
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME);
            //mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_GAME);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (gyroExists) {
                //use gyroscope with impovedOrientationProvider
                mOrientationProvider.start();
        }


    }





    public  void reactivateSensors() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(StepAndAzimutDetector.this);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 1);
            mSensorManager.registerListener(StepAndAzimutDetector.this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 1);

            if (gyroExists) {
                //use gyroscope with impovedOrientationProvider
                mOrientationProvider.start();
            }
        }
    }

    public  void pauseSensors() {
        try {
            mSensorManager.unregisterListener(this);
            //new orientation provider
            mOrientationProvider.stop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public  void imbaMagnetic(float[] magnetic) {
        // LowPass 0.5Hz for alpha0
        //double magnitude = Math.sqrt(magnetic[0]*magnetic[0] + magnetic[1]*magnetic[1]+magnetic[2]*magnetic[2]);
        xm0[0] = xm0[1];
        xm0[1] = xm0[2];
        xm0[2] = xm0[3];
        xm0[3] = magnetic[0] / ugainM;
        ym0[0] = ym0[1];
        ym0[1] = ym0[2];
        ym0[2] = ym0[3];
        ym0[3] = (xm0[0] + xm0[3]) + 3 * (xm0[1] + xm0[2]) + (tpM[0] * ym0[0]) + (tpM[1] * ym0[1]) + (tpM[2] * ym0[2]);
        magn[0] = (float) ym0[3];

        // LowPass 0.5Hz for alpha1
        xm1[0] = xm1[1];
        xm1[1] = xm1[2];
        xm1[2] = xm1[3];
        xm1[3] = magnetic[1] / ugainM;
        ym1[0] = ym1[1];
        ym1[1] = ym1[2];
        ym1[2] = ym1[3];
        ym1[3] = (xm1[0] + xm1[3]) + 3 * (xm1[1] + xm1[2]) + (tpM[0] * ym1[0]) + (tpM[1] * ym1[1]) + (tpM[2] * ym1[2]);
        magn[1] = (float) ym1[3];

        // LowPass 0.5Hz for alpha2
        xm2[0] = xm2[1];
        xm2[1] = xm2[2];
        xm2[2] = xm2[3];
        xm2[3] = magnetic[2] / ugainM;
        ym2[0] = ym2[1];
        ym2[1] = ym2[2];
        ym2[2] = ym2[3];
        ym2[3] = (xm2[0] + xm2[3]) + 3 * (xm2[1] + xm2[2]) + (tpM[0] * ym2[0]) + (tpM[1] * ym2[1]) + (tpM[2] * ym2[2]);
        magn[2] = (float) ym2[3];


        SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magn);
        Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
        //Matrix.multiplyMV(linearRemapped, 0, RMatrixTranspose, 0, linear, 0);
        //SensorManager.remapCoordinateSystem(RMatrix, x_world, y_world, RMatrixRemapped);




        Matrix.multiplyMV(magn_remapped, 0, RMatrixTranspose,0,magn,0);
        float A_D[] = magn.clone();




        stepUpdateListener.onMagneticValueUpdate(magn_remapped.clone(), isAccurate, A_D);


    }

    public  void imbaGravity(float[] accel) {
        // LowPass 0.5Hz for alpha0
        double magnitude = Math.sqrt(accel[0]*accel[0] + accel[1]*accel[1]+accel[2]*accel[2]);
        xa0[0] = xa0[1];
        xa0[1] = xa0[2];
        xa0[2] = xa0[3];
        xa0[3] = accel[0] / ugainA;
        ya0[0] = ya0[1];
        ya0[1] = ya0[2];
        ya0[2] = ya0[3];
        ya0[3] = (xa0[0] + xa0[3]) + 3 * (xa0[1] + xa0[2]) + (tpA[0] * ya0[0]) + (tpA[1] * ya0[1]) + (tpA[2] * ya0[2]);
        gravity[0] = (float) ya0[3];

        // LowPass 0.5Hz for alpha1
        xa1[0] = xa1[1];
        xa1[1] = xa1[2];
        xa1[2] = xa1[3];
        xa1[3] = accel[1] / ugainA;
        ya1[0] = ya1[1];
        ya1[1] = ya1[2];
        ya1[2] = ya1[3];
        ya1[3] = (xa1[0] + xa1[3]) + 3 * (xa1[1] + xa1[2]) + (tpA[0] * ya1[0]) + (tpA[1] * ya1[1]) + (tpA[2] * ya1[2]);
        gravity[1] = (float) ya1[3];

        // LowPass 0.5Hz for alpha2
        xa2[0] = xa2[1];
        xa2[1] = xa2[2];
        xa2[2] = xa2[3];
        xa2[3] = accel[2] / ugainA;
        ya2[0] = ya2[1];
        ya2[1] = ya2[2];
        ya2[2] = ya2[3];
        ya2[3] = (xa2[0] + xa2[3]) + 3 * (xa2[1] + xa2[2]) + (tpA[0] * ya2[0]) + (tpA[1] * ya2[1]) + (tpA[2] * ya2[2]);
        gravity[2] = (float) ya2[3];


    }

    public  void imbaLinear(float[] accel) {
        linear[0] = accel[0] - gravity[0];
        linear[1] = accel[1] - gravity[1];
        linear[2] = accel[2] - gravity[2];

        if(pcaStart == 0){
            pcaStart = System.currentTimeMillis();
        }
        //getScreenOrientationFromRollPitch();
        applyPCA();

    }

    public void applyPCA(){
        int axis = getVerticalAxis();

        SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magn);

        float orientation[] = new float[3];
        float R_remapped[] = new float[16];
        Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
        if(axis == 2){
            SensorManager.remapCoordinateSystem(RMatrix, SensorManager.AXIS_X,SensorManager.AXIS_Y,R_remapped);
        }else if(axis ==1){
            SensorManager.remapCoordinateSystem(RMatrix, SensorManager.AXIS_X,SensorManager.AXIS_MINUS_Z,R_remapped);
        }else if(axis == 0){
            SensorManager.remapCoordinateSystem(RMatrix,SensorManager.AXIS_MINUS_Z,SensorManager.AXIS_Y,R_remapped);
        }
        SensorManager.getOrientation(R_remapped, orientation);
        float mapped[] = new float[4];
        Matrix.multiplyMV(mapped, 0, RMatrixTranspose, 0, linear, 0);
        getScreenOrientationFromRollPitch(orientation);
        getQuanterinionAndApplyRotation(mapped, axis, orientation);
    }

    public int getVerticalAxis(){

        return axis;
    }

    public void getScreenOrientationFromRollPitch(float orientation[]){
        int axis = getVerticalAxis();
        //0 x axis, 1 yaxis,2 zaxis along world gravity line




        if (inclination < 25 || inclination > 155)
        {
            // device is flat don't calculate screen orientation from pitch and roll
            screenOrientation = ORIENTATION_PORTRAIT;
            adjustScreenOrientation();
        }else{

            averagePitch = addValue(orientation[1], pitches);
            averageRoll = addValue(orientation[2], rolls);
            screenOrientation = calculateOrientation();
            adjustScreenOrientation();
        }
    }

    public  void calculateAzimuth() {

        int axis = getVerticalAxis();
        int x_world = axisX,y_world = axisY;
        if(axis == 2){
            x_world = SensorManager.AXIS_X;
            y_world = SensorManager.AXIS_Y;
        }else if(axis ==1){
            x_world = SensorManager.AXIS_X;
            y_world = SensorManager.AXIS_MINUS_Z;
        }else if(axis == 0){
            x_world = SensorManager.AXIS_MINUS_Z;
            y_world = SensorManager.AXIS_Y;
        }




        magn[0] = (magn[0] - hardIronOffsetX)*softIronScaleX;
        magn[1] = (magn[1] - hardIronOffsetY)*softIronScaleY;
        magn[2] = (magn[2] - hardIronOffsetZ)*softIronScaleZ;



        SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magn);
        Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
        Matrix.multiplyMV(linearRemapped, 0, RMatrixTranspose, 0, linear, 0);
        //SensorManager.remapCoordinateSystem(RMatrix, x_world, y_world, RMatrixRemapped);




//        Matrix.multiplyMV(magn_remapped, 0, RMatrixTranspose,0,magn,0);
//        float A_D[] = magn.clone();
//
//
//
//
//        stepUpdateListener.onMagneticValueUpdate(magn_remapped.clone(), isAccurate, A_D);

        if (gyroExists) {

            float orientationValues[] = mOrientationProvider.getAzimuth(decl, x_world, y_world);
            azimuth = orientationValues[0];

        }
            SensorManager.getOrientation(RMatrixRemapped, orientation);
            if (orientation[0] >= 0) {
                // Azimuth-Calculation (rad in degree)
                magazimuth = (orientation[0] * 57.29577951f + decl);
            } else {
                // Azimuth-Calculation (rad in degree) +360
                magazimuth = (orientation[0] * 57.29577951f + 360 + decl);
            }

            if (magazimuth >= 360) {
                magazimuth -= 360;
            }


        long elapsedtime = System.currentTimeMillis() - pcaStart;
        if(isPositionSet && elapsedtime >= pcaWindow){
            calculatePCAAcceleration();
            pcaStart = System.currentTimeMillis();
        }

    }

//    private void calculateLinearRemapped() {
//        //float matrix[] = mOrientationProvider.getRotationMatrixArray();
//        SensorManager.getRotationMatrix(RMatrix, iMatrix, gravity, magn);
//        Matrix.transposeM(RMatrixTranspose, 0, RMatrix, 0);
//        Matrix.multiplyMV(linearRemapped, 0, RMatrixTranspose, 0, linear, 0);
//
//
//    }

    private void getQuanterinionAndApplyRotation(float accel[], int axis, float orientation[]){



        if(accelSampleQueue.size() >= ACCEL_SAMPLE_SIZE){
            accelSampleQueue.poll();
        }
        if(axis == 2){
            accelSampleQueue.add(new AccelSamples(-1.0*accel[0], -1.0*accel[1]));
        }else if(axis ==1){
            accelSampleQueue.add(new AccelSamples(-1.0*accel[0], accel[2]));
        }else{
            accelSampleQueue.add(new AccelSamples(-1.0*accel[2],-1.0*accel[1]));
        }

        userLinearAccel[2] = azimuth*Math.PI/180;


    }

//    private void getRotationFlat() {
//        if(0<=surfaceRotation && surfaceRotation<90){
//            screenOrientation = ORIENTATION_PORTRAIT;
//        }else if(90<=surfaceRotation && surfaceRotation<180){
//            screenOrientation = ORIENTATION_LANDSCAPE_REVERSE;
//        }else if(180<=surfaceRotation && surfaceRotation<270){
//            screenOrientation = ORIENTATION_PORTRAIT_REVERSE;
//        }else {
//            screenOrientation = ORIENTATION_LANDSCAPE;
//        }
//    }


    public  void stepDetection() {
        float value = linearRemapped[2];
        ArrayList<Float> currentAccel = new ArrayList<>();;
        if(timeForVelocity != 0) {
            long time = System.currentTimeMillis() - timeForVelocity;
            verticalAccValues.add(value);

            currentAccel.add((linearRemapped[0]*time)/1000);
            currentAccel.add((linearRemapped[1]*time)/1000);
            currentAccel.add((linearRemapped[2]*time)/1000);
            forVelocity.add(currentAccel);
        }

        boolean intimate = false;
        if (initialStep && value >= stepThreshold) {
            // Introduction of a step
            initialStep = false;
            stepBegin = true;
        } else if (!stepBegin) {
            if (oldAzimuth - azimuth > 5 || oldAzimuth - azimuth < -5) {
                //invoke step (only interface, not a real step), because orientation of user has changed more than X degree
                //so a step is necessary to update users position marker and respective orientation
                //at this position in code it means: no step is being awaited and therefore check orientation change
                //sendOrientationChange();
                intimate = true;
            }
        }
        if (stepBegin && iStep / frequency >= 0.24f && iStep / frequency <= 0.8f) {
            // Timeframe for step between minTime and maxTime
            // Check for negative peak
            if (value < -stepThreshold) {
                // TimeFrame correct AND Threshold of reverse side reached
//
                //distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz,2));
                stepCounter++;
                stepBegin = false;
                iStep = 1;
                initialStep = true;
                newStepDetected = true;
                if(timeForVelocity != 0) {

                    verticalAccValues.remove(verticalAccValues.size() - 1);
                    forVelocity.remove(forVelocity.size()-1);

                    calculateStrideLength();

                    verticalAccValues.clear();
                    forVelocity.clear();
                    verticalAccValues.add(value);
                    forVelocity.add(currentAccel);
                }

                newStep();

                //save old azimith for possibly necessary orientation change, in case no steps are detected and users orientation changes strong enough
                oldAzimuth = azimuth;

            } else {
                // TimeFrame correct but negative Threshold is too low
                iStep++;
                //sendOrientationChange();
            }
        } else if (stepBegin && iStep / frequency < 0.24f) {
            // TimeFrame for step too small, so wait and iStep++
            iStep++;
            //sendOrientationChange();
        } else if (stepBegin && iStep / frequency > 0.8f) {
            // TimeFrame for step too long
            stepBegin = false;
            initialStep = true;
            iStep = 1;
            //sendOrientationChange();
        }
        if(!newStepDetected && intimate){

            sendOrientationChange();
        }
        timeForVelocity = System.currentTimeMillis();
    }

    private void calculateStrideLength(){
        //k=0.36
        //double K = calculateKFromVelocity();
        double minimum = Collections.min(verticalAccValues);
        double maximum = Collections.max(verticalAccValues);
        strideLength   = Math.pow(Math.abs(maximum - minimum),0.25)*0.36;

    }

    private double calculateKFromVelocity(){
        double meanX=0,meanY=0,meanZ = 0;
        for(int j=0; j<forVelocity.size(); j++){
            meanX += forVelocity.get(j).get(0)/forVelocity.size();
            meanY += forVelocity.get(j).get(1)/forVelocity.size();
            meanZ += forVelocity.get(j).get(2)/forVelocity.size();
        }
        double vel = Math.sqrt(Math.pow(meanX, 2) + Math.pow(meanY, 2) + Math.pow(meanZ, 2));

        double K = 0.68 - 0.37 * vel + 0.15 * Math.pow(vel, 2);
        return K;
    }

    private void sendOrientationChange(){
        stepUpdateListener.onStepUpdate(0,azimuth, userOrientation, RMatrixRemapped, gravity, direction, 0);
        oldAzimuth = azimuth;
    }

    private  void newStep() {
        double winkel = azimuth;
        double winkel2 = winkel * 0.01745329252;

        deltaLat = Math.cos(winkel2) * 0.000008984725966 * stepLength;
        // 100cm for a step will be calculated according to angle on lat
        deltaLon = Math.sin(winkel2) / (distanceLongitude * 1000) * stepLength;
        // 100cm for a step will be calculated according to angle on lon

        deltaLat = Math.abs(deltaLat);
        deltaLon = Math.abs(deltaLon);
        // made by Christian Henke
        if (startLat > 0) {
            // User is on northern hemisphere, Latitude bigger than 0
            if (winkel > 270 || winkel < 90) { // Movement towards north
                startLat += deltaLat;
            } else {
                // Movement towards south
                startLat -= deltaLat;
            }
        } else if (startLat < 0) {
            // User is on southern hemisphere, Latitude smaller than 0
            if (winkel > 270 || winkel < 90) {
                // Movement towards north
                startLat += deltaLat;
            } else {
                // Movement towards south
                startLat -= deltaLat;
            }
        }
        if (winkel < 180) {
            // Movement towards east
            startLon += deltaLon;
        } else {
            // Movement towards west
            startLon -= deltaLon;
        }



        stepUpdateListener.onStepUpdate(1,azimuth, userOrientation, RMatrixRemapped, gravity, direction, strideLength);
        newStepDetected = false;
    }

    public  void changeDelay(int freq, int sensor) {
        // LowPassFilter 3. Order - Corner frequency all at 0.3 Hz

        //Initializing on 50Hz
        float ugain = 154994.3249f;
        float tp0 = 0.9273699683f;
        float tp1 = -2.8520278186f;
        float tp2 = 2.9246062355f;

        // Values according to actual frequency
        if (freq >= 125) {    //130
            ugain = 2662508.633f;
            tp0 = 0.9714168814f;
            tp1 = -2.9424208232f;
            tp2 = 2.9710009372f;
        } else if (freq <= 124 && freq >= 115) { //120
            ugain = 2096647.970f;
            tp0 = 0.9690721133f;
            tp1 = -2.9376603253f;
            tp2 = 2.9685843964f;
        } else if (freq <= 114 && freq >= 105) { //110
            ugain = 1617241.715f;
            tp0 = 0.9663083052f;
            tp1 = -2.9320417512f;
            tp2 = 2.9657284993f;
        } else if (freq <= 104 && freq >= 95) { //100
            ugain = 1217122.860f;
            tp0 = 0.9630021159f;
            tp1 = -2.9253101348f;
            tp2 = 2.9623014461f;
        } else if (freq <= 94 && freq >= 85) { //90
            ugain = 889124.3983f;
            tp0 = 0.9589765397f;
            tp1 = -2.9170984005f;
            tp2 = 2.9581128632f;
        } else if (freq <= 84 && freq >= 75) { //80
            ugain = 626079.3215f;
            tp0 = 0.9539681632f;
            tp1 = -2.9068581408f;
            tp2 = 2.9528771997f;
        } else if (freq <= 74 && freq >= 65) { //70
            ugain = 420820.6222f;
            tp0 = 0.9475671238f;
            tp1 = -2.8937318862f;
            tp2 = 2.9461457520f;
        } else if (freq <= 64 && freq >= 55) { //60
            ugain = 266181.2926f;
            tp0 = 0.9390989403f;
            tp1 = -2.8762997235f;
            tp2 = 2.9371707284f;
        } else if (freq <= 54 && freq >= 45) {  //50
            ugain = 154994.3249f;
            tp0 = 0.9273699683f;
            tp1 = -2.8520278186f;
            tp2 = 2.9246062355f;
        } else if (freq <= 44 && freq >= 35) { //40
            ugain = 80092.71123f;
            tp0 = 0.9100493001f;
            tp1 = -2.8159101079f;
            tp2 = 2.9057609235f;
        } else if (freq <= 34 && freq >= 28) { //30
            ugain = 34309.44333f;
            tp0 = 0.8818931306f;
            tp1 = -2.7564831952f;
            tp2 = 2.8743568927f;
        } else if (freq <= 27 && freq >= 23) { //25
            ugain = 20097.49869f;
            tp0 = 0.8599919781f;
            tp1 = -2.7096291328f;
            tp2 = 2.8492390952f;
        } else if (freq <= 22 && freq >= 15) { //20
            ugain = 10477.51171f;
            tp0 = 0.8281462754f;
            tp1 = -2.6404834928f;
            tp2 = 2.8115736773f;
        } else if (freq <= 14) { //10
            ugain = 1429.899908f;
            tp0 = 0.6855359773f;
            tp1 = -2.3146825811f;
            tp2 = 2.6235518066f;
        }

        // Set values for specific sensor
        if (sensor == 0) {
            //  Accelerometer
            frequency = freq;
            ugainA = ugain;
            tpA[0] = tp0;
            tpA[1] = tp1;
            tpA[2] = tp2;
        } else if (sensor == 1) {
            // Magnetic Field
            // here not: frequency = freq; otherwise value is wrong for step detection
            //that value has to be specified by accelerometer
            ugainM = ugain;
            tpM[0] = tp0;
            tpM[1] = tp1;
            tpM[2] = tp2;
        }
    }

    public  void shutdown(Context mContext) {
        pauseSensors();

        try {
            //Show new files with MTP for Windows immediatly
            mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(sensorFile)));
        } catch (Exception e) {
            // is always the case
        }
        try {
            //Show new files with MTP for Windows immediatly
            mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(posFile)));
        } catch (Exception e) {
            // is always the case
        }

    }

    @Override
    public  void onSensorChanged(SensorEvent event) {

//        if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
//            return;
//        }
        switch (event.sensor.getType()) {



            case Sensor.TYPE_MAGNETIC_FIELD:

                float[] values = event.values.clone();



                imbaMagnetic(values);

                magnUnits++;
                break;

            case Sensor.TYPE_ACCELEROMETER:

                    long currTime = System.nanoTime();
                    long time = System.nanoTime() - startTime;
                    timePassed = currTime - lasttime;
                    aclUnits++;
                    units++;
                    //shake detection working code ignore these values when shake detected
//                    if(timePassed > 100 * 1000000){
//                        lasttime = currTime;
//                        float[] values_new = event.values.clone();
//                        float x = values_new[0];
//                        float y = values_new[1];
//                        float z = values_new[2];
//                        timePassed /= 1000000;
//                        float speed = Math.abs(x+y+z - last_x - last_y - last_z) / timePassed * 10000;
//
//                        if (speed > SHAKE_THRESHOLD) {
//                            Log.d("sensor", "shake detected w/ speed: " + speed);
//                            Toast.makeText((Context) stepUpdateListener, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
//                        }
//                        last_x = x;
//                        last_y = y;
//                        last_z = z;
//                    }

                    if (time >= 2000000000) { // every 2sek
                        changeDelay(aclUnits / 2, 0);
                        changeDelay(magnUnits / 2, 1);

                        startTime = System.nanoTime();
                        aclUnits = magnUnits = 0;
                    }
                    calculateVerticalAxis(event.values.clone());
                    imbaGravity(event.values.clone());
                    calculateInclination(event.values.clone());
                    imbaLinear(event.values.clone());

                    //calculateScreenOrientation();

                    if (isPositionSet) {

                        calculateAzimuth();
                        stepDetection();

                    }




                break;

        }
    }

    private void calculateVerticalAxis(float[] clone) {
        double x = Math.abs(1.0/clone[0]);
        double y = Math.abs(1.0/clone[1]);
        double z = Math.abs(1.0/clone[2]);

        if(x <= z){
            if(x <= y){
                axis = 0;
            }else{
                axis = 1;
            }
        }else if(x <= y){
            if(x <= z){
                axis = 0;
            }else{
                axis = 2;
            }
        }else{
            if(y <= z){
                axis = 1;
            }else {
                axis = 2;
            }
        }
    }

    private void  calculatePCAAcceleration() {
        if(accelSampleQueue.size() == 0 ){
            return;
        }

        int x_neg=0,y_neg=0;
        int axis = getVerticalAxis();
        if(axis == 2 || axis == 1) {
            double accelvalues[][] = new double[accelSampleQueue.size()][2];
            JSONArray accel = new JSONArray();
            ArrayDeque<AccelSamples> clone = accelSampleQueue.clone();
            int i = 0;
            while (clone.peek() != null) {
                AccelSamples headSample = clone.poll();
                JSONArray inside = new JSONArray();
                accelvalues[i][0] = headSample.getX();
                accelvalues[i][1] = headSample.getY();

                    if (accelvalues[i][0] < 0) {
                        x_neg += 1;
                    }
                    if (accelvalues[i][1] < 0) {
                        y_neg += 1;
                    }

                try {
                    inside.put(headSample.getX());
                    inside.put(headSample.getY());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                accel.put(inside);
                i++;
            }
            String direc = "";
            direction = "";
            if(y_neg < accelSampleQueue.size()/2){
                direc = "1";
            }else{
                direc = "0";
            }
            if(x_neg < accelSampleQueue.size()/2){
                direc += "1";
            }else{
                direc += "0";
            }
            direction = direc;

            JSONObject obj = new JSONObject();
            try {
                obj.put("accelValues", accel);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            pcaMatrix = MatrixUtils.createRealMatrix(accelvalues);
            PCA pca = new PCA(pcaMatrix);
            RealMatrix principalComponents = pca.getPrincipalComponents();


            double[] firstComponent = principalComponents.getColumn(0);
            double tan1 = Math.atan2(firstComponent[0], firstComponent[1]) * 180 / Math.PI;

            //double tan2 = Math.toDegrees(Math.atan2(firstComponent[1], firstComponent[0]));
            userOrientation = (tan1 + 360) % 360;
            try {
                obj.put("principal_component_x", firstComponent[0]);
                obj.put("principal_component_y", firstComponent[1]);
                obj.put("azimuth", azimuth);
            } catch (JSONException e) {

            }
           double threshold = (userLinearAccel[2] * 180 / Math.PI);
            if (threshold < 0) {
                threshold += 360;
            }
//            if (screenOrientation == ORIENTATION_PORTRAIT_REVERSE || screenOrientation == ORIENTATION_LANDSCAPE_REVERSE) {
//                threshold += 180;
//            }

//            double threshold = 0;
            if(direc.equals("00")){
                if(135<=userOrientation && userOrientation<=315){
                    userOrientation += 180;
                }
            }else if(direc.equals("10")){
                if(225<=userOrientation || userOrientation<=45){
                    userOrientation += 180;
                }
            }else if(direc.equals("11")){
                if(315<=userOrientation || userOrientation<=135){
                    userOrientation += 180;
                }
            }else if (direc.equals("01")){
                if(45<=userOrientation &&userOrientation<=225){
                    userOrientation += 180;
                }
            }
            userOrientation %= 360;

//            int valueThreshold = accelvalues.length/10;
//            //double radiansdifference = Math.toRadians(userOrientation) - threshold - Math.toRadians(Math.abs(userOrientation+180-threshold));
//            if (Math.abs(userOrientation - threshold) > 90 && Math.abs(userOrientation - threshold) < 270) {
//                double value = 180 + userOrientation;
//                value += 360;
//                value %= 360;
//                userOrientation = value;
//            }
//            if(Math.abs(userOrientation - threshold) > 90){
//                userOrientation += 180;
//                userOrientation %= 360;
//            }
        }else {
            double threshold = (userLinearAccel[2] * 180 / Math.PI);
            if (threshold < 0) {
                threshold += 360;
            }
            userOrientation = threshold;
        }


//        if(isLogging) {
//            data.put(obj);
//        }


    }

    public  void arrayCopy(double[][] aSource, double[][] aDestination,int startIndex, int endIndex) {
        int k=0;
        for (int i = startIndex; i < endIndex; i++) {
            System.arraycopy(aSource[i], 0, aDestination[k], 0, aSource[i].length);
            k++;
        }
    }

    public boolean isIncreasing(double arr1[][], double arr2[][]){
        double arr1Target[][] = new double[arr1.length][arr1[0].length];
        double arr2Target[][] = new double[arr2.length][arr2[0].length];
        normaLizeArray(arr1, arr1Target);
        normaLizeArray(arr2, arr2Target);

        int minLength = Math.min(arr1Target.length, arr2Target.length);
        assert arr1Target[0].length == arr2Target[0].length;
        double val = 0;
        double normalizationFactor = 0;
        for(int i=0;i<minLength;i++){
            normalizationFactor = 0;
            double vectorVal= 0;
            for(int j=0;j<arr1Target[0].length;j++){
                vectorVal += arr2Target[i][j] - arr1Target[i][j];
                normalizationFactor += Math.pow(val, 2);
            }
            val += vectorVal/normalizationFactor;
        }
        if(val > 0){
            return true;
        }
        return false;
    }

    public void normaLizeArray(double [][] source, double [][]target){

        double[] normalizationFactor = new double[source.length];

        for(int i=0;i<source.length;i++){
            for(int k=0;k<source[0].length;k++){
                normalizationFactor[i] += Math.pow(source[i][k], 2);
            }
        }


        for (int i=0;i<source.length;i++){
            for(int j=0;j<source[i].length;j++){
                target[i][j] = source[i][j]/Math.sqrt(normalizationFactor[i]);
            }
        }


    }

    //private void convertToUserFrame() {

//        float orientationValues[] = mOrientationProvider.getAzimuth(decl, axisX, axisY);
//
////        double gamma = Math.toRadians(orientationValues[0]);
////        double alpha = Math.toRadians(orientationValues[1]);
////        double beta = Math.toRadians(orientationValues[2]);
//        double gamma = orientation[0];
//        double alpha = orientation[1];
//        double beta  = orientation[2];
//
//        double q0 = (Math.cos(alpha/2.0)*Math.cos(beta/2.0)*Math.cos(gamma/2.0)) + (Math.sin(alpha/2.0)*Math.sin(beta/2.0)*Math.sin(gamma/2.0));
//        double q1 = (Math.cos(alpha/2.0)*Math.sin(beta/2.0)*Math.cos(gamma/2.0)) - (Math.sin(alpha/2.0)*Math.cos(beta/2.0)*Math.sin(gamma/2.0)) ;
//        double q2 = (Math.sin(alpha/2.0)*Math.cos(beta/2.0)*Math.cos(gamma/2.0)) + (Math.cos(alpha/2.0)*Math.sin(beta/2.0)*Math.sin(gamma/2.0));
//        double q3 = (Math.cos(alpha/2.0)*Math.cos(beta/2.0)*Math.sin(gamma/2.0)) - (Math.sin(alpha/2.0)*Math.sin(beta/2.0)*Math.cos(gamma/2.0));
//        //double norm = Math.pow(q0,2) + Math.pow(q1, 2) + Math.pow(q2, 2) + Math.pow(q3, 2);
//        Rotation phoneToUser = new Rotation(q0, q1, q2, q3, false);
//        phoneToUser.applyTo(phoneLinearAccel,userLinearAccel);
        //accelQueue.add(new double[]{linearRemapped[0], linearRemapped[1]});

//        pcaMatrix.addColumnVector(colMatrix);

    //}

//    private void calculateDeviceRotation(long l, float[] clone) {
//        if(clone[2] > 0.5f) { // anticlockwise
//            azimuthdevice = 1;
//        } else if(clone[2] < -0.5f) { // clockwise
//            azimuthdevice = 1;
//        }else{
//            azimuthdevice = 0;
//        }
//    }

    private void adjustScreenOrientation() {


        if(screenOrientation != ORIENTATION_UNKNOWN){
            //pitch = orientation;
            if (screenOrientation == ORIENTATION_PORTRAIT){
                axisX = SensorManager.AXIS_X;
                axisY = SensorManager.AXIS_Y;
            }else if (screenOrientation == ORIENTATION_LANDSCAPE){
                axisX = SensorManager.AXIS_MINUS_Y;
                axisY = SensorManager.AXIS_X;
            }else if (screenOrientation == ORIENTATION_PORTRAIT_REVERSE){
                axisX = SensorManager.AXIS_MINUS_X;
                axisY = SensorManager.AXIS_MINUS_Y;
            }else if (screenOrientation == ORIENTATION_LANDSCAPE_REVERSE){
                axisX = SensorManager.AXIS_Y;
                axisY = SensorManager.AXIS_MINUS_X;
            }
        }


    }

    @Override
    public  void onAccuracyChanged(Sensor sensor, int accuracy) {
        if(sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
            if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_HIGH){
                isAccurate = true;
            }else if(accuracy == SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM){
                isAccurate = true;
            }else {
                isAccurate = false;
            }
        }
    }

    @Override
    public void onFlushCompleted(Sensor sensor) {

    }

    public interface onStepUpdateListener {
        void onStepUpdate(int event, double headingDegrees, double turn, float[] rotationMatrix, float[] linearAcceleration, String orientation, double strideLength);
        void onMagneticValueUpdate(float[] magValues, boolean accuracy, float[] deviceMag);
    }

    private int calculateOrientation() {
        // finding local orientation dip
        if (((screenOrientation == ORIENTATION_PORTRAIT || screenOrientation == ORIENTATION_PORTRAIT_REVERSE)
                && (averageRoll > -30 && averageRoll < 30))) {
            if (averagePitch > 0)
                return ORIENTATION_PORTRAIT_REVERSE;
            else
                return ORIENTATION_PORTRAIT;
        } else {
            // divides between all orientations
            if (Math.abs(averagePitch) >= 30) {
                if (averagePitch > 0)
                    return ORIENTATION_PORTRAIT_REVERSE;
                else
                    return ORIENTATION_PORTRAIT;
            } else {
                if (averageRoll > 0) {
                    return ORIENTATION_LANDSCAPE_REVERSE;
                } else {
                    return ORIENTATION_LANDSCAPE;
                }
            }
        }
    }

    private float addValue(float value, float[] values) {
        value = 57.29577951f * value;
        //value = (float) Math.round((Math.toDegrees(value)));
        float average = 0;
        for (int i = 1; i < smoothness; i++) {
            values[i - 1] = values[i];
            average += values[i];
        }
        values[smoothness - 1] = value;
        average = (average + value) / smoothness;
        return average;
    }

    private void calculateInclination(float values[]){
        float x = values[0];
        float y = values[1];
        float z = values[2];
        float norm_Of_g =(float) Math.sqrt(x * x + y * y + z * z);

        // Normalize the accelerometer vector
        x = (x / norm_Of_g);
        y = (y / norm_Of_g);
        z = (z / norm_Of_g);
        int axis = getVerticalAxis();
        if(axis == 2){
            inclination = (int) Math.round(Math.toDegrees(Math.acos(z)));
        }else if(axis == 1){
            inclination = (int) Math.round(Math.toDegrees(Math.acos(y)));
        }else{
            inclination = (int) Math.round(Math.toDegrees(Math.acos(x)));
        }

    }


}
