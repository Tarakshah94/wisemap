package com.example.tarak.wisemap.managers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceManager {

    private Context mContext;

    public SharedPreferenceManager(Context context){
        this.mContext = context;
    }

    public void putBoolean(String key, boolean val){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, val);
        editor.commit();
    }

    public void putInt(String key, int val){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, val);
        editor.commit();
    }

    public void putString(String key, String val){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public void putFloat(String key, float val){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(key, val);
        editor.commit();
    }

    public void putLong(String key, long val){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, val);
        editor.commit();
    }

    public String getString(String key){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getString(key,"0");
    }

    public float getFloat(String key){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getFloat(key,0.0f);
    }

    public long getLong(String key){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getLong(key,0L);
    }

    public int getInt(String key){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getInt(key,0);
    }

    public boolean getBoolean(String key){
        if(mContext == null){
            throw new NullPointerException("Context passed to preference manager is null");
        }
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getBoolean(key,false);
    }
}
