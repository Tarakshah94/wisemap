package com.example.tarak.wisemap.managers;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prathyush on 02/12/18.
 */

public interface NeighboursUpdater {
    public void onNeighboursLocation(double location[]);
    public void particlesLocation(double[][] particles);
}
