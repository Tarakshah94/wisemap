package com.example.tarak.wisemap.utils;

import android.content.Context;
import android.content.pm.PackageManager;

public class HardwareUtility {

    public static boolean isGyroAvailable(Context context){
        PackageManager packageManager = context.getPackageManager();
        boolean gyroExists = packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
        return gyroExists;
    }
}
