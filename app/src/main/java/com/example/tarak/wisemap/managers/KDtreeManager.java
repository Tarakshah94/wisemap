package com.example.tarak.wisemap.managers;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.example.tarak.wisemap.models.MagneticMapPojo;
import com.example.tarak.wisemap.models.MeasurementsPojo;
import com.example.tarak.wisemap.utils.FileUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prathyush on 02/12/18.
 */

public class KDtreeManager {

    static {
        System.loadLibrary("slamlib");
    }

    HandlerThread handlerThread;
    Handler messenger;
    private static final int INIT_KDTREE   = 0;
    private static final int SIMULATE_ROBOT_WORLD  = 1;
    private static final int SEND_INITIAL_LOCATION =  2;
    private ParticleManager tree;
    private NeighboursUpdater updater;
    private int dimensions = 3;
    //this for mumbai airport
    //double[][] geofence = {{1772612.5964885126-5, 1772947.8879604405+5}, {5753319.554939809-5, 5753471.8145920625+5},{2084466.295558238-5, 2084623.9072973288+5}};
    //this is for office ecostar
    double[][] geofence = {{1774184.2793695577-5, 1774196.7543790387+5}, {
            5750321.7180962-5, 5750331.520549943+5},{2091789.3776727167-5, 2091819.0163994082+5}};

    private int particleSize = 1000;

    private long pointerAddress;

    public KDtreeManager(Context context) {

        if (context instanceof NeighboursUpdater) {
            updater = (NeighboursUpdater) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Neigbours updater interface");
        }

        handlerThread = new HandlerThread("neighboursthread", Thread.MAX_PRIORITY);
        handlerThread.start();
        messenger = new Handler(handlerThread.getLooper(), callback);
        initLocProvider();



    }

    private void initLocProvider() {
        this.pointerAddress = nativeInitlocProvider();
    }

    public void invalidate(){
        if(handlerThread != null && handlerThread.isAlive()){
            handlerThread.quit();
        }
        if(tree != null){
            tree = null;
        }
        deleteNativeReference(this.pointerAddress);

    }

    private Handler.Callback callback = new Handler.Callback(){
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case INIT_KDTREE:
                    initKdTree();
                    break;
                case SIMULATE_ROBOT_WORLD:
                    simulateRobotWorld(msg.obj);
                    break;
                case SEND_INITIAL_LOCATION:
                    sendMeanLocation();
                    break;

            }
            return false;
        }
    };

    private void simulateRobotWorld(Object obj) {
        try {
            JSONObject message_obj = new JSONObject(obj.toString());
            double distance        = Double.parseDouble(message_obj.get("distance").toString());
            double turn            = Double.parseDouble(message_obj.get("turn").toString());

            JSONArray magneticObs          = new JSONArray(message_obj.get("magnetic").toString());
            JSONArray wifiObs              = new JSONArray(message_obj.get("wifi").toString());
            JSONArray beaconObs            = new JSONArray(message_obj.get("beacon").toString());
            boolean isWifi                 = Boolean.parseBoolean(message_obj.get("isWifi").toString());
            boolean isBeacon               = Boolean.parseBoolean(message_obj.get("isBeacon").toString());
            int size                       = message_obj.getInt("beaconsize");

            double magneticobservations[]  = new double[magneticObs.length()];
            for(int k=0;k<magneticObs.length();k++){
                magneticobservations[k] = Double.parseDouble(magneticObs.get(k).toString());
            }

            double wifiobservations[]  = new double[wifiObs.length()];
            for(int k=0;k<wifiObs.length();k++){
                wifiobservations[k] = Double.parseDouble(wifiObs.get(k).toString());
            }

            double bluetoothobservations[]  = new double[beaconObs.length()];
            for(int k=0;k<beaconObs.length();k++){
                bluetoothobservations[k]    = Double.parseDouble(beaconObs.get(k).toString());
            }
            List<JSONArray> observationLoc = tree.getNearestNeigboursLocation(bluetoothobservations);

            double locationObserved[] = new double[3];
            if(isBeacon && observationLoc.size()>0){
                for(int k=0;k<observationLoc.get(0).length();k++) {
                    locationObserved[k] = observationLoc.get(0).getDouble(k);
                }
            }
            if(observationLoc.size() < 1){
                isBeacon = false;
            }
//            double returnedloaction[]   = tree.move(distance, turn, bluetoothobservations);
//            this.updater.onNeighboursLocation(returnedloaction);
//            double nearestLocation[]    = tree.getParticlesLocation();
//
//                this.updater.particlesLocation(new double[][]{nearestLocation, new double[3]});

            double returnedlocation[] = nativeMoveSlaves(distance, turn, magneticobservations, this.pointerAddress, magneticobservations.length, locationObserved, isBeacon, wifiobservations, isWifi, size);
            //double particlesLocation[][] = getParticlesLocations(this.pointerAddress);
            this.updater.onNeighboursLocation(returnedlocation);
//            particlesLocation[0] = locationObserved;
            //this.updater.particlesLocation(particlesLocation);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void initKdTree(){
        Context context = (Context) this.updater;
        if(context != null) {
            try {
                JSONArray data = new JSONArray(FileUtils.loadJSONFromAsset(context, "features_new.json"));
                tree = new ParticleManager(dimensions, data,particleSize,geofence);
                JSONArray magneticMapData = new JSONArray(FileUtils.loadJSONFromAsset(context, "magnetic_map.json"));
                Gson gson = new Gson();
                Type type = new TypeToken<List<MeasurementsPojo>>(){}.getType();
                ArrayList<MeasurementsPojo> measurementData = gson.fromJson(data.toString(), type);

                Type magType = new TypeToken<List<MagneticMapPojo>>(){}.getType();
                ArrayList<MagneticMapPojo> magMap = gson.fromJson(magneticMapData.toString(), magType);

                initNativeLocalizerAndMapper(this.pointerAddress,dimensions, measurementData, particleSize, geofence, magMap);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }




    public void passInitMessage(){
        Message msg = new Message();
        msg.what    = INIT_KDTREE;
        messenger.sendEmptyMessage(msg.what);
    }

    public void passMeanLocationMessage() {
        Message message = new Message();
        message.what    = SEND_INITIAL_LOCATION;
        messenger.sendEmptyMessage(message.what);
    }

    public void sendMeanLocation(){
        double x_mean = (geofence[0][1] + geofence[0][0]) /2;
        double y_mean = (geofence[1][1] + geofence[1][0]) /2;
        double z_mean = (geofence[2][1] + geofence[2][0]) /2;
        this.updater.onNeighboursLocation(new double[]{x_mean, y_mean, z_mean, 0.0});
        //double particlesLocation[][] = getParticlesLocations(this.pointerAddress);
        //this.updater.particlesLocation(particlesLocation);
//        double positions[][] = getParticlesLocations(this.pointerAddress);
////        System.out.print("hbchbchbs");
//        this.updater.particlesLocation(positions);
    }

    public void passSimulateMessage(double distance, double turn, double[] magObservations, double[] wifiObservations, double[] beaconObservations, boolean isWifi, boolean isBeacon, int size) {
        Message msg     = new Message();
        msg.what        = SIMULATE_ROBOT_WORLD;
        JSONObject obj  = new JSONObject();
        try {
            obj.put("distance", distance);
            obj.put("turn", turn);

            JSONArray magneticObs = new JSONArray();
            for(int i=0;i<magObservations.length;i++){
                magneticObs.put(magObservations[i]);
            }

            JSONArray wifiObs = new JSONArray();
            for(int i=0;i<wifiObservations.length;i++){
                wifiObs.put(wifiObservations[i]);
            }

            JSONArray beaconObs = new JSONArray();
            for(int i=0;i<beaconObservations.length;i++){
                beaconObs.put(beaconObservations[i]);
            }

            obj.put("magnetic", magneticObs);
            obj.put("wifi", wifiObs);
            obj.put("beacon", beaconObs);
            obj.put("isWifi", isWifi);
            obj.put("isBeacon", isBeacon);
            obj.put("beaconsize", size);

            msg.obj = obj;
            messenger.sendMessage(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private native long nativeInitlocProvider();

    private native void deleteNativeReference(long address);

    private native void initNativeLocalizerAndMapper(long address, int dimensions, ArrayList<MeasurementsPojo> measurements,int particleSize, double[][] geofence,ArrayList<MagneticMapPojo> magMap);

    private native double[] nativeMoveSlaves(double distance, double turn, double[] magobservations, long address, int maglength, double []beaconsobservations, boolean isBeacon, double []wifiobservations, boolean isWifi, int size);

    private native double[][] getParticlesLocations(long address);

}
