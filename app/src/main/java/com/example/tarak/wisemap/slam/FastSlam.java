package com.example.tarak.wisemap.slam;

import android.os.Environment;

import com.example.tarak.wisemap.managers.LocationUpdater;
import com.example.tarak.wisemap.models.BeaconsData;
import com.example.tarak.wisemap.models.LandMark;
import com.example.tarak.wisemap.models.WifiDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FastSlam {
    static {
        System.loadLibrary("slamlib");
    }
    private long pointerObject = 0;

    private int mParticleSize;
    private int minObservations = 0;
    private int maxObservations = 2;
    private double[][] geofence;
    private LocationUpdater mUpdater;
    private String path;
    private boolean simulatorInitialized = false;
    public FastSlam(int size, double[][] geofence, LocationUpdater updater){

        this.mParticleSize  = size;
        this.geofence       = geofence;
        this.mUpdater       = updater;

        pointerObject = initSlamNative();
        File f = Environment.getExternalStorageDirectory();

        File file = new File(f, /*"ml_record" + System.currentTimeMillis() */System.currentTimeMillis() + ".json");
        path = file.getPath();
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        initFastSlam(geofence, mParticleSize, pointerObject,file.getPath());

    }



    public native long initSlamNative();

    public native void initFastSlam(double[][] geofence, int mParticleSize,long address,String savePath );

    public native void setObservations(int min, int max, long address);

    public native String setStartStopNative(boolean flag, long address);

    public native void initSimulatorNative(double x, double y, double z, double orientation, long address);

    public native void setRobotPositionNative(double x, double y, double z,  long address);

    public native void setLandMarksNative(ArrayList<LandMark> landmarks, long address);

    public native void moveRobotWorldNative(double distance, double turn, long address);

    public native void senseAndUpdateNative(long address);

    public native double[] getUserPositionNative(long address);

    public native double[] getRobotPositionNative(long address);

    public native void resampleParticlesNative(long address);

    public native void addNearestNeighboursNative(long address);

    public native void setSensorReadingsNative(ArrayList<BeaconsData> beaconsData,ArrayList<WifiDetails> wifiDetails,float[] magValues,float[] devicemagValues, long address, double waypoint[]);

    public native void invalidateNative(long address);

    public void setMinMaxObservations(int min, int max) {
        setObservations(min, max, pointerObject);
    }

    public void setStartStop(boolean obj) {
        String s = setStartStopNative(obj, pointerObject);
        if(!obj){
            //FileUtils.saveFile();
            mUpdater.showToast(s,path);
        }

    }

    public void initManager(double x, double y, double z, double orientation) {
        initSimulatorNative(x,y,z,orientation, pointerObject);
        simulatorInitialized = true;
    }

    public synchronized void simulateRobotWorld(double distance, double turn) {
        Long timeStart = System.currentTimeMillis();
        if(distance > 0){
            System.out.println("xxx");
        }
        moveRobotWorldNative(distance, turn, pointerObject);

        if(distance > 0) {
            System.out.println("--->" + "move");
            senseAndUpdateNative(pointerObject);
            if (mUpdater != null) {
                mUpdater.onParticleLocationsUpdated(getUserPositionNative(pointerObject), getRobotPositionNative(pointerObject));
            }
            resampleParticlesNative(pointerObject);
            addNearestNeighboursNative(pointerObject);
        }


    }



    public synchronized void setLandMarks(ArrayList<LandMark> landmarks) {
        //Log.d("debug","before landmark reading native");
        setLandMarksNative(landmarks, pointerObject);
        //Log.d("debug","after landmark reading native");
    }

    public synchronized void setSensorReadings(JSONObject obj) {
        try {
            JSONArray bdata             = new JSONArray(obj.get("bluetooth_data").toString());
            JSONArray wdata             = new JSONArray(obj.get("wifi_data").toString());
            Gson gson = new Gson();
            Type type = new TypeToken<List<BeaconsData>>(){}.getType();
            ArrayList<BeaconsData> bData = gson.fromJson(bdata.toString(), type);




            Type type2 = new TypeToken<List<WifiDetails>>(){}.getType();
            ArrayList<WifiDetails> wData = gson.fromJson(wdata.toString(), type2);



            JSONArray magvalues = obj.getJSONArray("mag_data");
            JSONArray devicemagvalues = obj.getJSONArray("device_mag_data");
            JSONArray waypointloc     = obj.getJSONArray("waypointloc");

            float[] magValues            = new float[3];
            magValues[0]                 = Float.parseFloat(magvalues.get(0).toString());
            magValues[1]                 = Float.parseFloat(magvalues.get(1).toString());
            magValues[2]                 = Float.parseFloat(magvalues.get(2).toString());


            float[] devicemagValues            = new float[3];
            devicemagValues[0]                 = Float.parseFloat(devicemagvalues.get(0).toString());
            devicemagValues[1]                 = Float.parseFloat(devicemagvalues.get(1).toString());
            devicemagValues[2]                 = Float.parseFloat(devicemagvalues.get(2).toString());

            double[] waypointLoc  = new double[3];
            waypointLoc[0] = Double.parseDouble(waypointloc.get(0).toString());
            waypointLoc[1] = Double.parseDouble(waypointloc.get(1).toString());
            waypointLoc[2] = Double.parseDouble(waypointloc.get(2).toString());

            //Log.d("debug","before sensor reading native");
            //Log.d("debug",String.valueOf(pointerObject));
            setSensorReadingsNative(bData,wData,magValues,devicemagValues,pointerObject,waypointLoc);
            //Log.d("debug","before sensor reading after native");
            //Log.d("debug",String.valueOf(pointerObject));
            System.out.print("xxxx");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void invalidate() {

        invalidateNative(pointerObject);
        pointerObject = 0;
        System.gc();
        System.runFinalization();
    }


    public void setRobotPosition(double x, double y, double z) {
        setRobotPositionNative(x,y,z, pointerObject);
        if (mUpdater != null && simulatorInitialized) {
            mUpdater.onParticleLocationsUpdated(getUserPositionNative(pointerObject), getRobotPositionNative(pointerObject));
        }
    }
}
