package com.example.tarak.wisemap.utils;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;

/**
 * Created by Furqan on 30-11-2017.
 */

public class FileUtils {
    public static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json;
        try {
            InputStream is = context.getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String loadJSONFromFile(Context context, String jsonFileName) {
        String json ="";
        try {
            FileInputStream fis = new FileInputStream(jsonFileName);
            DataInputStream din = new DataInputStream(fis);
            BufferedReader br = new BufferedReader(new InputStreamReader(din));
            String strLine;
            while ((strLine = br.readLine()) != null){
                json = json + strLine;
            }
            din.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static void saveFile(Context context, String array, String fileName) {
        try {
            Writer output = null;
            File f = Environment.getExternalStorageDirectory();

            File file = new File(f, /*"ml_record" + System.currentTimeMillis() */fileName + ".json");
            if (!file.exists()) {
                file.createNewFile();
            }
            output = new BufferedWriter(new FileWriter(file));
            output.write(array);
            output.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFileWithoutExtension(Context context, String array, String fileName) {
        try {
            Writer output = null;
            File f = Environment.getExternalStorageDirectory();

            File file = new File(fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            output = new BufferedWriter(new FileWriter(file));
            output.write(array);
            output.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
