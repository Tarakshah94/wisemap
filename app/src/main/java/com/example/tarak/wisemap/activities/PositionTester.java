package com.example.tarak.wisemap.activities;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tarak.wisemap.R;

import com.example.tarak.wisemap.hardwaresensors.StepAndAzimutDetector;
import com.example.tarak.wisemap.helpers.Kalman;
import com.example.tarak.wisemap.managers.KDtreeManager;
import com.example.tarak.wisemap.managers.NeighboursUpdater;
import com.example.tarak.wisemap.models.BeaconsData;
import com.example.tarak.wisemap.models.BeaconsDetail;
import com.example.tarak.wisemap.models.FilteredBeacon;
import com.example.tarak.wisemap.models.LandMark;
import com.example.tarak.wisemap.models.WifiDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kontakt.sdk.android.ble.configuration.ScanMode;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.light.Position;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import org.apache.commons.math3.linear.ArrayRealVector;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.asin;
import static java.lang.Math.atan2;

public class PositionTester extends AppCompatActivity implements OnMapReadyCallback, StepAndAzimutDetector.onStepUpdateListener,NeighboursUpdater{

    private MapView mapView;
    private MapboxMap mMap;
    private Icon userIcon;
    private Icon particleIcon;
    private Icon robotIcon;
    private ProximityManager proximityManager;
    private WifiManager wifiManager;
    private StepAndAzimutDetector sensorController;
    HashMap<String, BeaconsDetail> beaconsHashMap = new HashMap<>();
    private ArrayList<FilteredBeacon> beaconList = new ArrayList<>();

    private boolean initialPointSet = false;
    private float magObserved[] = new float[3];
    private float deviceMagObserved[] = new float[3];
    private ArrayList<WifiDetails> wifiResults = new ArrayList<>();
    private KDtreeManager neighboursManager;
    long currentTime = 0;

    private Marker mUserLocationMarker;
    private Marker mParticleLocationMarker;
    private Marker mRobotLocationMarker;
    private SymbolLayer layers[] = new SymbolLayer[1000];
    private double heading;
    private boolean initializationDone = false;
    private boolean notCalled = false;
    private TextView textView;
    private Marker[] particles = new Marker[200];
    private HashMap<String, Kalman> beaconsFilterHashMap = new HashMap<>();
    private Kalman[] magKalman = new Kalman[3];
    private Kalman kalMagnetic[] = new Kalman[3];
    boolean locationRequestDone = false;
    private boolean initialLocationDone = false;
    private int didStartWalking = 5;
    private ArrayList<double[]> magValues = new ArrayList<double[]>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_position_tester);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        textView = findViewById(R.id.textView3);


        sensorController = new StepAndAzimutDetector(this);
        wifiManager      = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        proximityManager = ProximityManagerFactory.create(this);
        proximityManager.configuration()
                .scanPeriod(ScanPeriod.RANGING)
                .scanMode(ScanMode.LOW_LATENCY)
                //OnDeviceUpdate callback will be received with 1 seconds interval
                .deviceUpdateCallbackInterval(100);

        proximityManager.setIBeaconListener(createIBeaconListener());

        sensorController.startSensors(false);
        neighboursManager = new KDtreeManager(this);
        neighboursManager.passInitMessage();

        IconFactory iconFactory = IconFactory.getInstance(this);
        userIcon = iconFactory.fromResource(R.mipmap.dot);
        particleIcon = iconFactory.fromResource(R.mipmap.dot);
        robotIcon = iconFactory.fromResource(R.mipmap.beacon1);


    }

    private double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = Math.toRadians(latitude);
        double lon = Math.toRadians(longitude);
        double x = earthRadius * Math.cos(lat) * Math.cos(lon);
        double y = earthRadius * Math.cos(lat) * Math.sin(lon);
        double z = earthRadius * Math.sin(lat);
        return new double[]{x, y, z};
    }


    private void startSense(@NonNull LatLng point) {
        try {
            SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD).getName();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //uid = "" + (1 + (int) (Math.random() * ((10000000 - 1) + 1)));
        String stepLengthString = "165";

        if (stepLengthString != null) {
            try {
                stepLengthString = stepLengthString.replace(",", ".");
                Float savedBodyHeight = (Float.parseFloat(stepLengthString));
                if (savedBodyHeight < 241 && savedBodyHeight > 119) {
                    StepAndAzimutDetector.stepLength = savedBodyHeight / 222;
                } else if (savedBodyHeight < 95 && savedBodyHeight > 45) {
                    StepAndAzimutDetector.stepLength = (float) (savedBodyHeight * 2.54 / 222);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }
        currentTime = System.currentTimeMillis();
        setInitialPosition(point.getLatitude(), point.getLongitude());
    }

    private void setInitialPosition(double startLat,
                                    double startLon) {
        double altitude = 0;
        double middleLat = startLat * 0.01745329252;
        double distanceLongitude = 111.3 * Math.cos(middleLat);

        StepAndAzimutDetector.initialize(startLat, startLon, distanceLongitude, altitude);
        StepAndAzimutDetector.setLocation(startLat, startLon);
        initialPointSet = true;
        StepAndAzimutDetector.isPositionSet = true;

    }



    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();


    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        if(sensorController != null){
            sensorController.pauseSensors();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        if(sensorController != null){
            sensorController.pauseSensors();
        }
        proximityManager.stopScanning();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if(sensorController != null){
            sensorController.shutdown(this);
        }
        if(neighboursManager != null){
            neighboursManager.invalidate();
        }
        proximityManager.disconnect();
        proximityManager = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    private Bitmap getMarkerIcon(Resources resources, int marker_shopping) {
        return BitmapFactory.decodeResource(resources, marker_shopping);
    }


    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMap = mapboxMap;
        Resources resources;
        try {
            resources = getResources();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }
        mMap.addImage("particle", getMarkerIcon(resources, R.mipmap.beacon1));
        mMap.addImage("magnetic", getMarkerIcon(resources, R.mipmap.beacon));
        //mMap.addOnMapLongClickListener(this);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                readBeaconFile();
                startScanning();
            }
        });

        if(!initialPointSet && !notCalled){
            getNeighbourLocation(0,0);
        }



    }

    private void readBeaconFile() {
        Gson gson = new Gson();
        Type type = new TypeToken<List<BeaconsDetail>>() {
        }.getType();
        //iconFactory = IconFactory.getInstance(this);
        List<BeaconsDetail> details = gson.fromJson(com.example.tarak.wisemap.utils.FileUtils.loadJSONFromAsset(this, "beacons.json"), type);
        for (BeaconsDetail detail : details) {
            if (detail.getEnabled()) {
              /*  Icon icon = iconFactory.fromResource(R.drawable.beacons_marker);
                beaconMarker = mapboxMap.addMarker(new MarkerViewOptions()
                        .position(new LatLng(detail.getLat(), detail.getLng())).icon(icon));*/
                beaconsHashMap.put(detail.getUniqueId(), detail);
            }

        }
    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });

    }

    private IBeaconListener createIBeaconListener() {
        return new IBeaconListener() {


            @Override
            public void onIBeaconDiscovered(IBeaconDevice iBeacon, IBeaconRegion region) {
                final String uniqueid = iBeacon.getUniqueId();
                if (uniqueid == null) {
                    return;
                }

                Kalman kalman = beaconsFilterHashMap.get(uniqueid);
                if (kalman == null) {
                    kalman = new Kalman(iBeacon.getDistance(), 1e-3d, 1.0d);
                    kalman.estimatedFilter(new ArrayRealVector(1, iBeacon.getDistance()));
                    beaconsFilterHashMap.put(uniqueid, kalman);
                }
            }

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> iBeacons, IBeaconRegion region) {

                ArrayList<FilteredBeacon> modifiedList = new ArrayList<>();
                //ArrayList<BeaconsDetail> observedBeacons = new ArrayList<>();
                for (IBeaconDevice iBeacon : iBeacons) {
                    FilteredBeacon filteredBeacon = new FilteredBeacon();
                    filteredBeacon.setRssi(iBeacon.getRssi());
                    filteredBeacon.setTxPower(iBeacon.getTxPower());
                    filteredBeacon.setDistance(iBeacon.getDistance());
                    filteredBeacon.setUniqueId(iBeacon.getUniqueId());
                    if (iBeacon.getUniqueId() != null && beaconsHashMap.get(iBeacon.getUniqueId()) != null)
                    {
                        modifiedList.add(filteredBeacon);
                        //observedBeacons.add(beaconsHashMap.get(iBeacon.getUniqueId()));
                    }
                }

                for (FilteredBeacon iBeaconDevice : new ArrayList<FilteredBeacon>(modifiedList)) {
                    String uniqueId = iBeaconDevice.getUniqueId();
                    Kalman kalman = beaconsFilterHashMap.get(uniqueId);
                    int index = modifiedList.indexOf(iBeaconDevice);
                    if (kalman != null) {
                        double distance = iBeaconDevice.getDistance();
                        double[] predictedDistance = kalman.estimatedFilter(new ArrayRealVector(1, distance));
                        iBeaconDevice.setKalmanDistance(predictedDistance[0]);
                        modifiedList.set(index, iBeaconDevice);
                    } else {
                        modifiedList.remove(index);
                    }
                }


                Collections.sort(modifiedList, new Comparator<FilteredBeacon>() {
                    @Override
                    public int compare(FilteredBeacon filteredBeacon, FilteredBeacon t1) {
                        return Double.compare(filteredBeacon.getKalmanDistance(), t1.getKalmanDistance());
                    }
                });
                beaconList = modifiedList;


            }

            @Override
            public void onIBeaconLost(IBeaconDevice iBeacon, IBeaconRegion region) {
                String uniqueId = iBeacon.getUniqueId();
                if (uniqueId == null) {
                    return;
                }
                beaconsFilterHashMap.remove(uniqueId);
            }
        };
    }

    @Override
    public void onStepUpdate(int event, double headingDegrees, double turn, float rotations[], float[] azimuthDevice, String screenOrientation, double stepLength) {


        String text = String.valueOf(headingDegrees) + "------->" + String.valueOf(turn) + "---->"+screenOrientation;
        textView.setText(text);
        initializationDone = true;
        if(event == 0){
            if(initializationDone){

                getNeighbourLocation(stepLength,turn);
                if(mRobotLocationMarker != null) {
                    //rotateMapbox(turn);

                }
            }

        }else{

            if(initializationDone){

                getNeighbourLocation(0.7432, turn);
                if(mRobotLocationMarker != null){
                    //updateMarkerPosition();
//                    if(Math.abs(turn - this.heading) > 5){
//                        rotateMapbox(turn);
//                    }
                    //rotateMapbox(turn);

                }
            }



        }
        this.heading = turn;
    }

    @Override
    public void onMagneticValueUpdate(float[] magval, boolean isAccurate, float[] deviceMagObserved) {

        double magnitude = Math.sqrt(Math.pow(deviceMagObserved[0],2) + Math.pow(deviceMagObserved[1],2) + Math.pow(deviceMagObserved[2],2));
        if(isAccurate) {
            this.magValues.add(new double[]{magval[0], magval[1], magval[2]});
       }
        if(!initialPointSet && !notCalled){
            magObserved = magval;
            //getNeighbourLocation(0,0);
        }else{
            if(!isAccurate){


            }
            magObserved = magval;
        }

        this.deviceMagObserved = deviceMagObserved;

//
    }

    private void getNeighbourLocation(double distance, double turn) {

        ArrayList<Double> rssiSignals = new ArrayList();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        ArrayList<FilteredBeacon>beacons = new ArrayList<>();
        beacons = (ArrayList<FilteredBeacon>) beaconList.clone();


        ArrayList<BeaconsData> beaconsData = new ArrayList();
        ArrayList<LandMark> observedBeacons = new ArrayList<>();
        double stdDeviation[] = new double[2];

        ArrayList<double[]> clonedMagValues = (ArrayList<double[]>)this.magValues.clone();
        this.magValues.clear();
        double[] mean_mag     = this.getMeanFromArrayList(clonedMagValues);



        if(!initialPointSet){
            neighboursManager.passMeanLocationMessage();
        }else{

            double distanceweigths[] = new double[beacons.size()];
            double sumweights = 0.0;
            for(int i=0;i<beacons.size();i++){
                distanceweigths[i] = 1.0/beacons.get(i).getDistance();
                sumweights += distanceweigths[i];
            }

            double rssiweights[]    = new double[beacons.size()];
            double rssiweightssum   = 0.0;
            for(int i=0;i<beacons.size();i++){
                rssiweights[i] = beacons.get(i).getRssi();
                rssiweightssum += rssiweights[i];
            }

            double x=0.0;
            double y =0.0;
            double z = 0.0;


            double x1 = 0.0;
            double y1 = 0.0;
            double z1 = 0.0;



            //send move particle and update information
            float magData[]                      = deviceMagObserved;

            double magnitude   = Math.sqrt(Math.pow(magData[0],2)+Math.pow(magData[1],2)+Math.pow(magData[2],2));
            double xy_mag      = Math.sqrt(Math.pow(magData[0],2)+Math.pow(magData[1],2));

            double magnitude_world = Math.sqrt(Math.pow(deviceMagObserved[0],2)+Math.pow(deviceMagObserved[1],2)+Math.pow(deviceMagObserved[2],2));


            if(beacons.size()<2){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(PositionTester.this,"Not enough beacons",Toast.LENGTH_LONG).show();
                    }
                });
//
//                return;
//                    double observations[] = {mag_mean, mag_mean};
//                    neighboursManager.passSimulateMessage(distance, turn, observations, stdDeviation, new double[]{0,0,0}, !rssiSignals.isEmpty(),false);

//
                initialLocationDone = true;
                if(!initialLocationDone)
                    return;
                else {
                    double observations[] = mean_mag;
                    neighboursManager.passSimulateMessage(distance, turn, observations, stdDeviation, new double[]{0, 0, 0}, !rssiSignals.isEmpty(), false, beacons.size());
                    return;
                }
            }

            String ids="";
            for(int i=0;i<beacons.size();i++){
                ids += beacons.get(i).getUniqueId()+",";
            }
            //textView.setText(ids);
            double sum_factor = 0.0;
            double rssi_sum = 0.0;




            for (int i = 0; i < beacons.size(); i++) {
                BeaconsDetail matchedBeacons = beaconsHashMap.get(beacons.get(i).getUniqueId());
                if (matchedBeacons == null) {
                    Log.e("TAG", " beacon id is null" + beacons.get(i).getUniqueId());
                    continue;
                }
                double[] p1 = convertSphericalToCartesian(matchedBeacons.getLat(), matchedBeacons.getLng());
                double weight = distanceweigths[i] / sumweights;
                sum_factor += weight;
                x += weight * p1[0];
                y += weight * p1[1];
                z += weight * p1[2];

                double rssiweight = rssiweights[i] / rssiweightssum;
                rssi_sum          += rssiweight;

                x1  += rssiweight * p1[0];
                y1  += rssiweight *p1[1];
                z1  += rssiweight * p1[2];

            }
            x /= sum_factor;
            y /= sum_factor;
            z /= sum_factor;

            x1 /= rssi_sum;
            y1 /= rssi_sum;
            z1 /= rssi_sum;





            //double observations[] = {x, y, z, magnitude, xz_mag, magData[2], magData[1]};
            double observations[] = mean_mag;
            notCalled = true;
            //double observations[] = {x, y, z};

            neighboursManager.passSimulateMessage(distance, turn, observations, stdDeviation, new double[]{x, y, z}, !rssiSignals.isEmpty(),beacons.size()>0, beacons.size());
            locationRequestDone = true;
        }


    }

    private double[] getMeanFromArrayList(ArrayList<double[]> clonedMagValues) {
        if(clonedMagValues.size() == 0){
            return new double[3];
        }
        double meanVal[] = new double[clonedMagValues.get(0).length];
        for(int i =0;i<clonedMagValues.size();i++){
            for (int k=0; k<clonedMagValues.get(0).length; k++){
                meanVal[k] += (clonedMagValues.get(i)[k] /clonedMagValues.size());
            }
        }

        return meanVal;
    }

    @Override
    public void onNeighboursLocation(double location[]) {




                double first_latlng[] = convertCartesianToSpherical(new double[]{location[0], location[1], location[2]});
                //double second_latlng[] = convertCartesianToSpherical(new double[]{Double.parseDouble(neigh.get(3).toString()), Double.parseDouble(neigh.get(4).toString()), Double.parseDouble(neigh.get(5).toString())});
                Log.d("<-----fata kya---->",String.valueOf(first_latlng[0]));
                Log.d("<-----fata kya---->", String.valueOf(first_latlng[1]));
                LatLng firstLat = new LatLng(first_latlng[0], first_latlng[1]);



                double status = location[3];


                if (!initialPointSet) {
                    startSense(firstLat);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mRobotLocationMarker == null) {
                            mRobotLocationMarker = mMap.addMarker(new MarkerViewOptions()
                                    .position(firstLat)
                                    .icon(userIcon));
                        } else {
                            mRobotLocationMarker.setPosition(firstLat);
                        }
                       //textView.setText(String.valueOf(status));
                    }

                });






    }

    @Override
    public void particlesLocation(double[][] particles) {

        for(final int[] k = {0}; k[0] <particles.length; k[0]++){
            double latLng[] = convertCartesianToSpherical(particles[k[0]]);
            //LatLng latilngi = new LatLng(latLng[0], latLng[1]);
            int finalK = k[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMap.removeLayer("layer-id"+finalK);
                    mMap.removeSource("source-id"+finalK);
                    GeoJsonSource geoJsonSource = new GeoJsonSource("source-id"+finalK, Feature.fromGeometry(
                            Point.fromLngLat(latLng[1], latLng[0])));
                    mMap.addSource(geoJsonSource);
                    SymbolLayer symbolLayer = new SymbolLayer("layer-id"+finalK, "source-id"+finalK);
//                   if(finalK == 0) {
//                       symbolLayer.withProperties(
//                               PropertyFactory.iconImage("particle"),
//                               PropertyFactory.iconAllowOverlap(true),
//                               PropertyFactory.textAllowOverlap(true)
//
//                       );
//                   }else{
                       symbolLayer.withProperties(
                               PropertyFactory.iconImage("magnetic"),
                               PropertyFactory.iconAllowOverlap(true),
                               PropertyFactory.textAllowOverlap(true)

                       );
//                   }

                    mMap.addLayer(symbolLayer);
                }
            });
        }

    }



    private double[] convertCartesianToSpherical(double[] cartesian) {
        double r = Math.sqrt(cartesian[0] * cartesian[0] + cartesian[1] * cartesian[1] + cartesian[2] * cartesian[2]);
        double lat = Math.toDegrees(asin(cartesian[2] / r));
        double lon = Math.toDegrees(atan2(cartesian[1], cartesian[0]));

        return new double[]{lat, lon};
    }

    public void rotateMapbox(double heading){
        CameraPosition currentPlace = new CameraPosition.Builder().target(mRobotLocationMarker.getPosition()).bearing(heading).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace));
    }
    private void updateMarkerPosition() {
        if (mMap != null && StepAndAzimutDetector.startLat > 0) {
            float zoomLevel = (float) mMap.getCameraPosition().zoom;
            LatLng newPos = new LatLng(StepAndAzimutDetector.startLat, StepAndAzimutDetector.startLon);
            //float rotation = (float) SensorData.azimuth;
            mUserLocationMarker.setPosition(newPos);
            CameraPosition currentPlace = new CameraPosition.Builder().target(newPos).zoom(zoomLevel).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(currentPlace));
        }

    }

    private double[] standardDeviation(ArrayList<Double>arrayList){
        if(arrayList.size() == 0){
            return new double[]{0,0};
        }
        double mean = 0.0;
        for (int k=0;k<arrayList.size();k++){
            mean += arrayList.get(k);
        }
        mean /= arrayList.size();

        double sumofsquares = 0.0;

        for (int k=0;k<arrayList.size();k++){
            sumofsquares += Math.pow(arrayList.get(k)-mean,2);
        }
        double deviation = Math.sqrt(sumofsquares/arrayList.size());
        return new double[]{mean,deviation};

    }
}
