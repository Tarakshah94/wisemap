package com.example.tarak.wisemap.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MagneticMapPojo {
    @SerializedName("magnitude_world")
    @Expose
    private double magnitudeWorld;
    @SerializedName("x_mag")
    @Expose
    private double xMag;
    @SerializedName("y_mag")
    @Expose
    private double yMag;
    @SerializedName("z_mag")
    @Expose
    private double zMag;
    @SerializedName("particles")
    @Expose
    private List<Double> particles = null;

    public double getMagnitudeWorld() {
        return magnitudeWorld;
    }

    public void setMagnitudeWorld(double magnitudeWorld) {
        this.magnitudeWorld = magnitudeWorld;
    }

    public double getXMag() {
        return xMag;
    }

    public void setXMag(double xMag) {
        this.xMag = xMag;
    }

    public double getYMag() {
        return yMag;
    }

    public void setYMag(double yMag) {
        this.yMag = yMag;
    }

    public double getZMag() {
        return zMag;
    }

    public void setZMag(double zMag) {
        this.zMag = zMag;
    }

    public List<Double> getParticles() {
        return particles;
    }

    public void setParticles(List<Double> particles) {
        this.particles = particles;
    }

    public double getParticleX(){
        return particles.get(0);
    }

    public double getParticleY(){
        return particles.get(1);
    }

    public double getParticleZ(){
        return particles.get(2);
    }
}
