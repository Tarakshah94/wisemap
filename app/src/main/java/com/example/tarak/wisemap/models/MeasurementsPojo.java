package com.example.tarak.wisemap.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MeasurementsPojo {
    @SerializedName("magnitude")
    @Expose
    private double magnitude;
    @SerializedName("magnitude_world")
    @Expose
    private double magnitudeWorld;
    @SerializedName("xy_mag")
    @Expose
    private double xyMag;
    @SerializedName("yz_mag")
    @Expose
    private double yzMag;
    @SerializedName("xz_mag")
    @Expose
    private double xzMag;
    @SerializedName("x_mag")
    @Expose
    private double xMag;
    @SerializedName("y_mag")
    @Expose
    private double yMag;
    @SerializedName("z_mag")
    @Expose
    private double zMag;
    @SerializedName("beacon_x")
    @Expose
    private double beaconX;
    @SerializedName("beacon_y")
    @Expose
    private double beaconY;
    @SerializedName("beacon_z")
    @Expose
    private double beaconZ;
    @SerializedName("beaconrssi_x")
    @Expose
    private double beaconrssiX;
    @SerializedName("beaconrssi_y")
    @Expose
    private double beaconrssiY;
    @SerializedName("beaconrssi_z")
    @Expose
    private double beaconrssiZ;

    @SerializedName("particles")
    @Expose
    private List<Double> particles = null;

    @SerializedName("mean_wifi")
    @Expose
    private Double meanWifi;
    @SerializedName("std_wifi")
    @Expose
    private Double stdWifi;


    public double getMeanWifi() {
        return meanWifi;
    }

    public void setMeanWifi(Double meanWifi) {
        this.meanWifi = meanWifi;
    }

    public double getStdWifi() {
        return stdWifi;
    }

    public void setStdWifi(Double stdWifi) {
        this.stdWifi = stdWifi;
    }


    public double getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(double magnitude) {
        this.magnitude = magnitude;
    }

    public double getMagnitudeWorld() {
        return magnitudeWorld;
    }

    public void setMagnitudeWorld(double magnitudeWorld) {
        this.magnitudeWorld = magnitudeWorld;
    }

    public double getXyMag() {
        return xyMag;
    }

    public void setXyMag(double xyMag) {
        this.xyMag = xyMag;
    }

    public double getYzMag() {
        return yzMag;
    }

    public void setYzMag(double yzMag) {
        this.yzMag = yzMag;
    }

    public double getXzMag() {
        return xzMag;
    }

    public void setXzMag(double xzMag) {
        this.xzMag = xzMag;
    }

    public double getXMag() {
        return xMag;
    }

    public void setXMag(double xMag) {
        this.xMag = xMag;
    }

    public double getYMag() {
        return yMag;
    }

    public void setYMag(double yMag) {
        this.yMag = yMag;
    }

    public double getZMag() {
        return zMag;
    }

    public void setZMag(double zMag) {
        this.zMag = zMag;
    }

    public double getBeaconX() {
        return beaconX;
    }

    public void setBeaconX(double beaconX) {
        this.beaconX = beaconX;
    }

    public double getBeaconY() {
        return beaconY;
    }

    public void setBeaconY(double beaconY) {
        this.beaconY = beaconY;
    }

    public double getBeaconZ() {
        return beaconZ;
    }

    public void setBeaconZ(double beaconZ) {
        this.beaconZ = beaconZ;
    }

    public double getBeaconrssiX() {
        return beaconrssiX;
    }

    public void setBeaconrssiX(double beaconrssiX) {
        this.beaconrssiX = beaconrssiX;
    }

    public double getBeaconrssiY() {
        return beaconrssiY;
    }

    public void setBeaconrssiY(double beaconrssiY) {
        this.beaconrssiY = beaconrssiY;
    }

    public double getBeaconrssiZ() {
        return beaconrssiZ;
    }

    public void setBeaconrssiZ(double beaconrssiZ) {
        this.beaconrssiZ = beaconrssiZ;
    }

    public List<Double> getParticles() {
        return particles;
    }

    public void setParticles(List<Double> particles) {
        this.particles = particles;
    }

    public double getParticleX(){
        return particles.get(0);
    }

    public double getParticleY(){
        return particles.get(1);
    }

    public double getParticleZ(){
        return particles.get(2);
    }
}
