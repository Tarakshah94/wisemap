package com.example.tarak.wisemap.activities;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.tarak.wisemap.R;
import com.example.tarak.wisemap.utils.FileUtils;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class WayPointEditor extends AppCompatActivity implements OnMapReadyCallback,MapboxMap.OnMapLongClickListener, MapboxMap.OnMarkerClickListener {

    private MapView mapView;
    private MapboxMap mMap;
    private String fileName;
    private JSONArray wayPointsList;
    private JSONObject wayPointMagData = new JSONObject();
    ImageButton saveButton;
    private Icon wayPointIcon;
    private Marker deleteMarker;
    private Marker testMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_point_editor);
        if(getIntent() !=null){
            if(getIntent().getExtras() != null){
                fileName = getIntent().getExtras().getString("filename");
                mapView = findViewById(R.id.mapView);
                saveButton = findViewById(R.id.saveButton);
                mapView.onCreate(savedInstanceState);
                mapView.getMapAsync(this);
                checkDirAndCreateFile(fileName);
                String name = FileUtils.loadJSONFromFile(WayPointEditor.this, fileName);

                if(name == null){
                    wayPointsList = new JSONArray();
                }else {
                    try {
                        wayPointsList = new JSONArray(name);
                    } catch (JSONException e) {
                        wayPointsList = new JSONArray();
                        e.printStackTrace();
                    }
                }
                IconFactory iconFactory = IconFactory.getInstance(this);
                wayPointIcon = iconFactory.fromResource(R.mipmap.beacon);




                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //save the existing waypointslist to file and return
                        createMagneticFileIfNotCreated();
                        FileUtils.saveFileWithoutExtension(WayPointEditor.this, wayPointsList.toString(), fileName);
                        setResult(RESULT_OK);
                        finish();
                    }
                });
            }
        }

    }

    private void createMagneticFileIfNotCreated() {
        String directoryPath = WaypointFileSelector.directoryPath+"/";
        String file = fileName.replace(directoryPath,"");
        String fileWithoutExtension = file.replace(".json", "");
        String waypointmagneticname = WaypointFileSelector.magDirectoryPath+"/"+fileWithoutExtension+"_fingdata"+".json";
        checkAndCreateDir(WaypointFileSelector.magDirectoryPath);
        checkDirAndCreateFile(waypointmagneticname);
        String name = FileUtils.loadJSONFromFile(WayPointEditor.this, waypointmagneticname);

        if(name == null){
            wayPointMagData = new JSONObject();
        }else {
            try {
                wayPointMagData = new JSONObject(name);
            } catch (JSONException e) {
                wayPointMagData = new JSONObject();
                e.printStackTrace();
            }
        }

        for(int i=0;i<wayPointsList.length();i++){
            try {
                JSONObject insideObj = wayPointsList.getJSONObject(i);
                double latitude = insideObj.getDouble("latitude");
                double longitude = insideObj.getDouble("longitude");
                double position[] = convertSphericalToCartesian(latitude, longitude);
                String key = String.valueOf(position[0])+ "-->" +String.valueOf(position[1]) + "-->" + position[2];
                if(wayPointMagData.has(key)){

                }else{
                    JSONObject objInside = new JSONObject();
                    objInside.put("mean",-100);
                    objInside.put("error",-100);
                    wayPointMagData.put(key, objInside);
                }

            }catch (JSONException e){

            }
        }
        FileUtils.saveFileWithoutExtension(WayPointEditor.this, wayPointMagData.toString(), waypointmagneticname);
    }

    private double[] convertSphericalToCartesian(double latitude, double longitude) {
        double earthRadius = 6371000; //radius in m
        double lat = this.degToRad(latitude);
        double lon = this.degToRad(longitude);
        double x = earthRadius * cos(lat) * cos(lon);
        double y = earthRadius * cos(lat) * sin(lon);
        double z = earthRadius * sin(lat);
        return new double[]{x, y, z};
    }
    private double degToRad(double deg) {
        return deg * Math.PI / 180;
    }


    private void checkDirAndCreateFile(String directoryPath) {
        File file = new File(directoryPath);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkAndCreateDir(String directoryPath) {
        File file = new File(directoryPath);
        if(!file.exists()){
            file.mkdirs();
        }
    }

    private void addAllWaypointsAsMarkers() {
        for(int i=0;i<wayPointsList.length();i++){
            try {
                JSONObject insideObj = wayPointsList.getJSONObject(i);
                double latitude      = insideObj.getDouble("latitude");
                double longitude     = insideObj.getDouble("longitude");

                 mMap.addMarker(new MarkerViewOptions()
                        .position(new LatLng(latitude, longitude))
                        .icon(wayPointIcon));



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMapLongClick(@NonNull LatLng point) {
        JSONObject insideObject = new JSONObject();
        try {
            insideObject.put("latitude", point.getLatitude());
            insideObject.put("longitude", point.getLongitude());
            wayPointsList.put(insideObject);

            double latitude      = insideObject.getDouble("latitude");
            double longitude     = insideObject.getDouble("longitude");


            mMap.addMarker(new MarkerViewOptions()
                    .anchor(0.5f, 0.5f)
                    .position(new LatLng(latitude, longitude))
                    .icon(wayPointIcon));



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMap = mapboxMap;
        mMap.addOnMapLongClickListener(this);


        mMap.setOnMarkerClickListener(this);
        addAllWaypointsAsMarkers();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        if(deleteMarker == null){
            deleteMarker = marker;
            return true;
        }
        if(deleteMarker.getPosition() == marker.getPosition())
        {
            double latitude = deleteMarker.getPosition().getLatitude();
            double longitude = deleteMarker.getPosition().getLongitude();
            removeObject(latitude, longitude);
            mMap.removeMarker(marker);
            deleteMarker = null;
            return true;
        }

        deleteMarker = marker;
        return  true;
    }

    private void removeObject(double lat, double lng) {
        int index = -1;
        for(int i=0;i<wayPointsList.length();i++){
            try {
                double latitude = wayPointsList.getJSONObject(i).getDouble("latitude");
                double longitude = wayPointsList.getJSONObject(i).getDouble("longitude");
                if(latitude == lat && longitude == lng){
                    index = i;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(index > 0) {
            wayPointsList.remove(index);
        }else{
            Toast.makeText(WayPointEditor.this, "delete is not possible something wrong", Toast.LENGTH_LONG).show();
        }
    }
}
