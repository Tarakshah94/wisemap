package com.example.tarak.wisemap.models;

public class LandMark {

    private double mX;
    private double mY;
    private double mZ;


    public double getmX() {
        return mX;
    }

    public double getmY() {
        return mY;
    }

    public double getmZ() {
        return mZ;
    }

    public double getRssi() {
        return rssi;
    }

    public void setRssi(double rssi) {
        this.rssi = rssi;
    }

    private double rssi;
    public LandMark(double x, double y, double z, String id,double rssi){
        this.mX         = x;
        this.mY         = y;
        this.mZ         = z;
        this.rssi       = rssi;



    }



    public double[] getPos(){
        return new double[]{this.mX,this.mY,this.mZ};
    }



}
