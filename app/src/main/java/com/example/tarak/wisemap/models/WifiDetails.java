package com.example.tarak.wisemap.models;

import com.google.gson.annotations.SerializedName;

import java.util.Random;

/**
 * Created by Furqan on 08-02-2018.
 */

public class WifiDetails {

    @SerializedName("rssi")
    private int rssi;
    @SerializedName("bssid")
    private String bssid;

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public void setRssiGaussian(double noise) {
        this.rssi += new Random().nextGaussian() * noise + 0;
    }



}
