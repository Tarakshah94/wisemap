package com.example.tarak.wisemap.models;

import com.google.gson.annotations.SerializedName;

import java.util.Random;

/**
 * Created by Furqan on 23-08-2017.
 */

public class BeaconsData {
    @SerializedName("beacon_distance")
    private double beacon_distance;

    @SerializedName("beacon_kalman_distance")
    private double beacon_kalman_distance;

    public double getBeacon_kalman_distance() {
        return beacon_kalman_distance;
    }

    public void setBeacon_kalman_distance(double beacon_kalman_distance) {
        this.beacon_kalman_distance = beacon_kalman_distance;
    }

    @SerializedName("beacon_lat")
    private double beacon_lat;
    @SerializedName("beacon_lng")
    private double beacon_lng;
    @SerializedName("max_beacon")
    private int max_beacon;
    @SerializedName("used_beacon")
    private int used_beacon;

    @SerializedName("beacon_position")
    private double[] beaconPosition;


    @SerializedName("beacon_rssi")
    private int beaconRssi;


    public double getBeacon_distance() {
        return beacon_distance;
    }

    public void setBeacon_distance(double beacon_distance) {
        this.beacon_distance = beacon_distance;
    }


    public double getBeacon_lat() {
        return beacon_lat;
    }

    public void setBeacon_lat(double beacon_lat) {
        this.beacon_lat = beacon_lat;
    }

    public double getBeacon_lng() {
        return beacon_lng;
    }

    public void setBeacon_lng(double beacon_lng) {
        this.beacon_lng = beacon_lng;
    }

    public int getMax_beacon() {
        return max_beacon;
    }

    public void setMax_beacon(int max_beacon) {
        this.max_beacon = max_beacon;
    }

    public int getUsed_beacon() {
        return used_beacon;
    }

    public void setUsed_beacon(int used_beacon) {
        this.used_beacon = used_beacon;
    }


    public double[] getBeaconPosition() {
        return beaconPosition;
    }

    public void setBeaconPosition(double[] beaconPosition) {
        this.beaconPosition = beaconPosition;
    }


    public int getBeaconRssi() {
        return beaconRssi;
    }

    public void setBeaconRssi(int beaconRssi) {
        this.beaconRssi = beaconRssi;
    }

    public void setBeaconRssiGaussian(double noise){
        this.beaconRssi += new Random().nextGaussian() * noise + 0;
    }
    public void setBeaconDistanceGaussian(double noise){
        this.beacon_distance +=  new Random().nextGaussian() * noise + 0;
    }

    public double getX(){
        return beaconPosition[0];
    }

    public double getY(){
        return beaconPosition[1];
    }

    public double getZ(){
        return beaconPosition[2];
    }
}


