package com.example.tarak.wisemap.managers;

public interface LocationUpdater {
    public void onParticleLocationsUpdated(double[] values, double[] robot);
    public void showToast(String val,String name);
}
