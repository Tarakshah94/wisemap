# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)
include_directories(src/main/cpp/Eigen)
include_directories(src/main/cpp/dist)
include_directories(src/main/cpp/FastSlam)
include_directories(src/main/cpp/models)
include_directories(src/main/cpp/slam)
include_directories(src/main/cpp/kdtree)
include_directories(src/main/cpp/AugmentedTree)



# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_library( # Sets the name of the library.
        slamlib

        # Sets the library as a shared library.
        SHARED

        # Provides a relative path to your source file(s).

        src/main/cpp/slamlib.cpp
        src/main/cpp/FastSlam/DataAssociation.cpp
        src/main/cpp/FastSlam/FastSlam.cpp
        src/main/cpp/FastSlam/HelperClass.cpp
        src/main/cpp/FastSlam/Landmark.cpp
        src/main/cpp/FastSlam/Robot.cpp
        src/main/cpp/slam/LocProvider.cpp
        src/main/cpp/slam/Particle.cpp
        src/main/cpp/dist/jsoncpp.cpp
        src/main/cpp/kdtree/KDTree.cpp)

# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.

find_library( # Sets the name of the path variable.
        log-lib

        # Specifies the name of the NDK library that
        # you want CMake to locate.
        log)

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

target_link_libraries( # Specifies the target library.
        slamlib

        # Links the target library to the log library
        # included in the NDK.
        ${log-lib})